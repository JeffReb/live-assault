import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RedirectGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (this.auth.getToken()) {
      if (this.auth.getAuthenticated()) {
        if (!this.auth.getValidated()) {
          this.router.navigateByUrl('/emailvalidation');
        } else {
          if (!this.auth.getActivity()) {
            this.router.navigateByUrl('/home');
          } else {
            this.router.navigateByUrl('/home');
          }
        }

        return false;
      }
    }

    return true;
  }
}
