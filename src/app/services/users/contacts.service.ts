import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ContactState } from '../../models/contact-state';
import { Socket } from 'ngx-socket-io';
import { JwtHelperService } from '@auth0/angular-jwt';

const APIEndpoint = environment.APIEndpoint;

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  private BASE_URL = APIEndpoint;
  private myID: string;
  private myUserName: string;

  constructor(private https: HttpClient, private socket: Socket) {}

  contactState(contactId): Observable<ContactState> {
    const url = `${this.BASE_URL}/contacts/contactstate`;
    return this.https.post<ContactState>(url, { contactId });
  }

  inviteContact(contactId): Observable<ContactState> {
    const url = `${this.BASE_URL}/contacts/sendinvite`;
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.myUserName = helper.decodeToken(tokenUser)['userName'];
    const newMess = 'You receive a new contact request from: ' + this.myUserName;
    const messSocket = [
      {
        idUserDest: contactId,
        content: {
          idFrom: this.myID,
          noteType: 'invitation',
          mess: newMess
        }
      }
    ];
    this.socket.emit('contact request', messSocket);
    return this.https.post<ContactState>(url, { contactId });
  }

  refuseContact(contactId): Observable<ContactState> {
    const url = `${this.BASE_URL}/contacts/refuseinvite`;
    return this.https.post<ContactState>(url, { contactId });
  }

  deleteContact(contactId): Observable<ContactState> {
    const url = `${this.BASE_URL}/contacts/removecontact`;
    return this.https.post<ContactState>(url, { contactId });
  }

  acceptContact(contactId): Observable<ContactState> {
    const url = `${this.BASE_URL}/contacts/acceptinvite`;
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.myUserName = helper.decodeToken(tokenUser)['userName'];
    const newMess = 'Your contact request is accepted by: ' + this.myUserName;
    const messSocket = [
      {
        idUserDest: contactId,
        content: {
          idFrom: this.myID,
          noteType: 'accept',
          mess: newMess
        }
      }
    ];
    this.socket.emit('contact accept', messSocket);
    return this.https.post<ContactState>(url, { contactId });
  }
}
