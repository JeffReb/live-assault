import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserRead } from '../../models/user-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  readUser(userId): Observable<UserRead> {
    const url = `${this.BASE_URL}/users/read`;
    return this.https.post<UserRead>(url, { userId });
  }
}
