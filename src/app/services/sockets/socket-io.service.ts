import { Socket } from 'ngx-socket-io';
import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../../models/user';
import { ContactState } from '../../models/contact-state';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

const APIEndpoint = environment.APIEndpoint;

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {
  private myID: string;
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient, private socket: Socket) {}

  getMessages = () => {
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];

    return new Observable((observer: Observer<any>) => {
      this.socket.on(this.myID, message => {
        // console.log(message[0].content.noteType);
        observer.next(message[0].content);
      });
    });
  };

  setConnectedStatus(connected): Observable<User> {
    const url = `${this.BASE_URL}/socketuser/setconnected`;
    return this.https.post<User>(url, { connected });
  }
}
