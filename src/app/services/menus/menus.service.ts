import { Injectable } from '@angular/core';
import { Menu } from '../../theme/components/menu/menu.model';
import * as _ from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class MenusService {
  public verticalHomeMenuItemsService: Array<any>;

  constructor() {}

  public getMenus(activities): Array<any> {
    this.verticalHomeMenuItemsService = [];

    // console.log('activities: ', activities);
    let bandObj = {};
    let siteObj = {};
    let manageObj = {};
    let orgaObj = {};
    let serviceObj = {};
    let equipObj = {};
    if (activities === null) {
      bandObj = {};
      siteObj = {};
      manageObj = {};
      orgaObj = {};
      serviceObj = {};
      equipObj = {};
    } else {
      const acti = activities;
      const lenact = Object.keys(acti).length;
      for (let i = 0, len = lenact; i < len; ++i) {
        const keyact = Object.keys(acti)[i];
        const keyActValue = Object.values(acti)[i];
        const keyactband = _.startsWith(keyact, 'band');
        const keyactsite = _.startsWith(keyact, 'site');
        const keyactmanage = _.startsWith(keyact, 'manage');
        const keyactorga = _.startsWith(keyact, 'orga');
        const keyactservice = _.startsWith(keyact, 'service');
        const keyactequip = _.startsWith(keyact, 'equip');
        // console.log('keyActValue', keyActValue);
        if (keyactband === true) {
          const nameband = _.replace(keyact, 'band_', '');
          bandObj[nameband] = Object.values(acti)[i];
        }
        if (keyactsite === true) {
          const namesite = _.replace(keyact, 'site_', '');
          siteObj[namesite] = Object.values(acti)[i];
        }
        if (keyactmanage === true) {
          const namemanage = _.replace(keyact, 'manage_', '');
          manageObj[namemanage] = Object.values(acti)[i];
        }
        if (keyactorga === true) {
          const nameorga = _.replace(keyact, 'orga_', '');
          orgaObj[nameorga] = Object.values(acti)[i];
        }
        if (keyactservice === true) {
          const nameservice = _.replace(keyact, 'service_', '');
          serviceObj[nameservice] = Object.values(acti)[i];
        }
        if (keyactequip === true) {
          const nameequip = _.replace(keyact, 'equip_', '');
          equipObj[nameequip] = Object.values(acti)[i];
        }
      }
      if (Object.keys(bandObj).length === 0) {
        bandObj = {};
      }
      if (Object.keys(siteObj).length === 0) {
        siteObj = {};
      }
      if (Object.keys(manageObj).length === 0) {
        manageObj = {};
      }
      if (Object.keys(orgaObj).length === 0) {
        orgaObj = {};
      }
      if (Object.keys(serviceObj).length === 0) {
        serviceObj = {};
      }
      if (Object.keys(equipObj).length === 0) {
        equipObj = {};
      }
    }
    this.verticalHomeMenuItemsService.push(new Menu(1, 'Search...', '/home', null, 'find_in_page', null, false, 0));
    const lenband = Object.keys(bandObj).length;
    for (let iband = 0, lenBand = lenband; iband < lenBand; ++iband) {
      const nameband = Object.keys(bandObj)[iband];
      const idBandToken = Object.values(bandObj)[iband];
      const idBand = helper.decodeToken(idBandToken.toString())['bandId'];
      const routeBand = '/home/complete-band/?id=' + idBand;
      // console.log('nameband: ', nameband, 'n°: ', iband, 'id band: ', idBand);
      this.verticalHomeMenuItemsService.push(new Menu(iband + 10, nameband, null, null, 'music_video', null, true, 0));
      this.verticalHomeMenuItemsService.push(
        new Menu(iband + 100, 'Notifications', '/', null, 'notifications', null, false, iband + 10)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(iband + 125, 'Planning', '/', null, 'event', null, false, iband + 10)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(iband + 150, 'Channel news', null, routeBand, 'edit', null, false, iband + 10)
      );
    }
    const lensite = Object.keys(siteObj).length;
    for (let isite = 0, lenSite = lensite; isite < lenSite; ++isite) {
      const namesite = Object.keys(siteObj)[isite];
      const idSiteToken = Object.values(siteObj)[isite];
      const idSite = helper.decodeToken(idSiteToken.toString())['siteId'];
      const routeSite = '/home/complete-site/?id=' + idSite;
      // console.log('namesite: ', namesite, 'n°: ', isite, 'idSite: ', idSite);
      this.verticalHomeMenuItemsService.push(
        new Menu(isite + lenband + 10, namesite, null, null, 'room', null, true, 0)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(isite + lenband + 300, 'Notifications', '/', null, 'notifications', null, false, isite + lenband + 10)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(isite + lenband + 325, 'Planning', '/', null, 'event', null, false, isite + lenband + 10)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(isite + lenband + 350, 'Channel news', null, routeSite, 'edit', null, false, isite + lenband + 10)
      );
    }
    const lenmanage = Object.keys(manageObj).length;
    for (let imanage = 0, lenManage = lenmanage; imanage < lenManage; ++imanage) {
      const namemanage = Object.keys(manageObj)[imanage];
      const idManageToken = Object.values(manageObj)[imanage];
      const idManage = helper.decodeToken(idManageToken.toString())['manageId'];
      const routeManage = '/home/complete-manage/?id=' + idManage;
      // console.log('namemanage: ', namemanage, 'n°: ', imanage, 'idManage: ', idManage);
      this.verticalHomeMenuItemsService.push(
        new Menu(imanage + lensite + lenband + 10, namemanage, '/', null, 'description', null, true, 0)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          imanage + lensite + lenband + 500,
          'Notifications',
          '/',
          null,
          'notifications',
          null,
          false,
          imanage + lensite + lenband + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          imanage + lensite + lenband + 525,
          'Planning',
          '/',
          null,
          'event',
          null,
          false,
          imanage + lensite + lenband + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          imanage + lensite + lenband + 550,
          'Channel news',
          null,
          routeManage,
          'edit',
          null,
          false,
          imanage + lensite + lenband + 10
        )
      );
    }
    const lenorga = Object.keys(orgaObj).length;
    for (let iorga = 0, lenOrga = lenorga; iorga < lenOrga; ++iorga) {
      const nameorga = Object.keys(orgaObj)[iorga];
      const idOrgaToken = Object.values(orgaObj)[iorga];
      const idOrga = helper.decodeToken(idOrgaToken.toString())['orgaId'];
      const routeOrga = '/home/complete-orga/?id=' + idOrga;
      // console.log('nameorga: ', nameorga, 'n°: ', iorga, 'idOrga: ', idOrga);
      this.verticalHomeMenuItemsService.push(
        new Menu(iorga + lensite + lenband + lenmanage + 10, nameorga, '/', null, 'group_work', null, true, 0)
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iorga + lensite + lenband + lenmanage + 700,
          'Notifications',
          '/',
          null,
          'notifications',
          null,
          false,
          iorga + lensite + lenband + lenmanage + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iorga + lensite + lenband + lenmanage + 725,
          'Planning',
          '/',
          null,
          'event',
          null,
          false,
          iorga + lensite + lenband + lenmanage + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iorga + lensite + lenband + lenmanage + 750,
          'Channel news',
          null,
          routeOrga,
          'edit',
          null,
          false,
          iorga + lensite + lenband + lenmanage + 10
        )
      );
    }
    const lenservice = Object.keys(serviceObj).length;
    for (let iservice = 0, lenService = lenservice; iservice < lenService; ++iservice) {
      const nameservice = Object.keys(serviceObj)[iservice];
      const idServiceToken = Object.values(serviceObj)[iservice];
      const idService = helper.decodeToken(idServiceToken.toString())['servicesId'];
      const routeService = '/home/complete-service/?id=' + idService;
      // console.log('nameservice: ', nameservice, 'n°: ', iservice, 'idServiceToken: ', idService);
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iservice + lensite + lenband + lenmanage + lenorga + 10,
          nameservice,
          '/',
          null,
          'extension',
          null,
          true,
          0
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iservice + lensite + lenband + lenmanage + lenorga + 900,
          'Notifications',
          '/',
          null,
          'notifications',
          null,
          false,
          iservice + lensite + lenband + lenmanage + lenorga + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iservice + lensite + lenband + lenmanage + lenorga + 925,
          'Planning',
          '/',
          null,
          'event',
          null,
          false,
          iservice + lensite + lenband + lenmanage + lenorga + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iservice + lensite + lenband + lenmanage + lenorga + 950,
          'Channel news',
          null,
          routeService,
          'edit',
          null,
          false,
          iservice + lensite + lenband + lenmanage + lenorga + 10
        )
      );
    }
    const lenequip = Object.keys(equipObj).length;
    for (let iequip = 0, lenEquip = lenequip; iequip < lenEquip; ++iequip) {
      const nameequip = Object.keys(equipObj)[iequip];
      const idEquipToken = Object.values(equipObj)[iequip];
      const idEquip = helper.decodeToken(idEquipToken.toString())['equipId'];
      const routeEquip = '/home/complete-equip/?id=' + idEquip;
      // console.log('nameequip: ', nameequip, 'n°: ', iequip, 'idEquip: ', idEquip);
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 10,
          nameequip,
          '/',
          null,
          'shop',
          null,
          true,
          0
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 1100,
          'Notifications',
          '/',
          null,
          'notifications',
          null,
          false,
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 1120,
          'Planning',
          '/',
          null,
          'event',
          null,
          false,
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 10
        )
      );
      this.verticalHomeMenuItemsService.push(
        new Menu(
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 1150,
          'Channel news',
          null,
          routeEquip,
          'edit',
          null,
          false,
          iequip + lensite + lenband + lenmanage + lenorga + lenservice + 10
        )
      );
    }
    // const menusactis = activities;
    // const lenmenusactis = Object.keys(menusactis).length;
    // console.log('lenband', lenband);
    // console.log('lensite', lensite);

    this.verticalHomeMenuItemsService.push(
      new Menu(3000, 'Create your line-up', null, null, 'add_box', null, true, 0),
      new Menu(3001, 'Bands', '/home/create-band', null, 'music_video', null, false, 3000),
      new Menu(3002, 'Site - Bar', '/home/create-site', null, 'room', null, false, 3000),
      new Menu(3003, 'Management', '/home/create-manage', null, 'description', null, false, 3000),
      new Menu(3004, 'Association', '/home/create-orga', null, 'group_work', null, false, 3000),
      new Menu(3005, 'Services', '/home/create-service', null, 'extension', null, false, 3000),
      new Menu(3006, 'Equipment Store', '/home/create-equip', null, 'shop', null, false, 3000),
      new Menu(3007, 'Global Planning', '/', null, 'date_range', null, false, 0),
      new Menu(3008, 'Chat', '/home/chat', null, 'forum', null, false, 0)
    );
    return this.verticalHomeMenuItemsService;
  }
}
