import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Manage } from '../../../models/manage';
import { ManageRead } from '../../../models/manage-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ManageService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerManage(
    namemanage: string,
    stylemanage: string,
    adressmanage: string,
    phonemanage: string,
    emailmanage: string,
    typemanage: string,
    statusmanage: string,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latManage: string,
    lngManage: string
  ): Observable<Manage> {
    const url = `${this.BASE_URL}/manages/register`;
    return this.https.post<Manage>(url, {
      namemanage,
      stylemanage,
      adressmanage,
      phonemanage,
      emailmanage,
      typemanage,
      statusmanage,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latManage,
      lngManage
    });
  }

  readManage(manageId): Observable<ManageRead> {
    const url = `${this.BASE_URL}/manages/read`;
    return this.https.post<ManageRead>(url, { manageId });
  }
}
