import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../models/user';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerActivity(firstname: string, lastname: string, phone: string): Observable<User> {
    const url = `${this.BASE_URL}/activity/register`;
    return this.https.post<User>(url, {
      firstname,
      lastname,
      phone
    });
  }
}
