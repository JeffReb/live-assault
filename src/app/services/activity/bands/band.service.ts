import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { Band } from '../../../models/band';
import { BandRead } from '../../../models/band-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class BandService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerBand(
    nameband: string,
    style1: string,
    style2: string,
    style3: string,
    emailband: string,
    roleband: object,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latBand: string,
    lngBand: string
  ): Observable<Band> {
    const url = `${this.BASE_URL}/bands/register`;
    return this.https.post<Band>(url, {
      nameband,
      style1,
      style2,
      style3,
      emailband,
      roleband,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latBand,
      lngBand
    });
  }

  readBand(bandId): Observable<BandRead> {
    const url = `${this.BASE_URL}/bands/read`;
    return this.https.post<BandRead>(url, { bandId });
  }
}
