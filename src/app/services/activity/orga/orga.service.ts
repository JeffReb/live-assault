import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Orga } from '../../../models/orga';
import { OrgaRead } from '../../../models/orga-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class OrgaService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerOrga(
    nameorga: string,
    styleorga: string,
    adressorga: string,
    phoneorga: string,
    emailorga: string,
    typeorga: string,
    statusorga: string,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latOrga: string,
    lngOrga: string
  ): Observable<Orga> {
    const url = `${this.BASE_URL}/orgas/register`;
    return this.https.post<Orga>(url, {
      nameorga,
      styleorga,
      adressorga,
      phoneorga,
      emailorga,
      typeorga,
      statusorga,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latOrga,
      lngOrga
    });
  }

  readOrga(orgaId): Observable<OrgaRead> {
    const url = `${this.BASE_URL}/orgas/read`;
    return this.https.post<OrgaRead>(url, { orgaId });
  }
}
