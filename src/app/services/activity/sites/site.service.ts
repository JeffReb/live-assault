import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Site } from '../../../models/site';
import { SiteRead } from '../../../models/site-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class SiteService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerSite(
    namehall: string,
    stylehall: string,
    adresshall: string,
    phonehall: string,
    emailhall: string,
    typehall: string,
    capacityhall: string,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latHall: string,
    lngHall: string
  ): Observable<Site> {
    const url = `${this.BASE_URL}/sites/register`;
    return this.https.post<Site>(url, {
      namehall,
      stylehall,
      adresshall,
      phonehall,
      emailhall,
      typehall,
      capacityhall,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latHall,
      lngHall
    });
  }

  readSite(siteId): Observable<SiteRead> {
    const url = `${this.BASE_URL}/sites/read`;
    return this.https.post<SiteRead>(url, { siteId });
  }
}
