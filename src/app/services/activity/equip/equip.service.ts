import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Equip } from '../../../models/equip';
import { EquipRead } from '../../../models/equip-read';
import { OrgaRead } from '../../../models/orga-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class EquipService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerEquip(
    nameequip: string,
    adressequip: string,
    phoneequip: string,
    emailequip: string,
    typeequip: string,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latEquip: string,
    lngEquip: string
  ): Observable<Equip> {
    const url = `${this.BASE_URL}/equips/register`;
    return this.https.post<Equip>(url, {
      nameequip,
      adressequip,
      phoneequip,
      emailequip,
      typeequip,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latEquip,
      lngEquip
    });
  }

  readEquip(equipId): Observable<EquipRead> {
    const url = `${this.BASE_URL}/equips/read`;
    return this.https.post<EquipRead>(url, { equipId });
  }
}
