import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Services } from '../../../models/services';
import { ServiceRead } from '../../../models/service-read';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  registerServices(
    nameservices: string,
    styleservices: string,
    adressservices: string,
    phoneservices: string,
    emailservices: string,
    servicesservices: object,
    statusservices: string,
    cityid: string,
    city: string,
    cityalt2id: string,
    cityalt2: string,
    cityalt1id: string,
    cityalt1: string,
    countryid: string,
    country: string,
    countrycode: string,
    continent: string,
    latServices: string,
    lngServices: string
  ): Observable<Services> {
    const url = `${this.BASE_URL}/services/register`;
    return this.https.post<Services>(url, {
      nameservices,
      styleservices,
      adressservices,
      phoneservices,
      emailservices,
      servicesservices,
      statusservices,
      cityid,
      city,
      cityalt2id,
      cityalt2,
      cityalt1id,
      cityalt1,
      countryid,
      country,
      countrycode,
      continent,
      latServices,
      lngServices
    });
  }

  readService(serviceId): Observable<ServiceRead> {
    const url = `${this.BASE_URL}/services/read`;
    return this.https.post<ServiceRead>(url, { serviceId });
  }
}
