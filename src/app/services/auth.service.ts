import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { environment } from '../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

const APIEndpoint = environment.APIEndpoint;

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  getToken(): string {
    return localStorage.getItem('token');
  }

  getCurrentIdAct(): string {
    return localStorage.getItem('currentIdAct');
  }

  getStatus(): Observable<User> {
    const url = `${this.BASE_URL}/users/status`;
    return this.https.get<User>(url);
  }

  logIn(email: string, password: string): Observable<any> {
    const url = `${this.BASE_URL}/users/login`;
    return this.https.post<User>(url, { email, password });
  }

  signUp(name: string, email: string, password: string): Observable<User> {
    const url = `${this.BASE_URL}/users/register`;
    return this.https.post<User>(url, { name, email, password });
  }

  sendEmailConf(id: string): Observable<User> {
    const url = `${this.BASE_URL}/users/emailconf`;
    return this.https.post<User>(url, { id });
  }

  receipEmailConf(email: string, password: string, tokenemail: string): Observable<any> {
    const url = `${this.BASE_URL}/users/emailconflog`;
    return this.https.post<User>(url, { email, password, tokenemail });
  }

  getAuthenticated(): boolean {
    if (!this.getToken()) {
      return false;
    } else {
      return true;
    }
  }

  getValidated(): boolean {
    if (!this.getToken()) {
      return false;
    } else {
      // console.log(helper.decodeToken(this.getToken())['isValidate']);
      return helper.decodeToken(this.getToken())['isValidate'];
    }
  }

  getActivity(): boolean {
    if (!this.getToken()) {
      return false;
    } else {
      // console.log(helper.decodeToken(this.getToken())['isActivity']);
      return helper.decodeToken(this.getToken())['isActivity'];
    }
  }
}
