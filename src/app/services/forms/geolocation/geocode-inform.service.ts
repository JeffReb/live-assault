import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

declare var google;

@Injectable({
  providedIn: 'root'
})
export class GeocodeInformService {
  bounds: google.maps.LatLngBounds;
  markers: google.maps.Marker[];
  infoWindow: google.maps.InfoWindow;

  getGeoCode$: Observable<any>;

  private getGeoCodeSubject = new Subject<any>();

  constructor() {
    this.getGeoCode$ = this.getGeoCodeSubject.asObservable();
  }

  getGeoCode(dataGeo) {
    // console.log('Service GeoCode receive data: ', dataGeo[0]);

    const lat = +dataGeo[0];
    const long = +dataGeo[1];

    const resultLocate = [];

    // console.log('lat in service: ', lat, long);

    const activity = dataGeo[2];
    if (activity === 'activity') {
      return false;
    } else {
      const locate = new google.maps.LatLng(lat, long);

      FindLocateCallback(locate, data => {
        const locateRes = data.Locate;
        // console.log('callbackLocate: ', data.Locate);
        // console.log('callbackLocate: ', locateRes[3].country);
        // localStorage.setItem('locate', data.Locate);
        // return data.Locate;
      });
    }

    function FindLocateCallback(FindLocate, callback) {
      if (activity === 'activity') {
        return false;
      } else {
        new google.maps.Geocoder().geocode({ location: FindLocate }, function geoCodBand(results, status) {
          if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              let country = null;
              let countryCode = null;
              let city = null;
              let cityAlt1 = null;
              let cityAlt2 = null;
              let c;
              let lc;
              let component;
              for (let r = 0, rl = results.length; r < rl; r += 1) {
                const result = results[r];

                if (!city && result.types[0] === 'locality') {
                  for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
                    component = result.address_components[c];

                    if (component.types[0] === 'locality') {
                      city = component.long_name;
                      break;
                    }
                  }
                } else if (!cityAlt1 && result.types[0] === 'administrative_area_level_1') {
                  for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
                    component = result.address_components[c];

                    if (component.types[0] === 'administrative_area_level_1') {
                      cityAlt1 = component.long_name;
                      break;
                    }
                  }
                } else if (!cityAlt2 && result.types[0] === 'administrative_area_level_2') {
                  for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
                    component = result.address_components[c];

                    if (component.types[0] === 'administrative_area_level_2') {
                      cityAlt2 = component.long_name;
                      break;
                    }
                  }
                } else if (!country && result.types[0] === 'country') {
                  country = result.address_components[0].long_name;
                  countryCode = result.address_components[0].short_name;
                }

                if (city && country) {
                  break;
                }
              }

              FindId(city, data => {
                localStorage.setItem('callbackCityId'.concat(activity), data.ID);
                localStorage.setItem('callbackCity'.concat(activity), city);
                const CityID = data.ID;
                resultLocate.push({ CityID });
              });
              FindId(cityAlt1, data => {
                localStorage.setItem('callbackCityAlt1Id'.concat(activity), data.ID);
                localStorage.setItem('callbackCityAlt1'.concat(activity), cityAlt1);
              });

              if (cityAlt2 === null) {
                localStorage.setItem('callbackCityAlt2Id'.concat(activity), null);
                localStorage.setItem('callbackCityAlt2'.concat(activity), null);
              }

              FindId(cityAlt2, data => {
                localStorage.setItem('callbackCityAlt2Id'.concat(activity), data.ID);
                localStorage.setItem('callbackCityAlt2'.concat(activity), cityAlt2);
              });

              FindId(country, data => {
                localStorage.setItem('callbackCountryId'.concat(activity), data.ID);
                localStorage.setItem('callbackCountry'.concat(activity), country);
              });

              localStorage.setItem('callbackCountryCode'.concat(activity), countryCode);

              resultLocate.push({ city }, { cityAlt1 }, { cityAlt2 }, { country }, { countryCode });
              resultLocate.push({ city });

              callback({ Status: 'OK', Locate: resultLocate });
            }
          }
        });
      }
    }
    function FindId(FindID, callback) {
      if (activity === 'activity') {
        return false;
      } else {
        new google.maps.Geocoder().geocode({ address: FindID }, function geoCodeId(resultsId, statusId) {
          if (statusId === google.maps.GeocoderStatus.OK) {
            const resultID = resultsId[0].place_id;
            callback({ Status: 'OK', ID: resultID });
          }
        });
      }
    }

    this.getGeoCodeSubject.next(dataGeo);
    {
    }
  }
}
