import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { NominatimResponse } from '../../../models/nominatim-response';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StreetMapGeoService {
  private BASE_URL = 'https://nominatim.openstreetmap.org';

  constructor(private https: HttpClient) {}

  getLatLongOpenStreetMap(req?: any): Observable<NominatimResponse[]> {
    const url = `${this.BASE_URL}/search?q=${req}&format=json&adressdetails=1`;
    // console.log(url);
    return this.https
      .get(url)
      .pipe(
        map((data: any[]) => data.map((item: any) => new NominatimResponse(item.lat, item.lon, item.display_name)))
      );
  }
}
