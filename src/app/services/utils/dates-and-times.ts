import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatesAndTimes {
  private memberfullyear: string;
  private membermonth: string;
  private memberdate: string;
  private memberyear: string;

  constructor() {}

  getMemberSince(addedDate: string): string {
    const memberSince = new Date(addedDate);

    this.memberfullyear = memberSince.getFullYear().toString();
    this.memberyear = this.memberfullyear.substr(this.memberfullyear.length - 2);
    this.memberdate = memberSince.getDate().toString();
    const memberMonthNumber = memberSince.getMonth();
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.membermonth = monthNames[memberMonthNumber];

    // return this.membermonth + ' ' + this.memberfullyear;
    return this.memberdate + '-' + this.membermonth + '-' + this.memberyear;
  }
}
