import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.getAuthenticated()) {
      this.router.navigateByUrl('/login');
      return false;
    }
    return true;
  }
}

export class ValidateGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.getValidated()) {
      this.router.navigateByUrl('/emailvalidation');
      return false;
    }
    return true;
  }
}

export class ActivityGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.getActivity()) {
      this.router.navigateByUrl('/home');
      return false;
    }
    return true;
  }
}
