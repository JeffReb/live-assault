import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';
import { AppStates, State } from '../../store/app.states';

import { AuthService } from '../../services/auth.service';
import { select, Store } from '@ngrx/store';
import { GetStatus } from '../../store/actions/auth.actions';
import { Observable } from 'rxjs';
// import { getAppUser, getElementsState } from '../../store/selectors/user-state.selector';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-homepublic',
  templateUrl: './homepublic.component.html',
  styleUrls: ['./homepublic.component.scss']
})
export class HomepublicComponent implements AfterViewInit, OnInit {
  public settings: Settings;

  public user: Observable<object> | null;

  public token: Observable<string> | null;
  public profile: Observable<object> | null;
  public isValidate: Observable<boolean> | null;

  constructor(public appSettings: AppSettings, private store: Store<State>) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    // this.user = this.store.select<any>(getAppUser);
    // this.user.subscribe(data => {
    //   if (!data) {
    //     console.log('data-null');
    //     console.log(data);
    //   } else {
    //     console.log('data-not-null');
    //     console.log(data['profile']['userId']);
    //     this.profile = this.store.select<any>(getAppUser).pipe(map(user => user.profile));
    //     this.profile.subscribe(dataprofile => {
    //       if (!dataprofile) {
    //         console.log('data-null');
    //         console.log(dataprofile);
    //       } else {
    //         console.log('data-not-null');
    //         console.log(dataprofile['isValidate']);
    //       }
    //     });
    //   }
    // });
    //
    // // this.token = this.store.select<any>(getElementsState).user(state => state.user).map(user => user.token);
    //
    // this.profile = this.store.select<any>(getElementsState).pipe(map(user => user.profile));
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }

  public scrollToDemos() {
    setTimeout(() => {
      window.scrollTo(0, 520);
    });
  }

  public changeLayout(menu, menuType, isRtl) {
    this.settings.menu = menu;
    this.settings.menuType = menuType;
    this.settings.rtl = isRtl;
    this.settings.theme = 'indigo-light';
  }

  public changeTheme(theme) {
    this.settings.theme = theme;
  }
}
