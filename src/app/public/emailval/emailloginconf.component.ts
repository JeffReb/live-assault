import { AfterViewInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Settings } from '../../app.settings.model';
import { User } from '../../models/user';
import { AppSettings } from '../../app.settings';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../store/app.states';
import { emailValidator } from '../../theme/utils/app-validators';
import { ReceipEmailConfirm } from '../../store/actions/auth.actions';

@Component({
  selector: 'app-emailloginconf',
  templateUrl: './emailloginconf.component.html'
})
export class EmailloginconfComponent implements AfterViewInit {
  public form: FormGroup;
  public settings: Settings;
  public tokenemail: string;

  user: User = new User();
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private store: Store<AppStates>
  ) {
    this.settings = this.appSettings.settings;
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required, emailValidator])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  public onSubmit(values: object): void {
    this.tokenemail = this.route.snapshot.paramMap.get('token');
    if (this.form.valid) {
      this.user = this.form.value;
      // console.warn(this.user);
      // console.warn(this.form.value);

      const payload = {
        email: this.user.email,
        password: this.user.password,
        tokenemail: this.tokenemail
      };

      // console.log('token url: ', this.tokenemail);
      this.store.pipe(select('appStates')).subscribe(data => {
        // console.log(data);
        this.message = data.errorMessage;
      });
      this.store.dispatch(new ReceipEmailConfirm(payload));
    }
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
    this.tokenemail = this.route.snapshot.paramMap.get('token');
    // console.log('token url: ', this.tokenemail);
  }
}
