import { AfterViewInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';
import { Router } from '@angular/router';
import { emailValidator, matchingPasswords } from '../../theme/utils/app-validators';
import { User } from '../../models/user';
import { AppStates } from '../../store/app.states';
import { select, Store } from '@ngrx/store';
import { SignUp } from '../../store/actions/auth.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements AfterViewInit {
  public form: FormGroup;
  public settings: Settings;

  user: User = new User();
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    private store: Store<AppStates>
  ) {
    this.settings = this.appSettings.settings;
    this.form = this.fb.group(
      {
        name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        email: [null, Validators.compose([Validators.required, emailValidator])],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required]
      },
      {
        validator: matchingPasswords('password', 'confirmPassword')
      }
    );
  }

  public onSubmit(values: object): void {
    if (this.form.valid) {
      this.user = this.form.value;
      // console.warn(this.user);
      // console.warn(this.form.value);

      const payload = {
        name: this.user.name,
        email: this.user.email,
        password: this.user.password
      };
      this.store.pipe(select('appStates')).subscribe(data => {
        // console.log(data);
        this.message = data.errorMessage;
      });
      this.store.dispatch(new SignUp(payload));
      // this.router.navigate(['/login']);
    }
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
