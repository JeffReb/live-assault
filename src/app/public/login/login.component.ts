import { AfterViewInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Settings } from '../../app.settings.model';
import { AppSettings } from '../../app.settings';
import { Router } from '@angular/router';
import { emailValidator } from '../../theme/utils/app-validators';
import { User } from '../../models/user';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../store/app.states';
import { LogIn } from '../../store/actions/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements AfterViewInit {
  public form: FormGroup;
  public settings: Settings;

  user: User = new User();
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    private store: Store<AppStates>
  ) {
    this.settings = this.appSettings.settings;
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required, emailValidator])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  public onSubmit(values: object): void {
    if (this.form.valid) {
      this.user = this.form.value;
      // console.warn(this.user);
      // console.warn(this.form.value);

      const payload = {
        email: this.user.email,
        password: this.user.password
      };
      this.store.pipe(select('appStates')).subscribe(data => {
        // console.log(data);
        this.message = data.errorMessage;
      });
      this.store.dispatch(new LogIn(payload));
      // this.router.navigate(['/']);
    }
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;
  }
}
