import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from '../services/auth.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor, TokenInterceptor } from '../services/token.interceptor';
import { ActivityGuardService, AuthGuardService, ValidateGuardService } from '../services/auth-guard.service';
import { RedirectGuardService } from '../services/redirect-guard.service';
import { HomeModule } from '../protected/home/home.module';
import { GeocodeInformService } from '../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../services/forms/geolocation/get-continent.service';
import { BandService } from '../services/activity/bands/band.service';
import { SiteService } from '../services/activity/sites/site.service';
import { ManageService } from '../services/activity/manage/manage.service';
import { OrgaService } from '../services/activity/orga/orga.service';
import { ServicesService } from '../services/activity/services/services.service';
import { EquipService } from '../services/activity/equip/equip.service';
import { ActivityService } from '../services/activity/activity.service';
import { TransfertActivityService } from '../services/activity/transfert.activity.service';
import { TransfertProfileService } from '../services/activity/transfert.profile.service';
import { AssetsServices } from '../protected/home/services/assets.services';

@NgModule({
  declarations: [],
  imports: [CommonModule, HomeModule],
  providers: [
    AuthService,
    AuthGuardService,
    ValidateGuardService,
    RedirectGuardService,
    ActivityGuardService,
    GeocodeInformService,
    GetContinentService,
    BandService,
    SiteService,
    ManageService,
    OrgaService,
    ServicesService,
    EquipService,
    ActivityService,
    TransfertActivityService,
    TransfertProfileService,
    AssetsServices,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. !!!');
    }
  }
}
