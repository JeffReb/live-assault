export class BandRead {
  bandReadid?: string;
  bandReaduserid?: string;
  bandReadname?: string;
  bandReadaddedDate?: string;
  bandReadcity?: string;
  bandReadcityalt1?: string;
  bandReadcityalt2?: string;
  bandReadcountry?: string;
  bandReadcountrycode?: string;
  bandReadcontinent?: string;
  bandReadlatitude?: string;
  bandReadlongitude?: string;
  bandReademail?: string;
  bandReadstyle?: string;
  bandReadrole?: string;
}
