export class OrgaRead {
  orgaReadid?: string;
  orgaReaduserid?: string;
  orgaReadname?: string;
  orgaReadaddedDate?: string;
  orgaReadaddress?: string;
  orgaReadcity?: string;
  orgaReadcityalt1?: string;
  orgaReadcityalt2?: string;
  orgaReadcountry?: string;
  orgaReadcountrycode?: string;
  orgaReadcontinent?: string;
  orgaReadlatitude?: string;
  orgaReadlongitude?: string;
  orgaReademail?: string;
  orgaReadphone?: string;
  orgaReadtype?: string;
  orgaReadstyle?: string;
  orgaReadstatus?: string;
}
