export class EquipRead {
  equipReadid?: string;
  equipReaduserid?: string;
  equipReadname?: string;
  equipReadaddedDate?: string;
  equipReadaddress?: string;
  equipReadcity?: string;
  equipReadcityalt1?: string;
  equipReadcityalt2?: string;
  equipReadcountry?: string;
  equipReadcountrycode?: string;
  equipReadcontinent?: string;
  equipReadlatitude?: string;
  equipReadlongitude?: string;
  equipReademail?: string;
  equipReadphone?: string;
  equipReadtype?: string;
}
