export class User {
  userId?: string;
  name?: string;
  email?: string;
  password?: string;
  token?: string;
  firstname?: string;
  lastname?: string;
  phone?: string;
  hasband?: boolean;
  hassite?: boolean;
  hasmanage?: boolean;
  hasorga?: boolean;
  hasservice?: boolean;
  hasequip?: boolean;
  activity?: object;
  profile?: object;
  addeddate?: string;
}
