export class ServiceRead {
  serviceReadid?: string;
  serviceReaduserid?: string;
  serviceReadname?: string;
  serviceReadaddedDate?: string;
  serviceReadaddress?: string;
  serviceReadcity?: string;
  serviceReadcityalt1?: string;
  serviceReadcityalt2?: string;
  serviceReadcountry?: string;
  serviceReadcountrycode?: string;
  serviceReadcontinent?: string;
  serviceReadlatitude?: string;
  serviceReadlongitude?: string;
  serviceReademail?: string;
  serviceReadphone?: string;
  serviceReadstyle?: string;
  serviceReadstatus?: string;
  serviceReadservice?: string;
}
