export class ActivitiesSearch {
  locationfilter?: string;
  locationdistance?: string;
  locationlat?: string;
  locationlon?: string;
  namefilter?: string;
  name?: string;
  activityfilter?: string;
  activities?: string;
  typefilter?: string;
  type?: string;
  stylefilter?: string;
  style?: string;
  resultsearch?: object;
}
