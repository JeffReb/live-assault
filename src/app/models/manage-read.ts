export class ManageRead {
  manageReadid?: string;
  manageReaduserid?: string;
  manageReadname?: string;
  manageReadaddedDate?: string;
  manageReadaddress?: string;
  manageReadcity?: string;
  manageReadcityalt1?: string;
  manageReadcityalt2?: string;
  manageReadcountry?: string;
  manageReadcountrycode?: string;
  manageReadcontinent?: string;
  manageReadlatitude?: string;
  manageReadlongitude?: string;
  manageReademail?: string;
  manageReadphone?: string;
  manageReadtype?: string;
  manageReadstyle?: string;
  manageReadstatus?: string;
}
