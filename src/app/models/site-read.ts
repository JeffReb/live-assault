export class SiteRead {
  siteReadid?: string;
  siteReaduserid?: string;
  siteReadname?: string;
  siteReadaddedDate?: string;
  siteReadaddress?: string;
  siteReadcity?: string;
  siteReadcityalt1?: string;
  siteReadcityalt2?: string;
  siteReadcountry?: string;
  siteReadcountrycode?: string;
  siteReadcontinent?: string;
  siteReadlatitude?: string;
  siteReadlongitude?: string;
  siteReademail?: string;
  siteReadphone?: string;
  siteReadtype?: string;
  siteReadstyle?: string;
  siteReadcapacity?: string;
}
