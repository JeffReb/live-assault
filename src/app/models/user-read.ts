export class UserRead {
  result?: {
    user?: {
      userId?: string;
      name?: string;
      email?: string;
      addeddate?: string;
      activitiesId?: string;
      authorStatus?: string;
    };
  }[];
}
