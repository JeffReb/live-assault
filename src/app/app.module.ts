import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AppSettings } from './app.settings';
import { VerticalMenuComponent } from './theme/components/menu/vertical-menu/vertical-menu.component';
import { ErrorComponent } from './public/errors/error/error.component';
import { NotFoundComponent } from './public/errors/not-found/not-found.component';
import { SidenavComponent } from './theme/components/sidenav/sidenav.component';
import { LoginComponent } from './public/login/login.component';
import { RegisterComponent } from './public/register/register.component';
import { HomepublicComponent } from './public/homepublic/homepublic.component';
import { DialogEmailvalComponent, EmailvalComponent } from './protected/validation/emailval/emailval.component';
import { ChooseplanComponent, DialogChooseplanComponent } from './protected/validation/chooseplan/chooseplan.component';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './store/effects/auth.effects';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducers/auth.reducers';
import { HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgrxRouterStoreModule } from './store/reducers/router/ngrx-router.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EmailloginconfComponent } from './public/emailval/emailloginconf.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './protected/home/home.module';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { environment } from '../environments/environment';

const SocketEndpoint = environment.SocketEndPoint;

const config: SocketIoConfig = { url: SocketEndpoint, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    VerticalMenuComponent,
    HomepublicComponent,
    LoginComponent,
    RegisterComponent,
    EmailvalComponent,
    DialogEmailvalComponent,
    EmailloginconfComponent,
    ChooseplanComponent,
    DialogChooseplanComponent,
    ErrorComponent,
    NotFoundComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    CoreModule,
    AppRoutingModule,
    HomeModule,
    BrowserAnimationsModule,
    SharedModule,
    FontAwesomeModule,
    HttpClientModule,
    NgrxRouterStoreModule,
    StoreModule.forRoot(
      { appStates: reducer },
      {
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
          strictActionSerializability: true,
          strictStateSerializability: true
        }
      }
    ),
    StoreModule.forFeature('elements', reducer),
    StoreDevtoolsModule.instrument({
      name: 'live-assault Devtools',
      maxAge: 15
    }),
    EffectsModule.forRoot([AuthEffects])
  ],
  entryComponents: [VerticalMenuComponent, DialogChooseplanComponent, DialogEmailvalComponent, EmailloginconfComponent],
  providers: [AppSettings],
  bootstrap: [AppComponent]
})
export class AppModule {}
