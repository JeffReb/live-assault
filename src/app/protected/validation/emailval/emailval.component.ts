import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../store/app.states';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { getAppUser } from '../../../store/selectors/user-state.selector';
import { CancelErrorMessage, SendEmailConfirm } from '../../../store/actions/auth.actions';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';

export interface DialogDataEmailVal {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-emailval',
  templateUrl: 'dialog-emailval.component.html'
})
export class DialogEmailvalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataEmailVal) {}

  onNoClick(): void {
    // console.log('onclick !!');
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-emailval',
  templateUrl: './emailval.component.html'
})
export class EmailvalComponent implements AfterViewInit, OnInit {
  public settings: Settings;
  email: string | null;

  message: string | null;

  public user: Observable<object> | null;
  public profile: Observable<object> | null;

  // user: User = new User();

  constructor(
    public appSettings: AppSettings,
    public router: Router,
    private store: Store<AppStates>,
    public dialog: MatDialog
  ) {
    this.settings = this.appSettings.settings;
  }

  goHome(): void {
    this.store.dispatch(new SendEmailConfirm('test'));
    this.store.pipe(select('appStates')).subscribe(data => {
      // console.log('data.isAuthenticated: ', data.isAuthenticated);
      // console.log('data: ', data);
      // console.log('data.user: ', data.user);
      // console.log('Store Subscribe errorMessage: ', data.errorMessage);
      this.message = data.errorMessage;
    });
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    const dialogRef = this.dialog.open(DialogEmailvalComponent, {
      disableClose: true,
      data: {
        messagedialog: 'Check your spam box if necessary'
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('result', result);
      this.store.dispatch(new SendEmailConfirm('test'));
      this.store.pipe(select('appStates')).subscribe(data => {
        // console.log('data.isAuthenticated: ', data.isAuthenticated);
        // console.log('data: ', data);
        // console.log('data.user: ', data.user);
        // console.log('Store Subscribe errorMessage: ', data.errorMessage);
        this.message = data.errorMessage;
      });
    });
  }

  ngOnInit() {
    // this.store.dispatch(new SendEmailConfirm('test')); // <= send before this in register component !!!!

    this.user = this.store.select<any>(getAppUser);
    this.user.subscribe(data => {
      if (!data) {
        // console.log('data-null');
        // console.log(data);
      } else {
        // console.log('data-not-null');
        // console.log('line 62 ', data);
        this.profile = this.store.select<any>(getAppUser).pipe(map(user => user.profile));
        this.profile.subscribe(dataprofile => {
          if (!dataprofile) {
            // console.log('data-null');
            // console.log(dataprofile);
          } else {
            // console.log('data-not-null');
            // console.log(dataprofile['userEmail']);
            this.email = dataprofile['userEmail'];
          }
        });
      }
    });

    // this.token = this.store.select<any>(getElementsState).user(state => state.user).map(user => user.token);

    // this.profile = this.store.select<any>(getElementsState).pipe(map(user => user.profile));

    // this.appError = this.store.select<any>(getAppErrors);
    // this.appError.subscribe(data => {
    //     if (!data) {
    //         console.log('appError lline85: ', data);
    //         this.message = null;
    //         console.log('appError lline87: ', this.message);
    //     } else {
    //         console.log('appError lline87: ', data);
    //         this.message = data.toString();
    //         console.log('appError lline87: ', this.message);
    //     }
    // });
  }
}
