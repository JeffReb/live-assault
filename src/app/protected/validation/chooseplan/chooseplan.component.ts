import { AfterViewInit, Component, Inject } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';
import { Router } from '@angular/router';
import { faFileContract, faHandHolding, faMapMarkedAlt, faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import PlaceResult = google.maps.places.PlaceResult;
import { emailValidator } from '../../../theme/utils/app-validators';
import { GeocodeInformService } from '../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../services/forms/geolocation/get-continent.service';
import { BandService } from '../../../services/activity/bands/band.service';
import { Band } from '../../../models/band';
import { AppStates } from '../../../store/app.states';
import { select, Store } from '@ngrx/store';
import {
  CancelErrorMessage,
  RegisterActivity,
  RegisterBand,
  RegisterEquip,
  RegisterManage,
  RegisterOrga,
  RegisterServices,
  RegisterSite
} from '../../../store/actions/auth.actions';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { getAppUser } from '../../../store/selectors/user-state.selector';
import { Observable } from 'rxjs';

declare var google;

export interface DialogDataChoosePlan {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-chooseplan.component',
  templateUrl: 'dialog-chooseplan.component.html'
})
export class DialogChooseplanComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataChoosePlan, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-chooseplan',
  templateUrl: './chooseplan.component.html',
  styleUrls: ['./chooseplan.component.scss']
})
export class ChooseplanComponent implements AfterViewInit {
  public formband: FormGroup;
  public formhall: FormGroup;
  public formorga: FormGroup;
  public formmanager: FormGroup;
  public formservice: FormGroup;
  public formequipement: FormGroup;
  public form: FormGroup;
  public settings: Settings;

  public formValuesBand: object | null;
  public formValuesHall: object | null;
  public formValuesManager: object | null;
  public formValuesOrga: object | null;
  public formValuesServices: object | null;
  public formValuesEquip: object | null;
  public formValuesActivity: object | null;

  messageband: string | null;
  messagehall: string | null;
  messagemanager: string | null;
  messageorga: string | null;
  messageservice: string | null;
  messageequip: string | null;
  messageactor: string | null;

  public isDisabledJoinUs = true;

  public isDisabledBand = false;
  public isDisabledHall = false;
  public isDisabledManager = false;
  public isDisabledOrga = false;
  public isDisabledService = false;
  public isDisabledEquip = false;

  public user: Observable<object> | null;

  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();

  languagenav: Array<string> = [this.languagecodenavigator];

  faFileContract = faFileContract;
  faMapMarkedAlt = faMapMarkedAlt;
  faHandHolding = faHandHolding;
  faMicrophone = faMicrophone;

  band: Band = new Band();
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private bandService: BandService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.form = this.fb.group({
        firstname: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        lastname: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        phone: [null, Validators.compose([Validators.required])]
      })),
      (this.formband = this.fb.group({
        nameband: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        style1band: [null, Validators.compose([Validators.required])],
        style2band: [null],
        style3band: [null],
        cityband: [null, Validators.compose([Validators.required])],
        emailband: [null, Validators.compose([emailValidator])],
        roleband: [null, Validators.compose([Validators.required])]
      })),
      (this.formhall = this.fb.group({
        namehall: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adresshall: [null, Validators.compose([Validators.required])],
        phonehall: [null, Validators.compose([Validators.required])],
        emailhall: [null, Validators.compose([Validators.required, emailValidator])],
        typehall: [null],
        capacityhall: [null]
      })),
      (this.formmanager = this.fb.group({
        namemanager: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressmanager: [null, Validators.compose([Validators.required])],
        phonemanager: [null, Validators.compose([Validators.required])],
        emailmanager: [null, Validators.compose([Validators.required, emailValidator])],
        typemanager: [null],
        statusmanager: [null]
      })),
      (this.formorga = this.fb.group({
        nameorga: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressorga: [null, Validators.compose([Validators.required])],
        phoneorga: [null, Validators.compose([Validators.required])],
        emailorga: [null, Validators.compose([Validators.required, emailValidator])],
        typeorga: [null],
        statusorga: [null]
      })),
      (this.formservice = this.fb.group({
        nameservice: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressservice: [null, Validators.compose([Validators.required])],
        phoneservice: [null, Validators.compose([Validators.required])],
        emailservice: [null, Validators.compose([Validators.required, emailValidator])],
        typeservice: [null],
        statusservice: [null]
      })),
      (this.formequipement = this.fb.group({
        nameequip: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressequip: [null, Validators.compose([Validators.required])],
        phoneequip: [null, Validators.compose([Validators.required])],
        emailequip: [null, Validators.compose([Validators.required, emailValidator])],
        typeequip: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitBand(values: object): void {
    if (this.formband.valid) {
      this.messageband = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeBand');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageband);
      this.formValuesBand = {
        nameband: this.formband.value.nameband,
        style1: this.formband.value.style1band,
        style2: this.formband.value.style2band,
        style3: this.formband.value.style3band,
        emailband: this.formband.value.emailband,
        roleband: this.formband.value.roleband,
        cityid: localStorage.getItem('callbackCityIdBand'),
        city: localStorage.getItem('callbackCityBand'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdBand'),
        cityalt2: localStorage.getItem('callbackCityAlt2Band'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdBand'),
        cityalt1: localStorage.getItem('callbackCityAlt1Band'),
        countryid: localStorage.getItem('callbackCountryIdBand'),
        country: localStorage.getItem('callbackCountryBand'),
        countrycode: localStorage.getItem('callbackCountryCodeBand'),
        continent,
        latBand: localStorage.getItem('latBand'),
        lngBand: localStorage.getItem('lngBand'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesBand);

      // console.warn(this.formValuesBand.value.cityid);
      const cityId = this.formValuesBand;
      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);
      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your city address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledBand = false;
        });
      } else {
        const payload = this.formValuesBand;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageband = data.errorMessage;
          // console.log(this.messageband);
          if (!this.messageband) {
            // console.log('!this.messageband: ', this.messageband);
            this.isDisabledJoinUs = false;
            this.isDisabledBand = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledBand = false;
            // console.log('this.messageband: ', this.messageband);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageband
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledBand = false;
            });
          }
        });
        this.store.dispatch(new RegisterBand(payload));
      }

      localStorage.removeItem('callbackCityIdBand');
      localStorage.removeItem('callbackCityBand');
      localStorage.removeItem('callbackCityAlt2IdBand');
      localStorage.removeItem('callbackCityAlt2Band');
      localStorage.removeItem('callbackCityAlt1IdBand');
      localStorage.removeItem('callbackCityAlt1Band');
      localStorage.removeItem('callbackCountryIdBand');
      localStorage.removeItem('callbackCountryBand');
      localStorage.removeItem('callbackCountryCodeBand');
      localStorage.removeItem('latBand');
      localStorage.removeItem('lngBand');
      localStorage.removeItem('AdressBand');
    }
  }

  public onSubmitHall(values: object): void {
    if (this.formhall.valid) {
      this.messagehall = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeHall');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messagehall);
      this.formValuesHall = {
        namehall: this.formhall.value.namehall,
        adresshall: localStorage.getItem('AdressHall'),
        phonehall: this.formhall.value.phonehall,
        emailhall: this.formhall.value.emailhall,
        typehall: this.formhall.value.typehall,
        capacityhall: this.formhall.value.capacityhall,
        cityid: localStorage.getItem('callbackCityIdHall'),
        city: localStorage.getItem('callbackCityHall'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdHall'),
        cityalt2: localStorage.getItem('callbackCityAlt2Hall'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdHall'),
        cityalt1: localStorage.getItem('callbackCityAlt1Hall'),
        countryid: localStorage.getItem('callbackCountryIdHall'),
        country: localStorage.getItem('callbackCountryHall'),
        countrycode: localStorage.getItem('callbackCountryCodeHall'),
        continent,
        latHall: localStorage.getItem('latHall'),
        lngHall: localStorage.getItem('lngHall'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesHall);

      const cityId = this.formValuesHall;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);
      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledHall = false;
        });
      } else {
        const payload = this.formValuesHall;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messagehall = data.errorMessage;
          // console.log(this.messagehall);
          if (!this.messagehall) {
            // console.log('!this.messageband: ', this.messagehall);
            this.isDisabledJoinUs = false;
            this.isDisabledHall = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledHall = false;
            // console.log('this.messageband: ', this.messagehall);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messagehall
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledHall = false;
            });
          }
        });
        this.store.dispatch(new RegisterSite(payload));
      }

      localStorage.removeItem('callbackCityIdHall');
      localStorage.removeItem('callbackCityHall');
      localStorage.removeItem('callbackCityAlt2IdHall');
      localStorage.removeItem('callbackCityAlt2Hall');
      localStorage.removeItem('callbackCityAlt1IdHall');
      localStorage.removeItem('callbackCityAlt1Hall');
      localStorage.removeItem('callbackCountryIdHall');
      localStorage.removeItem('callbackCountryHall');
      localStorage.removeItem('callbackCountryCodeHall');
      localStorage.removeItem('latHall');
      localStorage.removeItem('lngHall');
      localStorage.removeItem('AdressHall');
    }
  }

  public onSubmitManager(values: object): void {
    if (this.formmanager.valid) {
      this.messagemanager = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeManager');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messagemanager);
      this.formValuesManager = {
        namemanage: this.formmanager.value.namemanager,
        adressmanage: localStorage.getItem('AdressManager'),
        phonemanage: this.formmanager.value.phonemanager,
        emailmanage: this.formmanager.value.emailmanager,
        typemanage: this.formmanager.value.typemanager,
        statusmanage: this.formmanager.value.statusmanager,
        cityid: localStorage.getItem('callbackCityIdManager'),
        city: localStorage.getItem('callbackCityManager'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdManager'),
        cityalt2: localStorage.getItem('callbackCityAlt2Manager'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdManager'),
        cityalt1: localStorage.getItem('callbackCityAlt1Manager'),
        countryid: localStorage.getItem('callbackCountryIdManager'),
        country: localStorage.getItem('callbackCountryManager'),
        countrycode: localStorage.getItem('callbackCountryCodeManager'),
        continent,
        latManage: localStorage.getItem('latManager'),
        lngManage: localStorage.getItem('lngManager'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesManager);

      const cityId = this.formValuesManager;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);

      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledManager = false;
        });
      } else {
        const payload = this.formValuesManager;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messagemanager = data.errorMessage;
          // console.log(this.messagemanager);
          if (!this.messagemanager) {
            // console.log('!this.messageband: ', this.messagemanager);
            this.isDisabledJoinUs = false;
            this.isDisabledManager = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledManager = false;
            // console.log('this.messageband: ', this.messagemanager);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messagemanager
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledManager = false;
            });
          }
        });
        this.store.dispatch(new RegisterManage(payload));
      }

      localStorage.removeItem('callbackCityIdManager');
      localStorage.removeItem('callbackCityManager');
      localStorage.removeItem('callbackCityAlt2IdManager');
      localStorage.removeItem('callbackCityAlt2Manager');
      localStorage.removeItem('callbackCityAlt1IdManager');
      localStorage.removeItem('callbackCityAlt1Manager');
      localStorage.removeItem('callbackCountryIdManager');
      localStorage.removeItem('callbackCountryManager');
      localStorage.removeItem('callbackCountryCodeManager');
      localStorage.removeItem('latManager');
      localStorage.removeItem('lngManager');
      localStorage.removeItem('AdressManager');
    }
  }

  public onSubmitOrga(values: object): void {
    if (this.formorga.valid) {
      this.messageorga = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeOrga');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageorga);
      this.formValuesOrga = {
        nameorga: this.formorga.value.nameorga,
        adressorga: localStorage.getItem('AdressOrga'),
        phoneorga: this.formorga.value.phoneorga,
        emailorga: this.formorga.value.emailorga,
        typeorga: this.formorga.value.typeorga,
        statusorga: this.formorga.value.statusorga,
        cityid: localStorage.getItem('callbackCityIdOrga'),
        city: localStorage.getItem('callbackCityOrga'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdOrga'),
        cityalt2: localStorage.getItem('callbackCityAlt2Orga'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdOrga'),
        cityalt1: localStorage.getItem('callbackCityAlt1Orga'),
        countryid: localStorage.getItem('callbackCountryIdOrga'),
        country: localStorage.getItem('callbackCountryOrga'),
        countrycode: localStorage.getItem('callbackCountryCodeOrga'),
        continent,
        latOrga: localStorage.getItem('latOrga'),
        lngOrga: localStorage.getItem('lngOrga'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesOrga);

      const cityId = this.formValuesOrga;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);

      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledOrga = false;
        });
      } else {
        const payload = this.formValuesOrga;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageorga = data.errorMessage;
          // console.log(this.messageorga);
          if (!this.messageorga) {
            console.log('!this.messageband: ', this.messageorga);
            this.isDisabledJoinUs = false;
            this.isDisabledOrga = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledOrga = false;
            // console.log('this.messageband: ', this.messageorga);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageorga
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledOrga = false;
            });
          }
        });
        this.store.dispatch(new RegisterOrga(payload));
      }

      localStorage.removeItem('callbackCityIdOrga');
      localStorage.removeItem('callbackCityOrga');
      localStorage.removeItem('callbackCityAlt2IdOrga');
      localStorage.removeItem('callbackCityAlt2Orga');
      localStorage.removeItem('callbackCityAlt1IdOrga');
      localStorage.removeItem('callbackCityAlt1Orga');
      localStorage.removeItem('callbackCountryIdOrga');
      localStorage.removeItem('callbackCountryOrga');
      localStorage.removeItem('callbackCountryCodeOrga');
      localStorage.removeItem('latOrga');
      localStorage.removeItem('lngOrga');
      localStorage.removeItem('AdressOrga');
    }
  }

  public onSubmitService(values: object): void {
    if (this.formservice.valid) {
      this.messageservice = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeService');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageservice);
      this.formValuesServices = {
        nameservices: this.formservice.value.nameservice,
        adressservices: localStorage.getItem('AdressService'),
        phoneservices: this.formservice.value.phoneservice,
        emailservices: this.formservice.value.emailservice,
        servicesservices: this.formservice.value.typeservice,
        statusservices: this.formservice.value.statusservice,
        cityid: localStorage.getItem('callbackCityIdService'),
        city: localStorage.getItem('callbackCityService'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdService'),
        cityalt2: localStorage.getItem('callbackCityAlt2Service'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdService'),
        cityalt1: localStorage.getItem('callbackCityAlt1Service'),
        countryid: localStorage.getItem('callbackCountryIdService'),
        country: localStorage.getItem('callbackCountryService'),
        countrycode: localStorage.getItem('callbackCountryCodeService'),
        continent,
        latServices: localStorage.getItem('latService'),
        lngServices: localStorage.getItem('lngService'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesServices);

      const cityId = this.formValuesServices;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);

      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledService = false;
        });
      } else {
        const payload = this.formValuesServices;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageservice = data.errorMessage;
          // console.log(this.messageservice);
          if (!this.messageservice) {
            // console.log('!this.messageband: ', this.messageservice);
            this.isDisabledJoinUs = false;
            this.isDisabledService = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledService = false;
            // console.log('this.messageband: ', this.messageservice);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageservice
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledService = false;
            });
          }
        });
        this.store.dispatch(new RegisterServices(payload));
      }

      localStorage.removeItem('callbackCityIdService');
      localStorage.removeItem('callbackCityService');
      localStorage.removeItem('callbackCityAlt2IdService');
      localStorage.removeItem('callbackCityAlt2Service');
      localStorage.removeItem('callbackCityAlt1IdService');
      localStorage.removeItem('callbackCityAlt1Service');
      localStorage.removeItem('callbackCountryIdService');
      localStorage.removeItem('callbackCountryService');
      localStorage.removeItem('callbackCountryCodeService');
      localStorage.removeItem('latService');
      localStorage.removeItem('lngService');
      localStorage.removeItem('AdressService');
    }
  }

  public onSubmitEquipement(values: object): void {
    if (this.formequipement.valid) {
      this.messageequip = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeEquip');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageequip);
      this.formValuesEquip = {
        nameequip: this.formequipement.value.nameequip,
        adressequip: localStorage.getItem('AdressEquip'),
        phoneequip: this.formequipement.value.phoneequip,
        emailequip: this.formequipement.value.emailequip,
        typeequip: this.formequipement.value.typeequip,
        cityid: localStorage.getItem('callbackCityIdEquip'),
        city: localStorage.getItem('callbackCityEquip'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdEquip'),
        cityalt2: localStorage.getItem('callbackCityAlt2Equip'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdEquip'),
        cityalt1: localStorage.getItem('callbackCityAlt1Equip'),
        countryid: localStorage.getItem('callbackCountryIdEquip'),
        country: localStorage.getItem('callbackCountryEquip'),
        countrycode: localStorage.getItem('callbackCountryCodeEquip'),
        continent,
        latEquip: localStorage.getItem('latEquip'),
        lngEquip: localStorage.getItem('lngEquip'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesEquip);

      const cityId = this.formValuesEquip;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);

      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
          this.isDisabledJoinUs = true;
          this.isDisabledEquip = false;
        });
      } else {
        const payload = this.formValuesEquip;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageequip = data.errorMessage;
          // console.log(this.messageequip);
          if (!this.messageequip) {
            // console.log('!this.messageband: ', this.messageequip);
            this.isDisabledJoinUs = false;
            this.isDisabledEquip = true;
          } else {
            this.isDisabledJoinUs = true;
            this.isDisabledEquip = false;
            // console.log('this.messageband: ', this.messageequip);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageequip
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
              this.isDisabledJoinUs = true;
              this.isDisabledEquip = false;
            });
          }
        });
        this.store.dispatch(new RegisterEquip(payload));
      }

      localStorage.removeItem('callbackCityIdEquip');
      localStorage.removeItem('callbackCityEquip');
      localStorage.removeItem('callbackCityAlt2IdEquip');
      localStorage.removeItem('callbackCityAlt2Equip');
      localStorage.removeItem('callbackCityAlt1IdEquip');
      localStorage.removeItem('callbackCityAlt1Equip');
      localStorage.removeItem('callbackCountryIdEquip');
      localStorage.removeItem('callbackCountryEquip');
      localStorage.removeItem('callbackCountryCodeEquip');
      localStorage.removeItem('latEquip');
      localStorage.removeItem('lngEquip');
      localStorage.removeItem('AdressEquip');
    }
  }

  public onSubmit(values: object): void {
    if (this.form.valid) {
      this.messageactor = 'testerror2';
      this.formValuesActivity = {
        firstname: this.form.value.firstname,
        lastname: this.form.value.lastname,
        phone: this.form.value.phone
      };
      const payload = this.formValuesActivity;
      this.store.dispatch(new RegisterActivity(payload));
    }
  }

  onAutocompleteSelectedBand(resultBand: PlaceResult) {
    // ('onAddressSelected: ', resultBand.address_components);
    // console.log('onAddressSelected: ', resultBand.place_id);
    const lat = resultBand.geometry.location.lat();
    const long = resultBand.geometry.location.lng();
    localStorage.setItem('latBand', lat.toString());
    localStorage.setItem('lngBand', long.toString());
    localStorage.setItem('AdressBand', resultBand.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Band']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Band');
    // console.warn('resultBandonAutocomplete: ', this.newlatlng);
    // console.warn('resultBandonAutocomplete: ', this.newlatlng[2]);
  }

  onAutocompleteSelectedHall(resultHall: PlaceResult) {
    // console.log('onAddressSelected: ', resultHall.address_components);
    // console.log('onAddressSelected: ', resultHall.id);
    const lat = resultHall.geometry.location.lat();
    const long = resultHall.geometry.location.lng();
    localStorage.setItem('latHall', lat.toString());
    localStorage.setItem('lngHall', long.toString());
    localStorage.setItem('AdressHall', resultHall.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Hall']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Hall');
  }

  onAutocompleteSelectedManager(resultManager: PlaceResult) {
    // console.log('onAddressSelected: ', resultManager.address_components);
    // console.log('onAddressSelected: ', resultManager.id);
    const lat = resultManager.geometry.location.lat();
    const long = resultManager.geometry.location.lng();
    localStorage.setItem('latManager', lat.toString());
    localStorage.setItem('lngManager', long.toString());
    localStorage.setItem('AdressManager', resultManager.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Manager']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Manager');
  }

  onAutocompleteSelectedOrga(resultOrga: PlaceResult) {
    // console.log('onAddressSelected: ', resultOrga.address_components);
    // console.log('onAddressSelected: ', resultOrga.id);
    const lat = resultOrga.geometry.location.lat();
    const long = resultOrga.geometry.location.lng();
    localStorage.setItem('latOrga', lat.toString());
    localStorage.setItem('lngOrga', long.toString());
    localStorage.setItem('AdressOrga', resultOrga.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Orga']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Orga');
  }

  onAutocompleteSelectedService(resultService: PlaceResult) {
    // console.log('onAddressSelected: ', resultService.address_components);
    // console.log('onAddressSelected: ', resultService.id);
    const lat = resultService.geometry.location.lat();
    const long = resultService.geometry.location.lng();
    localStorage.setItem('latService', lat.toString());
    localStorage.setItem('lngService', long.toString());
    localStorage.setItem('AdressService', resultService.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Service']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Service');
  }

  onAutocompleteSelectedEquip(resultEquip: PlaceResult) {
    // console.log('onAddressSelected: ', resultEquip.address_components);
    // console.log('onAddressSelected: ', resultEquip.id);
    const lat = resultEquip.geometry.location.lat();
    const long = resultEquip.geometry.location.lng();
    localStorage.setItem('latEquip', lat.toString());
    localStorage.setItem('lngEquip', long.toString());
    localStorage.setItem('AdressEquip', resultEquip.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Equip']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Equip');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    this.isDisabledJoinUs = true;
    this.isDisabledBand = false;
    this.isDisabledHall = false;
    this.isDisabledManager = false;
    this.isDisabledOrga = false;
    this.isDisabledService = false;
    this.isDisabledEquip = false;

    this.user = this.store.select<any>(getAppUser);
    this.user.subscribe(data => {
      if (!data) {
        // console.log('data-null');
        // console.log(data);
      } else {
        const hasBand = data['hasband'];
        const hasSite = data['hassite'];
        const hasManage = data['hasmanage'];
        const hasOrga = data['hasorga'];
        const hasService = data['hasservice'];
        const hasEquip = data['hasequip'];
        if (hasBand === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledBand = true;
        }
        if (hasSite === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledHall = true;
        }
        if (hasManage === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledManager = true;
        }
        if (hasOrga === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledOrga = true;
        }
        if (hasService === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledService = true;
        }
        if (hasEquip === true) {
          this.isDisabledJoinUs = false;
          this.isDisabledEquip = true;
        }
        // console.log('data-not-null');
        // console.log('line 521 ', hasBand);
      }
    });

    // console.log('test', this.languagecodenavigator);

    localStorage.removeItem('callbackCityIdBand');
    localStorage.removeItem('callbackCityBand');
    localStorage.removeItem('callbackCityAlt2IdBand');
    localStorage.removeItem('callbackCityAlt2Band');
    localStorage.removeItem('callbackCityAlt1IdBand');
    localStorage.removeItem('callbackCityAlt1Band');
    localStorage.removeItem('callbackCountryIdBand');
    localStorage.removeItem('callbackCountryBand');
    localStorage.removeItem('callbackCountryCodeBand');
    localStorage.removeItem('latBand');
    localStorage.removeItem('lngBand');
    localStorage.removeItem('AdressBand');
    localStorage.removeItem('callbackCityIdHall');
    localStorage.removeItem('callbackCityHall');
    localStorage.removeItem('callbackCityAlt2IdHall');
    localStorage.removeItem('callbackCityAlt2Hall');
    localStorage.removeItem('callbackCityAlt1IdHall');
    localStorage.removeItem('callbackCityAlt1Hall');
    localStorage.removeItem('callbackCountryIdHall');
    localStorage.removeItem('callbackCountryHall');
    localStorage.removeItem('callbackCountryCodeHall');
    localStorage.removeItem('latHall');
    localStorage.removeItem('lngHall');
    localStorage.removeItem('AdressHall');
    localStorage.removeItem('callbackCityIdManager');
    localStorage.removeItem('callbackCityManager');
    localStorage.removeItem('callbackCityAlt2IdManager');
    localStorage.removeItem('callbackCityAlt2Manager');
    localStorage.removeItem('callbackCityAlt1IdManager');
    localStorage.removeItem('callbackCityAlt1Manager');
    localStorage.removeItem('callbackCountryIdManager');
    localStorage.removeItem('callbackCountryManager');
    localStorage.removeItem('callbackCountryCodeManager');
    localStorage.removeItem('latManager');
    localStorage.removeItem('lngManager');
    localStorage.removeItem('AdressManager');
    localStorage.removeItem('callbackCityIdOrga');
    localStorage.removeItem('callbackCityOrga');
    localStorage.removeItem('callbackCityAlt2IdOrga');
    localStorage.removeItem('callbackCityAlt2Orga');
    localStorage.removeItem('callbackCityAlt1IdOrga');
    localStorage.removeItem('callbackCityAlt1Orga');
    localStorage.removeItem('callbackCountryIdOrga');
    localStorage.removeItem('callbackCountryOrga');
    localStorage.removeItem('callbackCountryCodeOrga');
    localStorage.removeItem('latOrga');
    localStorage.removeItem('lngOrga');
    localStorage.removeItem('AdressOrga');
    localStorage.removeItem('callbackCityIdService');
    localStorage.removeItem('callbackCityService');
    localStorage.removeItem('callbackCityAlt2IdService');
    localStorage.removeItem('callbackCityAlt2Service');
    localStorage.removeItem('callbackCityAlt1IdService');
    localStorage.removeItem('callbackCityAlt1Service');
    localStorage.removeItem('callbackCountryIdService');
    localStorage.removeItem('callbackCountryService');
    localStorage.removeItem('callbackCountryCodeService');
    localStorage.removeItem('latService');
    localStorage.removeItem('lngService');
    localStorage.removeItem('AdressService');
    localStorage.removeItem('callbackCityIdEquip');
    localStorage.removeItem('callbackCityEquip');
    localStorage.removeItem('callbackCityAlt2IdEquip');
    localStorage.removeItem('callbackCityAlt2Equip');
    localStorage.removeItem('callbackCityAlt1IdEquip');
    localStorage.removeItem('callbackCityAlt1Equip');
    localStorage.removeItem('callbackCountryIdEquip');
    localStorage.removeItem('callbackCountryEquip');
    localStorage.removeItem('callbackCountryCodeEquip');
    localStorage.removeItem('latEquip');
    localStorage.removeItem('lngEquip');
    localStorage.removeItem('AdressEquip');
  }
}
