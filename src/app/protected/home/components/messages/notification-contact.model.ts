export class NotificationContactModel {
  notificationContact?: {
    fromUser?: string;
    noteDate?: string;
    noteCheck?: boolean;
    noteType?: string;
  }[];
}
