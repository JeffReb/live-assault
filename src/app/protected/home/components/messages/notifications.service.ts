import { environment } from '../../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotificationsModel } from './notifications.model';
import { NotificationContactModel } from './notification-contact.model';
import { NotificationMessageModel } from './notification-message.model';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  checkNotificationContact(fromUserId): Observable<NotificationContactModel> {
    const url = `${this.BASE_URL}/notification/contact/check`;
    return this.https.post<NotificationContactModel>(url, { fromUserId });
  }

  readNotificationsContact(): Observable<NotificationContactModel> {
    const url = `${this.BASE_URL}/notification/contact/read`;
    return this.https.get<NotificationContactModel>(url);
  }

  checkNotificationMessage(fromUserId): Observable<NotificationMessageModel> {
    const url = `${this.BASE_URL}/notification/message/check`;
    return this.https.post<NotificationMessageModel>(url, { fromUserId });
  }

  readNotificationsMessage(): Observable<NotificationMessageModel> {
    const url = `${this.BASE_URL}/notification/message/read`;
    return this.https.get<NotificationMessageModel>(url);
  }
}
