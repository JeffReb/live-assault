import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MatMenuTrigger, MatSnackBar } from '@angular/material';
import { MessagesService } from './messages.service';
import { NotificationsService } from './notifications.service';
import { AssetsServices } from '../../services/assets.services';
import { UserService } from '../../../../services/users/user.service';
import { environment } from '../../../../../environments/environment';
import { TransfertActivityService } from '../../../../services/activity/transfert.activity.service';
import { Router } from '@angular/router';
import { SocketIoService } from '../../../../services/sockets/socket-io.service';

const APICloud = environment.APICloud;

const notificationDataTableContact: {
  image: string;
  name: string;
  date: string;
  text: string;
  checked: boolean;
  id: string;
}[] = [];
const sortNotificationDataTableContact: {
  image: string;
  name: string;
  date: string;
  text: string;
  checked: boolean;
  id: string;
}[] = [];
const notificationDataTableMessage: {
  image: string;
  name: string;
  date: string;
  text: string;
  checked: boolean;
  id: string;
}[] = [];
const sortNotificationDataTableMessage: {
  image: string;
  name: string;
  date: string;
  text: string;
  checked: boolean;
  id: string;
}[] = [];
const notificationDataTableFollowers: {
  image: string;
  name: string;
  date: string;
  text: string;
  checked: boolean;
  id: string;
}[] = [];

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MessagesService]
})
export class MessagesComponent implements OnInit {
  @ViewChild(MatMenuTrigger, { static: true }) trigger: MatMenuTrigger;
  // selectedTab: number;
  public selectedTab = 1;
  public messages: Array<object>;
  public files: Array<object>;
  public meetings: Array<object>;
  public badgeCount: number;
  public badgeContactCount: number;
  public badgeMessageCount: number;
  public notePresContact: boolean;
  public notePresMessage: boolean;
  public notePresFollowers: boolean;
  public noteName: string;
  public noteContactName: string;
  public noteMessageName: string;
  public noteDate: string;
  public noteText: string;
  public noteChecked: boolean;
  public noteFromId: string;
  public noteContactFromId: string;
  public noteMessageFromId: string;
  public noteType: string;

  noteContactTable = notificationDataTableContact;
  sortNoteContactTable = sortNotificationDataTableContact;
  sortNoteMessageTable = sortNotificationDataTableMessage;
  noteMessageTable = notificationDataTableMessage;
  noteFollowersTable = notificationDataTableFollowers;
  constructor(
    private messagesService: MessagesService,
    private notificationsService: NotificationsService,
    private assetsService: AssetsServices,
    private userService: UserService,
    private transfertActivityService: TransfertActivityService,
    private router: Router,
    private socketIoService: SocketIoService,
    private snackBar: MatSnackBar
  ) {
    this.messages = messagesService.getMessages();
    this.files = messagesService.getFiles();
    this.meetings = messagesService.getMeetings();
    this.badgeCount = 0;
    this.badgeContactCount = 0;
    this.badgeMessageCount = 0;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.noteContactTable = [];
    this.noteMessageTable = [];
    this.noteFollowersTable = [];
    this.sortNoteContactTable = [];
    this.sortNoteMessageTable = [];
    this.socketIoService.getMessages().subscribe((message: any) => {
      // console.log('new message socket: ', message.mess);
      if (message.noteType === 'accept' || message.noteType === 'invitation') {
        const snackBarRef = this.snackBar.open(message.mess, '', { duration: 7000, horizontalPosition: 'end' });
        this.badgeCount++;
        this.badgeContactCount++;
        this.notePresContact = true;
        const talkDate = new Date().toDateString();
        const timstampStr = Math.round(new Date().getTime()).toString();
        this.assetsService.getImgUser(message.idFrom).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              const noteImage = '../assets/img/users/user.png';
              this.userService.readUser(message.idFrom).subscribe(datauser => {
                if (!datauser) {
                } else {
                  // console.log('data name user: ', data.name);
                  this.noteName = datauser.result[0].user.name;
                  const noteName = datauser.result[0].user.name;
                  this.noteContactTable.push({
                    image: noteImage,
                    name: noteName,
                    date: timstampStr,
                    text: message.noteType,
                    checked: false,
                    id: message.idFrom
                  });
                }
              });
            } else {
              const noteImage = APICloud + '/' + message.idFrom + '/' + Filename;
              this.userService.readUser(message.idFrom).subscribe(datauser => {
                if (!datauser) {
                } else {
                  // console.log('data name user: ', data.name);
                  this.noteName = datauser.result[0].user.name;
                  const noteName = datauser.result[0].user.name;
                  this.noteContactTable.push({
                    image: noteImage,
                    name: noteName,
                    date: timstampStr,
                    text: message.noteType,
                    checked: false,
                    id: message.idFrom
                  });
                }
              });
            }
          }
        });
      }
    });

    this.notificationsService.readNotificationsContact().subscribe(data => {
      if (!data || data.notificationContact.length === 0) {
        this.notePresContact = false;
      } else {
        this.notePresContact = true;
        const noteLength = data.notificationContact.length;
        for (let i = 0; i < noteLength; i++) {
          this.noteContactFromId = data.notificationContact[i].fromUser;
          const FromId = data.notificationContact[i].fromUser;
          const noteDate = data.notificationContact[i].noteDate;
          const noteChecked = data.notificationContact[i].noteCheck;
          const noteText = data.notificationContact[i].noteType;
          if (noteChecked === false) {
            this.badgeCount++;
            this.badgeContactCount++;
          }
          this.assetsService.getImgUser(FromId).subscribe(dataImg => {
            if (!dataImg) {
            } else {
              const Filename = dataImg.filename;
              if (Filename === 'ImgProfile not found') {
                const noteImage = '../assets/img/users/user.png';
                this.userService.readUser(FromId).subscribe(dataUser => {
                  if (!dataUser) {
                  } else {
                    this.noteContactName = dataUser.result[0].user.name;
                    const noteName = dataUser.result[0].user.name;
                    this.noteContactTable.push({
                      image: noteImage,
                      name: noteName,
                      date: noteDate,
                      text: noteText,
                      checked: noteChecked,
                      id: FromId
                    });
                  }
                });
              } else {
                const noteImage = APICloud + '/' + FromId + '/' + Filename;
                this.userService.readUser(FromId).subscribe(dataUser => {
                  if (!dataUser) {
                  } else {
                    this.noteContactName = dataUser.result[0].user.name;
                    const noteName = dataUser.result[0].user.name;
                    this.noteContactTable.push({
                      image: noteImage,
                      name: noteName,
                      date: noteDate,
                      text: noteText,
                      checked: noteChecked,
                      id: FromId
                    });
                  }
                });
              }
            }
          });
        }
      }
    });

    this.notificationsService.readNotificationsMessage().subscribe(data => {
      if (!data || data.notificationMessage.length === 0) {
        this.notePresMessage = false;
      } else {
        this.notePresMessage = true;
        const noteLength = data.notificationMessage.length;
        for (let i = 0; i < noteLength; i++) {
          this.noteMessageFromId = data.notificationMessage[i].fromUser;
          const FromId = data.notificationMessage[i].fromUser;
          const noteDate = data.notificationMessage[i].noteDate;
          const noteChecked = data.notificationMessage[i].noteCheck;
          const noteText = data.notificationMessage[i].noteType;
          if (noteChecked === false) {
            this.badgeCount++;
            this.badgeMessageCount++;
          }
          this.assetsService.getImgUser(FromId).subscribe(dataImg => {
            if (!dataImg) {
            } else {
              const Filename = dataImg.filename;
              if (Filename === 'ImgProfile not found') {
                const noteImage = '../assets/img/users/user.png';
                this.userService.readUser(FromId).subscribe(dataUser => {
                  if (!dataUser) {
                  } else {
                    this.noteMessageName = dataUser.result[0].user.name;
                    const noteName = dataUser.result[0].user.name;
                    this.noteMessageTable.push({
                      image: noteImage,
                      name: noteName,
                      date: noteDate,
                      text: noteText,
                      checked: noteChecked,
                      id: FromId
                    });
                  }
                });
              } else {
                const noteImage = APICloud + '/' + FromId + '/' + Filename;
                this.userService.readUser(FromId).subscribe(dataUser => {
                  if (!dataUser) {
                  } else {
                    this.noteMessageName = dataUser.result[0].user.name;
                    const noteName = dataUser.result[0].user.name;
                    this.noteMessageTable.push({
                      image: noteImage,
                      name: noteName,
                      date: noteDate,
                      text: noteText,
                      checked: noteChecked,
                      id: FromId
                    });
                  }
                });
              }
            }
          });
        }
      }
    });

    // setTimeout(() => {
    //     this.sortNoteContactTable = this.noteContactTable.sort((a, b) =>
    //         a.date < b.date ? -1 : a.date > b.date ? 1 : 0
    //     );
    //     console.log('sortNoteContactTable: ', this.sortNoteContactTable);
    // }, 1000);
  }

  onClickNoteContact(event, FromId) {
    const urlUser = '/home/read-user?id=' + FromId;
    this.notificationsService.checkNotificationContact(FromId).subscribe(data => {
      if (!data) {
      } else {
        // console.log(data);
      }
    });
    this.router.navigateByUrl(urlUser);
  }

  onClickNoteMessage(event, FromId) {
    const urlUser = '/home/chat?id=' + FromId;
    this.notificationsService.checkNotificationMessage(FromId).subscribe(data => {
      if (!data) {
      } else {
        // console.log(data);
      }
    });
    this.router.navigateByUrl(urlUser);
  }

  openMessagesMenu() {
    this.trigger.openMenu();
    this.selectedTab = 0;
    // console.log('on_open');
    this.badgeCount = 0;
    // this.badgeContactCount = 0;
    // this.badgeMessageCount = 0;
    this.sortNoteContactTable = this.noteContactTable.sort((b, a) => (a.date < b.date ? -1 : a.date > b.date ? 1 : 0));
    // console.log('sortNoteContactTable: ', this.sortNoteContactTable);
    this.sortNoteMessageTable = this.noteMessageTable.sort((b, a) => (a.date < b.date ? -1 : a.date > b.date ? 1 : 0));
    // console.log('sortNoteMessageTable: ', this.sortNoteMessageTable);
  }

  onMouseLeave() {
    this.trigger.closeMenu();
  }

  stopClickPropagate(event: any) {
    event.stopPropagation();
    event.preventDefault();
  }
}
