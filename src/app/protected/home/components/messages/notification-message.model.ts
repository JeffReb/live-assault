export class NotificationMessageModel {
  notificationMessage?: {
    fromUser?: string;
    noteDate?: string;
    noteCheck?: boolean;
    noteType?: string;
  }[];
}
