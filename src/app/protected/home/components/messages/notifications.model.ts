export class NotificationsModel {
  notification?: {
    noteId?: string;
    fromUser?: string;
    noteDate?: string;
    noteType?: string;
    noteCheck?: boolean;
  }[];
}
