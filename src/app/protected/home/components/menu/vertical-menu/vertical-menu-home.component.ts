import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { AppSettings } from '../../../../../app.settings';
import { NavigationEnd, Router } from '@angular/router';
import { MenuHomeService } from '../menu-home.service';

@Component({
  selector: 'app-vertical-menu-home',
  templateUrl: './vertical-menu-home.component.html',
  styleUrls: ['./vertical-menu-home.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MenuHomeService]
})
export class VerticalMenuHomeComponent implements OnInit, AfterViewInit {
  @Input('menuItems') menuItems;
  @Input('menuParentId') menuParentId;
  @Output() ClickMenuItem: EventEmitter<any> = new EventEmitter<any>();
  parentMenu: Array<any>;
  public settings: Settings;
  constructor(public appSettings: AppSettings, public menuService: MenuHomeService, public router: Router) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this.parentMenu = this.menuItems.filter(item => item.parentId === this.menuParentId);
  }

  ngAfterViewInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.settings.fixedHeader) {
          const mainContent = document.getElementById('main-content');
          if (mainContent) {
            mainContent.scrollTop = 0;
          }
        } else {
          document.getElementsByClassName('mat-drawer-content')[0].scrollTop = 0;
        }
      }
    });
  }

  onClick(menuId) {
    this.menuService.toggleMenuItem(menuId);
    this.menuService.closeOtherSubMenus(this.menuItems, menuId);
    this.ClickMenuItem.emit(menuId);
  }
}
