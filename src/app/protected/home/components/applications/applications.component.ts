import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ApplicationsComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  createBand(): void {
    this.router.navigateByUrl('/home/create-band');
  }

  createSite(): void {
    this.router.navigateByUrl('/home/create-site');
  }

  createManage(): void {
    this.router.navigateByUrl('/home/create-manage');
  }

  createOrga(): void {
    this.router.navigateByUrl('/home/create-orga');
  }

  createEquip(): void {
    this.router.navigateByUrl('/home/create-equip');
  }

  createService(): void {
    this.router.navigateByUrl('/home/create-service');
  }

  homeSearch(): void {
    this.router.navigateByUrl('/home');
  }
}
