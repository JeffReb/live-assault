import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppStates } from '../../../../store/app.states';
import { LogOut } from '../../../../store/actions/auth.actions';
import { AssetsServices } from '../../services/assets.services';
import { environment } from '../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Socket } from 'ngx-socket-io';
import { SocketIoService } from '../../../../services/sockets/socket-io.service';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserMenuComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public userID: string;
  constructor(
    public router: Router,
    private assetsService: AssetsServices,
    private store: Store<AppStates>,
    private socket: Socket,
    private socketIoService: SocketIoService
  ) {}

  logOut(): void {
    this.socket.disconnect();
    this.socketIoService.setConnectedStatus('false').subscribe(data => {
      if (!data) {
      } else {
      }
    });
    this.store.dispatch(new LogOut());
  }

  ngOnInit() {
    const tokenUser = localStorage.getItem('token');
    this.userID = helper.decodeToken(tokenUser)['userId'];
    this.getImgProf(this.userID);
  }

  openMenu(): void {
    const tokenUser = localStorage.getItem('token');
    this.userID = helper.decodeToken(tokenUser)['userId'];
    this.getImgProf(this.userID);
  }

  getImgProf(userId): void {
    this.assetsService.getImgProfile().subscribe(data => {
      if (!data) {
      } else {
        const Filename = data.filename;
        if (Filename === 'ImgProfile not found') {
          this.userImage = '../assets/img/users/user.png';
        } else {
          this.userImage = APICloud + '/' + userId + '/' + Filename;
        }
      }
    });
  }
}
