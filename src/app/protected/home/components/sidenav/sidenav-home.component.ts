import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Settings } from '../../../../app.settings.model';
import { AppSettings } from '../../../../app.settings';
import { Store } from '@ngrx/store';
import { AppStates } from '../../../../store/app.states';
import { LogOut } from '../../../../store/actions/auth.actions';
import { MenuHomeService } from '../menu/menu-home.service';
import { TransfertActivityService } from '../../../../services/activity/transfert.activity.service';
import { TransfertProfileService } from '../../../../services/activity/transfert.profile.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AssetsServices } from '../../services/assets.services';
import { environment } from '../../../../../environments/environment';
import { MatDialog } from '@angular/material';
import { ImgProfileFilePickerAdapter } from './img-profile-file-picker.adapter';
import { HttpClient } from '@angular/common/http';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { DatesAndTimes } from '../../../../services/utils/dates-and-times';
import { Socket } from 'ngx-socket-io';
import { SocketIoService } from '../../../../services/sockets/socket-io.service';

const helper = new JwtHelperService();

const APICloud = environment.APICloud;

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-imgprofile-sidenav-home',
  templateUrl: './dialog-imgprofile-sidenav-home.component.html',
  styleUrls: ['./dialog-imgprofile-sidenave-home.component.scss']
})
export class DialogImgProfileSidenavHomeComponent implements OnInit {
  // constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new ImgProfileFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }
  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-sidenav-home',
  templateUrl: './sidenav-home.component.html',
  styleUrls: ['./sidenav-home.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [MenuHomeService]
})
export class SidenavHomeComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public menuItems: Array<any>;
  public settings: Settings;
  public lastname: string;
  public firstname: string;
  public pseudo: string;
  public memberyear: string;
  public membermonth: string;
  public MemberSince: string;
  public userID: string;

  constructor(
    public appSettings: AppSettings,
    public menuService: MenuHomeService,
    private store: Store<AppStates>,
    private transfertActivityService: TransfertActivityService,
    private transfertProfileService: TransfertProfileService,
    private assetsService: AssetsServices,
    public dialog: MatDialog,
    private dateAndTimes: DatesAndTimes,
    private socket: Socket,
    private socketIoService: SocketIoService
  ) {
    this.settings = this.appSettings.settings;
  }

  addImg(): void {
    const dialogRef = this.dialog.open(DialogImgProfileSidenavHomeComponent, {
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      const tokenUserClosed = localStorage.getItem('token');
      this.userID = helper.decodeToken(tokenUserClosed)['userId'];
      this.getImgProf(this.userID);
    });
  }

  ngOnInit() {
    const activityJson = localStorage.getItem('activity');
    const tokenJson = localStorage.getItem('token');

    if (!activityJson || !tokenJson) {
      const activityObj = this.transfertActivityService.getData();
      const tokenUser = this.transfertProfileService.getData();
      this.userID = helper.decodeToken(tokenUser)['userId'];
      this.lastname = helper.decodeToken(tokenUser)['lastname'];
      this.firstname = helper.decodeToken(tokenUser)['firstname'];
      this.pseudo = helper.decodeToken(tokenUser)['userName'];
      // const memberSinceNumber = helper.decodeToken(tokenUser)['addedDate'];
      this.MemberSince = helper.decodeToken(tokenUser)['addedDate'];
      // this.MemberSince = this.dateAndTimes.getMemberSince(memberSinceNumber);
      this.menuItems = this.menuService.getVerticalMenuItems(activityObj);
    } else {
      const activityObj = JSON.parse(activityJson);
      const tokenUser = localStorage.getItem('token');
      this.userID = helper.decodeToken(tokenUser)['userId'];
      this.lastname = helper.decodeToken(tokenUser)['lastname'];
      this.firstname = helper.decodeToken(tokenUser)['firstname'];
      this.pseudo = helper.decodeToken(tokenUser)['userName'];
      // const memberSinceNumber = helper.decodeToken(tokenUser)['addedDate'];
      this.MemberSince = helper.decodeToken(tokenUser)['addedDate'];
      // this.MemberSince = this.dateAndTimes.getMemberSince(memberSinceNumber);
      this.menuItems = this.menuService.getVerticalMenuItems(activityObj);
    }

    this.getImgProf(this.userID);
  }

  getImgProf(userId): void {
    this.assetsService.getImgProfile().subscribe(data => {
      if (!data) {
      } else {
        const Filename = data.filename;
        if (Filename === 'ImgProfile not found') {
          this.userImage = '../assets/img/users/user.png';
        } else {
          this.userImage = APICloud + '/' + userId + '/' + Filename;
        }
      }
    });
  }

  logOut(): void {
    this.socket.disconnect();
    this.socketIoService.setConnectedStatus('false').subscribe(data => {
      if (!data) {
      } else {
      }
    });
    this.store.dispatch(new LogOut());
  }

  public closeSubMenus() {
    const menu = document.getElementById('vertical-menu');
    if (menu) {
      for (let i = 0; i < menu.children[0].children.length; i++) {
        const child = menu.children[0].children[i];
        if (child) {
          if (child.children[0].classList.contains('expanded')) {
            child.children[0].classList.remove('expanded');
            child.children[1].classList.remove('show');
          }
        }
      }
    }
  }
}
