import { Component, OnInit, ViewChild } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import {
  MatDialog,
  MatListOption,
  MatOption,
  MatPaginator,
  MatSelectChange,
  MatSliderChange,
  MatSlideToggleChange,
  MatTable
} from '@angular/material';
import * as _ from 'lodash';
import { SearchServices } from '../services/search.services';
import { MatTableDataSource } from '@angular/material';
import { DatesAndTimes } from '../../../services/utils/dates-and-times';
import { Map, tileLayer, marker } from 'leaflet';
import PlaceResult = google.maps.places.PlaceResult;
import { TransfertActivityService } from '../../../services/activity/transfert.activity.service';
import { Router } from '@angular/router';

declare var google;

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

export interface Element {
  name: string;
  member: string;
  city: string;
  country: string;
  activity: string;
  style: string;
  details: string;
  icon: string;
}

const dataTable: Element[] = [
  { name: '', member: '', city: '', country: '', activity: '', style: '', details: '', icon: '' }
];

@Component({
  selector: 'app-dialog-geo-dashboard.component',
  templateUrl: './dialog-geo-dashboard.component.html',
  styleUrls: ['./dialog-geo-dashboard.component.scss']
})
export class DialogGeoDashboardComponent implements OnInit {
  map: Map;
  newMarker: any;
  public latitude: number;
  public longitude: number;
  constructor() {}

  onNoClick(): void {}

  ngOnInit() {
    if (!localStorage.getItem('searchLocationLat')) {
      this.latitude = 48.86;
      this.longitude = 2.33;
    } else {
      const latitude = localStorage.getItem('searchLocationLat');
      const longitude = localStorage.getItem('searchLocationLon');
      this.latitude = +latitude;
      this.longitude = +longitude;
    }

    this.map = new Map('mapId').setView([this.latitude, this.longitude], 13);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
    }).addTo(this.map);

    this.newMarker = marker([this.latitude, this.longitude], {
      draggable: false
    }).addTo(this.map);

    this.map.on('click', <LeafletMouseEvent>(e) => {
      // console.log(e.latlng.lat);
      this.latitude = e.latlng.lat;
      this.longitude = e.latlng.lng;
      this.newMarker.setLatLng(e.latlng);

      localStorage.setItem('searchLocationLat', this.latitude.toString());
      localStorage.setItem('searchLocationLon', this.longitude.toString());
    });
  }
}

@Component({
  selector: 'app-protected-home-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public settings: Settings;
  public searchText: string;
  public searchName: string;
  public searchBandName: string;
  public searchBandStyle: string;
  public searchBandLocation: string;
  public searchSiteName: string;
  public searchSiteStyle: string;
  public searchSiteLocation: string;
  public searchManageName: string;
  public searchManageStyle: string;
  public searchManageLocation: string;
  public searchOrgaName: string;
  public searchOrgaStyle: string;
  public searchOrgaLocation: string;
  public searchServiceName: string;
  public searchServiceStyle: string;
  public searchServiceType: string;
  public searchServiceLocation: string;
  public searchEquipName: string;
  public searchEquipLocation: string;

  public isDisabledType = true;
  private searchLocationFilter: string;
  private searchLocationDistance: string;
  public searchLocationLat: string;
  public searchLocationLon: string;
  private searchNameFilter: string;
  private searchNameStr: string;
  private searchActivityFilter: string;
  private searchActivities: string;
  private searchTypeFilter: string;
  private searchType: string;
  private searchStyleFilter: string;
  private searchStyle: string;

  public searchLocationLatStr: string;
  public searchLocationLonStr: string;

  private sourceName: string;
  private sourceMember: string;
  private sourceCity: string;
  private sourceDept: string;
  private sourceCountry: string;
  private sourceActivity: string;
  private sourceStyle: string;
  private sourceId: string;
  private sourceIcon: string;

  dataTableResult = dataTable;

  public displayedColumns = ['name', 'member', 'city', 'country', 'activity', 'style', 'details'];
  public dataSource: any;

  public arroundValue: any;

  public activities: {
    name: string;
    selected: boolean;
    valueact: string;
  }[];

  checked = false;
  indeterminate = false;

  // Slider Toggle
  color = 'accent';
  disabled = false;
  disabledSlider = true;
  autoTicksSlider = false;
  invertSlider = false;
  maxSlider = 1000;
  minSlider = 10;
  showTicksSlider = false;
  stepSlider = 1;
  thumbLabelSlider = true;
  public valueSlider = 500;
  verticalSlider = false;

  @ViewChild(MatTable, { static: false }) table: MatTable<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private searchService: SearchServices,
    private dateAndTimes: DatesAndTimes,
    public dialog: MatDialog,
    private transfertActivityService: TransfertActivityService,
    private router: Router
  ) {
    this.dataSource = new MatTableDataSource<Element>(dataTable);
    this.dataSource.paginator = this.paginator;
  }

  // LocationSearch
  pickLocationSearch(): void {
    const dialogRef = this.dialog.open(DialogGeoDashboardComponent, {
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('test');
      this.getSearchAct(
        localStorage.getItem('searchLocationFilter'),
        localStorage.getItem('searchLocationDistance'),
        localStorage.getItem('searchLocationLat'),
        localStorage.getItem('searchLocationLon'),
        localStorage.getItem('searchNameFilter'),
        localStorage.getItem('searchNameStr'),
        localStorage.getItem('searchActivityFilter'),
        localStorage.getItem('searchActivities'),
        localStorage.getItem('searchTypeFilter'),
        localStorage.getItem('searchType'),
        localStorage.getItem('searchStyleFilter'),
        localStorage.getItem('searchStyle')
      );
    });
  }

  onAutocompleteSelectedCitySearch(resultBand: PlaceResult) {
    // ('onAddressSelected: ', resultBand.address_components);
    // console.log('onAddressSelected: ', resultBand.place_id);
    const lat = resultBand.geometry.location.lat().toString();
    const long = resultBand.geometry.location.lng().toString();
    // console.log('lat autocomplete', lat);
    // console.log('long autocomplete', long);
    localStorage.setItem('searchLocationLat', lat);
    localStorage.setItem('searchLocationLon', long);

    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      lat,
      long,
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );

    // this.newlatlng = this.getGeoCode(lat, long, 'Band');
    // console.warn('resultBandonAutocomplete: ', this.newlatlng);
    // console.warn('resultBandonAutocomplete: ', this.newlatlng[2]);
  }

  ngOnInit() {
    this.activities = [
      {
        name: 'Bands',
        selected: false,
        valueact: 'Band'
      },
      {
        name: 'Site',
        selected: false,
        valueact: 'Site'
      },
      {
        name: 'Managers',
        selected: false,
        valueact: 'Manage'
      },
      {
        name: 'Associations',
        selected: false,
        valueact: 'Orga'
      },
      {
        name: 'Equipment',
        selected: false,
        valueact: 'Equip'
      },
      {
        name: 'Services',
        selected: false,
        valueact: 'Service'
      }
    ];
    this.searchLocationFilter = 'false';
    this.searchLocationDistance = '500km';
    this.searchNameFilter = 'false';
    this.searchNameStr = '';
    this.searchActivityFilter = 'false';
    this.searchActivities = '';
    this.searchTypeFilter = 'false';
    this.searchType = '';
    this.searchStyleFilter = 'false';
    this.searchStyle = '';
    this.searchLocationLatStr = localStorage.getItem('searchLocationLat');
    this.searchLocationLonStr = localStorage.getItem('searchLocationLon');

    if (localStorage.getItem('searchActivityFilter')) {
      if (localStorage.getItem('searchActivityFilter') === 'true') {
        this.searchActivityFilter = localStorage.getItem('searchActivityFilter');
        this.searchActivities = localStorage.getItem('searchActivities');
        this.activities = JSON.parse(localStorage.getItem('activitiesSelected'));
      }
    }

    if (!localStorage.getItem('searchLocationLat')) {
      navigator.geolocation.getCurrentPosition(location => {
        // console.log('location detected: ', location.coords.latitude);
        // console.log(location.coords.longitude);
        // console.log(location.coords.accuracy);
        this.searchLocationLat = location.coords.latitude.toString();
        this.searchLocationLon = location.coords.longitude.toString();
        localStorage.setItem('searchLocationLat', this.searchLocationLat);
        localStorage.setItem('searchLocationLon', this.searchLocationLon);
      });
    }
    if (localStorage.getItem('searchLocationDistance')) {
      // console.log('with storage:', this.valueSlider);
      // this.searchLocationDistance = localStorage.getItem('searchLocationDistance');
      const slider = localStorage.getItem('searchLocationDistance');
      // console.log(slider.substring(0, slider.length - 2));
      const sliderValue = slider.substring(0, slider.length - 2);
      this.valueSlider = +sliderValue;
    } else {
      // console.log('without storage: ', this.valueSlider);
      this.valueSlider = 500;
    }

    localStorage.setItem('searchLocationFilter', this.searchLocationFilter);
    localStorage.setItem('searchLocationDistance', this.searchLocationDistance);

    localStorage.setItem('searchNameFilter', this.searchNameFilter);
    localStorage.setItem('searchNameStr', this.searchNameStr);
    localStorage.setItem('searchActivityFilter', this.searchActivityFilter);
    localStorage.setItem('searchActivities', this.searchActivities);
    localStorage.setItem('searchTypeFilter', this.searchTypeFilter);
    localStorage.setItem('searchType', this.searchType);
    localStorage.setItem('searchStyleFilter', this.searchStyleFilter);
    localStorage.setItem('searchStyle', this.searchStyle);
    localStorage.setItem('activitiesSelected', JSON.stringify(this.activities));

    this.getSearchAct(
      this.searchLocationFilter,
      this.searchLocationDistance,
      this.searchLocationLat,
      this.searchLocationLon,
      this.searchNameFilter,
      this.searchNameStr,
      this.searchActivityFilter,
      this.searchActivities,
      this.searchTypeFilter,
      this.searchType,
      this.searchStyleFilter,
      this.searchStyle
    );
  }

  onDetails(event, detailsId, activity) {
    // console.log('detailsId: ', detailsId);
    // console.log('activity: ', activity);
    this.transfertActivityService.setData(detailsId);
    switch (activity) {
      case 'band':
        const urlBand = '/home/read-band?id=' + detailsId;
        this.router.navigateByUrl(urlBand);
        break;
      case 'site':
        const urlSite = '/home/read-site?id=' + detailsId;
        this.router.navigateByUrl(urlSite);
        break;
      case 'manage':
        const urlManage = '/home/read-manage?id=' + detailsId;
        this.router.navigateByUrl(urlManage);
        break;
      case 'orga':
        const urlOrga = '/home/read-orga?id=' + detailsId;
        this.router.navigateByUrl(urlOrga);
        break;
      case 'service':
        const urlService = '/home/read-service?id=' + detailsId;
        this.router.navigateByUrl(urlService);
        break;
      case 'equip':
        const urlEquip = '/home/read-equip?id=' + detailsId;
        this.router.navigateByUrl(urlEquip);
        break;
      default:
        this.router.navigateByUrl('/home');
    }
  }

  onGroupsActivityChange(options: MatListOption[]) {
    const activitiesResult = options.map(o => o.value);
    const bandSelected = _.includes(activitiesResult, 'Band');
    const siteSelected = _.includes(activitiesResult, 'Site');
    const manageSelected = _.includes(activitiesResult, 'Manage');
    const orgaSelected = _.includes(activitiesResult, 'Orga');
    const equipSelected = _.includes(activitiesResult, 'Equip');
    const serviceSelected = _.includes(activitiesResult, 'Service');

    // console.log(activitiesResult);
    // console.log(serviceSelected);

    this.searchActivities = JSON.stringify(activitiesResult);
    if (bandSelected) {
      const bandObj = {
        name: 'Bands',
        selected: true,
        valueact: 'Band'
      };
      this.activities.splice(0, 1, bandObj);
    } else {
      const bandObj = {
        name: 'Bands',
        selected: false,
        valueact: 'Band'
      };
      this.activities.splice(0, 1, bandObj);
    }
    if (siteSelected) {
      const siteObj = {
        name: 'Site',
        selected: true,
        valueact: 'Site'
      };
      this.activities.splice(1, 1, siteObj);
    } else {
      const siteObj = {
        name: 'Site',
        selected: false,
        valueact: 'Site'
      };
      this.activities.splice(1, 1, siteObj);
    }
    if (manageSelected) {
      const manageObj = {
        name: 'Managers',
        selected: true,
        valueact: 'Manage'
      };
      this.activities.splice(2, 1, manageObj);
    } else {
      const manageObj = {
        name: 'Managers',
        selected: false,
        valueact: 'Manage'
      };
      this.activities.splice(2, 1, manageObj);
    }
    if (orgaSelected) {
      const orgaObj = {
        name: 'Associations',
        selected: true,
        valueact: 'Orga'
      };
      this.activities.splice(3, 1, orgaObj);
    } else {
      const orgaObj = {
        name: 'Associations',
        selected: false,
        valueact: 'Orga'
      };
      this.activities.splice(3, 1, orgaObj);
    }
    if (equipSelected) {
      const equipObj = {
        name: 'Equipment',
        selected: true,
        valueact: 'Equip'
      };
      this.activities.splice(4, 1, equipObj);
    } else {
      const equipObj = {
        name: 'Equipment',
        selected: false,
        valueact: 'Equip'
      };
      this.activities.splice(4, 1, equipObj);
    }
    if (serviceSelected) {
      this.isDisabledType = false;
      const servObj = {
        name: 'Services',
        selected: true,
        valueact: 'Service'
      };
      this.activities.splice(5, 1, servObj);
    } else {
      this.isDisabledType = true;
      const servObj = {
        name: 'Services',
        selected: false,
        valueact: 'Service'
      };
      this.activities.splice(5, 1, servObj);
    }
    if (activitiesResult.length === 0) {
      this.searchActivityFilter = 'false';
    } else {
      this.searchActivityFilter = 'true';
    }
    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      this.searchActivityFilter,
      this.searchActivities,
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
    localStorage.setItem('searchActivityFilter', this.searchActivityFilter);
    localStorage.setItem('searchActivities', this.searchActivities);
    localStorage.setItem('activitiesSelected', JSON.stringify(this.activities));
  }

  onInputChange(event) {
    const locationDistance = event.target.value.toString();
    this.searchLocationFilter = 'true';
    this.searchLocationDistance = locationDistance + 'km';
    // console.log('distance before search funct: ', this.searchLocationDistance);

    this.getSearchAct(
      this.searchLocationFilter,
      this.searchLocationDistance,
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
  }

  onSliderChange(event: MatSliderChange) {
    const locationDistance = event.value.toString();
    this.searchLocationFilter = 'true';
    this.searchLocationDistance = locationDistance + 'km';
    localStorage.setItem('searchLocationDistance', this.searchLocationDistance);

    this.getSearchAct(
      this.searchLocationFilter,
      this.searchLocationDistance,
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
  }

  onChangeLocation(value: MatSlideToggleChange) {
    const { checked } = value;
    // console.log(checked);
    if (checked === true) {
      if (!localStorage.getItem('searchLocationLat')) {
        this.searchLocationLat = '48.86';
        this.searchLocationLon = '2.33';
        localStorage.setItem('searchLocationLat', this.searchLocationLat);
        localStorage.setItem('searchLocationLon', this.searchLocationLon);
      }
      const locationDistance = this.valueSlider.toString();
      this.searchLocationFilter = 'true';
      this.searchLocationDistance = locationDistance + 'km';

      this.disabledSlider = false;

      localStorage.setItem('searchLocationFilter', this.searchLocationFilter);
      localStorage.setItem('searchLocationDistance', this.searchLocationDistance);
    } else {
      const locationDistance = this.valueSlider.toString();
      this.searchLocationFilter = 'false';
      this.searchLocationDistance = locationDistance + 'km';
      this.disabledSlider = true;

      localStorage.setItem('searchLocationFilter', this.searchLocationFilter);
      localStorage.setItem('searchLocationDistance', this.searchLocationDistance);
    }

    this.getSearchAct(
      this.searchLocationFilter,
      this.searchLocationDistance,
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
  }

  onSelectedType(event: MatSelectChange) {
    const selectedData = {
      text: (event.source.selected as MatOption).viewValue,
      value: event.source.value
    };
    const arraySelectedData = Object.keys(selectedData).map(key => selectedData[key]);
    // console.log(arraySelectedData[1]);
    this.searchType = JSON.stringify(arraySelectedData[1]);
    if (arraySelectedData[1].length === 0) {
      this.searchTypeFilter = 'false';
    } else {
      this.searchTypeFilter = 'true';
    }
    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      this.searchTypeFilter,
      this.searchType,
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
    localStorage.setItem('searchTypeFilter', this.searchTypeFilter);
    localStorage.setItem('searchType', this.searchType);
  }

  onSearchName(event: KeyboardEvent) {
    this.searchNameStr = (event.target as HTMLInputElement).value;
    this.searchNameFilter = 'true';
    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      this.searchNameFilter,
      this.searchNameStr,
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
    localStorage.setItem('searchNameFilter', this.searchNameFilter);
    localStorage.setItem('searchNameStr', this.searchNameStr);
  }

  onSearchNameOff(): void {
    this.searchNameFilter = 'false';
    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      this.searchNameFilter,
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      localStorage.getItem('searchStyleFilter'),
      localStorage.getItem('searchStyle')
    );
    localStorage.setItem('searchNameFilter', this.searchNameFilter);
    localStorage.setItem('searchNameStr', '');
  }

  onSearchStyle(event: KeyboardEvent) {
    this.searchStyle = (event.target as HTMLInputElement).value;
    if (this.searchStyle.length === 0) {
      // console.log('false');
      this.searchStyleFilter = 'false';
    } else {
      // console.log('true');
      this.searchStyleFilter = 'true';
    }
    this.getSearchAct(
      localStorage.getItem('searchLocationFilter'),
      localStorage.getItem('searchLocationDistance'),
      localStorage.getItem('searchLocationLat'),
      localStorage.getItem('searchLocationLon'),
      localStorage.getItem('searchNameFilter'),
      localStorage.getItem('searchNameStr'),
      localStorage.getItem('searchActivityFilter'),
      localStorage.getItem('searchActivities'),
      localStorage.getItem('searchTypeFilter'),
      localStorage.getItem('searchType'),
      this.searchStyleFilter,
      this.searchStyle
    );
    localStorage.setItem('searchStyleFilter', this.searchStyleFilter);
    localStorage.setItem('searchStyle', this.searchStyle);
  }

  getSearchAct(
    locationfilter,
    locationdistance,
    locationlat,
    locationlon,
    namefilter,
    name,
    activityfilter,
    activities,
    typefilter,
    type,
    stylefilter,
    style
  ): void {
    // console.log('locationlat in search: ', locationlat);
    // console.log('locationlon in search: ', locationlon);
    // console.log('distance in search funct: ', locationdistance);
    this.searchService
      .getSearchActivities(
        locationfilter,
        locationdistance,
        locationlat,
        locationlon,
        namefilter,
        name,
        activityfilter,
        activities,
        typefilter,
        type,
        stylefilter,
        style
      )
      .subscribe(data => {
        if (!data) {
        } else {
          // console.log('result: ', data);
          // console.log('typeof: ', typeof data);
          const arrayData = Object.keys(data).map(key => data[key]);
          const dataLength = Object.keys(data).length;
          // console.log('dataLength: ', dataLength);
          if (dataLength === 0) {
            this.dataTableResult = [];
          } else {
            // console.log(arrayData[0]);
            const arrayLength = arrayData[0];
            const arrayDataLength = Object.keys(arrayLength).length;
            // console.log(arrayDataLength);
            // console.log(arrayLength[0]._source.name);
            this.dataTableResult = [];

            for (let i = 0; i < arrayDataLength; i++) {
              // console.log(arrayLength[i]._source.name);
              if (arrayLength[i]._source.name) {
                this.sourceName = arrayLength[i]._source.name;
              } else {
                this.sourceName = '';
              }
              if (arrayLength[i]._source.addedDate) {
                const addedDateStr = arrayLength[i]._source.addedDate;
                this.sourceMember = this.dateAndTimes.getMemberSince(addedDateStr);
              } else {
                this.sourceMember = '';
              }
              if (arrayLength[i]._source.city) {
                this.sourceCity = arrayLength[i]._source.city;
              } else {
                this.sourceCity = '';
              }
              if (arrayLength[i]._source.dept) {
                this.sourceDept = arrayLength[i]._source.dept;
              } else {
                this.sourceDept = '';
              }
              if (arrayLength[i]._source.country) {
                this.sourceCountry = arrayLength[i]._source.country;
              } else {
                this.sourceCountry = '';
              }
              if (arrayLength[i]._source.activity) {
                this.sourceActivity = arrayLength[i]._source.activity;
              } else {
                this.sourceActivity = '';
              }
              if (arrayLength[i]._source.style) {
                this.sourceStyle = arrayLength[i]._source.style;
              } else {
                this.sourceStyle = '';
              }
              if (arrayLength[i]._id) {
                this.sourceId = arrayLength[i]._id;
              } else {
                this.sourceId = '';
              }

              switch (this.sourceActivity) {
                case 'band':
                  this.sourceIcon = 'music_video';
                  break;
                case 'site':
                  this.sourceIcon = 'room';
                  break;
                case 'manage':
                  this.sourceIcon = 'description';
                  break;
                case 'orga':
                  this.sourceIcon = 'group_work';
                  break;
                case 'service':
                  this.sourceIcon = 'extension';
                  break;
                case 'equip':
                  this.sourceIcon = 'shop';
                  break;
                default:
                  this.sourceIcon = 'group_work';
                  break;
              }

              this.dataTableResult.push({
                name: this.sourceName,
                member: this.sourceMember,
                city: this.sourceCity,
                country: this.sourceCountry,
                activity: this.sourceActivity,
                style: this.sourceStyle,
                details: this.sourceId,
                icon: this.sourceIcon
              });
            }
          }

          // const dataTableResult: Element[] = [
          //   {name: 'Hydrogen', member: 'Janv 2019', city: 'H', dept: '42', country: 'FR', activity: 'Band', style: 'Trash'},
          //   {name: 'Hydrogen', member: 'Janv 2019', city: 'H', dept: '42', country: 'FR', activity: 'Band', style: 'Trash'},
          //   {name: 'Hydrogen', member: 'Janv 2019', city: 'H', dept: '42', country: 'FR', activity: 'Band', style: 'Trash'},
          //   {name: 'Hydrogen', member: 'Janv 2019', city: 'H', dept: '42', country: 'FR', activity: 'Band', style: 'Trash'},
          // ];
          this.dataSource = new MatTableDataSource<Element>(this.dataTableResult);
          this.dataSource.paginator = this.paginator;
        }
      });
  }
}
