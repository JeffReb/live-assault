import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterSite } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { faFileContract, faHandHolding, faMapMarkedAlt, faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';
import PlaceResult = google.maps.places.PlaceResult;

declare var google;

export interface DialogDataCreateSite {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-site-create.component',
  templateUrl: 'dialog-site-create.component.html'
})
export class DialogCreateSiteComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateSite, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-site-create',
  templateUrl: './site-create.component.html',
  styleUrls: ['./site-create.component.scss']
})
export class SiteCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formhall: FormGroup;
  public user: Observable<object> | null;
  public formValuesHall: object | null;

  messagehall: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();

  languagenav: Array<string> = [this.languagecodenavigator];
  message: string | null;

  faFileContract = faFileContract;
  faMapMarkedAlt = faMapMarkedAlt;
  faHandHolding = faHandHolding;
  faMicrophone = faMicrophone;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.formhall = this.fb.group({
        namehall: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adresshall: [null, Validators.compose([Validators.required])],
        phonehall: [null, Validators.compose([Validators.required])],
        emailhall: [null, Validators.compose([Validators.required, emailValidator])],
        typehall: [null],
        capacityhall: [null],
        stylehall: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitHall(values: object): void {
    if (this.formhall.valid) {
      this.messagehall = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeHall');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messagehall);
      this.formValuesHall = {
        namehall: this.formhall.value.namehall,
        adresshall: localStorage.getItem('AdressHall'),
        phonehall: this.formhall.value.phonehall,
        emailhall: this.formhall.value.emailhall,
        typehall: this.formhall.value.typehall,
        capacityhall: this.formhall.value.capacityhall,
        stylehall: this.formhall.value.stylehall,
        cityid: localStorage.getItem('callbackCityIdHall'),
        city: localStorage.getItem('callbackCityHall'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdHall'),
        cityalt2: localStorage.getItem('callbackCityAlt2Hall'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdHall'),
        cityalt1: localStorage.getItem('callbackCityAlt1Hall'),
        countryid: localStorage.getItem('callbackCountryIdHall'),
        country: localStorage.getItem('callbackCountryHall'),
        countrycode: localStorage.getItem('callbackCountryCodeHall'),
        continent,
        latHall: localStorage.getItem('latHall'),
        lngHall: localStorage.getItem('lngHall'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesHall);

      const cityId = this.formValuesHall;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[7]);
      if (valuecity[7] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesHall;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messagehall = data.errorMessage;
          // console.log(this.messagehall);
          if (!this.messagehall) {
            // console.log('!this.messageband: ', this.messagehall);
          } else {
            // console.log('this.messageband: ', this.messagehall);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messagehall
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterSite(payload));
      }

      localStorage.removeItem('callbackCityIdHall');
      localStorage.removeItem('callbackCityHall');
      localStorage.removeItem('callbackCityAlt2IdHall');
      localStorage.removeItem('callbackCityAlt2Hall');
      localStorage.removeItem('callbackCityAlt1IdHall');
      localStorage.removeItem('callbackCityAlt1Hall');
      localStorage.removeItem('callbackCountryIdHall');
      localStorage.removeItem('callbackCountryHall');
      localStorage.removeItem('callbackCountryCodeHall');
      localStorage.removeItem('latHall');
      localStorage.removeItem('lngHall');
      localStorage.removeItem('AdressHall');
    }
  }

  onAutocompleteSelectedHall(resultHall: PlaceResult) {
    // console.log('onAddressSelected: ', resultHall.address_components);
    // console.log('onAddressSelected: ', resultHall.id);
    const lat = resultHall.geometry.location.lat();
    const long = resultHall.geometry.location.lng();
    localStorage.setItem('latHall', lat.toString());
    localStorage.setItem('lngHall', long.toString());
    localStorage.setItem('AdressHall', resultHall.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Hall']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Hall');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdHall');
    localStorage.removeItem('callbackCityHall');
    localStorage.removeItem('callbackCityAlt2IdHall');
    localStorage.removeItem('callbackCityAlt2Hall');
    localStorage.removeItem('callbackCityAlt1IdHall');
    localStorage.removeItem('callbackCityAlt1Hall');
    localStorage.removeItem('callbackCountryIdHall');
    localStorage.removeItem('callbackCountryHall');
    localStorage.removeItem('callbackCountryCodeHall');
    localStorage.removeItem('latHall');
    localStorage.removeItem('lngHall');
    localStorage.removeItem('AdressHall');
  }
}
