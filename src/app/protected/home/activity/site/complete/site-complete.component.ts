import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SiteService } from '../../../../../services/activity/sites/site.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { HttpClient } from '@angular/common/http';
import { AssetsServices } from '../../../services/assets.services';
import { MatDialog } from '@angular/material';
import { LogoSiteFilePickerAdapter } from './logo-site-file-picker.adapter';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logosite-site-complete',
  templateUrl: './dialog-logosite-site-complete.component.html',
  styleUrls: ['./dialog-logosite-site-complete.component.scss']
})
export class DialogLogoSiteSiteCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoSiteFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-site-complete',
  templateUrl: './site-complete.component.html',
  styleUrls: ['./site-complete.component.scss']
})
export class SiteCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameSite: string;
  public addressSite: string;
  public addedDateSite: string;
  public mailSite: string;
  public phoneSite: string;
  public capacitySite: string;
  public styleSite: string;
  public typeSite: string;

  constructor(
    private siteService: SiteService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoSite(): void {
    const dialogRef = this.dialog.open(DialogLogoSiteSiteCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoSite(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getSiteById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoSite(this.idDetails);
  }

  getImgLogoSite(idSite): void {
    this.assetsService.getLogoSite(idSite).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logosite not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getSiteById(siteId): void {
    // console.log('siteId: ', siteId);
    this.siteService.readSite(siteId).subscribe(data => {
      if (!data) {
      } else {
        this.nameSite = data.siteReadname;
        this.addressSite = data.siteReadaddress;
        this.addedDateSite = data.siteReadaddedDate;
        this.mailSite = data.siteReademail;
        this.phoneSite = data.siteReadphone;
        this.capacitySite = data.siteReadcapacity;
        this.styleSite = data.siteReadstyle;
        this.typeSite = data.siteReadtype;
      }
    });
  }
}
