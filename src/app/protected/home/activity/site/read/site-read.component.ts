import { Component, OnInit } from '@angular/core';
import { SiteService } from '../../../../../services/activity/sites/site.service';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AssetsServices } from '../../../services/assets.services';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

@Component({
  selector: 'app-protected-home-site-read',
  templateUrl: './site-read.component.html',
  styleUrls: ['./site-read.component.scss']
})
export class SiteReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idSite: string;
  public nameSite: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public addressSite: string;
  public addedDateSite: string;
  public mailSite: string;
  public phoneSite: string;
  public capacitySite: string;
  public styleSite: string;
  public typeSite: string;
  public contactState: string;
  public myID: string;

  constructor(
    private siteService: SiteService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const siteID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    // console.log('siteIDTest: ', siteID);
    if (!siteID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
        // console.log('idDetails: ', this.idDetails);
      });
      this.idSite = this.idDetails;
      this.getSiteById(this.idSite);
    } else {
      this.idSite = siteID;
      this.getSiteById(this.idSite);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getSiteById(siteId): void {
    // console.log('siteId: ', siteId);
    this.siteService.readSite(siteId).subscribe(data => {
      if (!data) {
      } else {
        this.nameSite = data.siteReadname;
        this.addressSite = data.siteReadaddress;
        this.addedDateSite = data.siteReadaddedDate;
        this.mailSite = data.siteReademail;
        this.phoneSite = data.siteReadphone;
        this.capacitySite = data.siteReadcapacity;
        this.styleSite = data.siteReadstyle;
        this.typeSite = data.siteReadtype;
        this.getReadUserById(data.siteReaduserid);
        this.userID = data.siteReaduserid;
        this.getImgLogoSite(this.idSite, this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.result[0].user.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoSite(idSite, idUser): void {
    this.assetsService.getLogoSite(idSite).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logosite not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
