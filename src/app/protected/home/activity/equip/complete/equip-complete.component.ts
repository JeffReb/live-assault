import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipService } from '../../../../../services/activity/equip/equip.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { LogoEquipFilePickerAdapter } from './logo-equip-file-picker.adapter';
import { HttpClient } from '@angular/common/http';
import { AssetsServices } from '../../../services/assets.services';
import { MatDialog } from '@angular/material';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logoequip-equip-complete',
  templateUrl: './dialog-logoequip-equip-complete.component.html',
  styleUrls: ['./dialog-logoequip-equip-complete.component.scss']
})
export class DialogLogoEquipEquipCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoEquipFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-equip-complete',
  templateUrl: './equip-complete.component.html',
  styleUrls: ['./equip-complete.component.scss']
})
export class EquipCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameEquip: string;
  public addedDateEquip: string;
  public mailEquip: string;
  public addressEquip: string;
  public phoneEquip: string;
  public typeEquip: string;

  constructor(
    private equipService: EquipService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoEquip(): void {
    const dialogRef = this.dialog.open(DialogLogoEquipEquipCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoEquip(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getEquipById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoEquip(this.idDetails);
  }

  getImgLogoEquip(idEquip): void {
    this.assetsService.getLogoEquip(idEquip).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoequip not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getEquipById(equipId): void {
    this.equipService.readEquip(equipId).subscribe(data => {
      if (!data) {
      } else {
        this.nameEquip = data.equipReadname;
        this.addressEquip = data.equipReadaddress;
        this.addedDateEquip = data.equipReadaddedDate;
        this.mailEquip = data.equipReademail;
        this.phoneEquip = data.equipReadphone;
        this.typeEquip = data.equipReadtype;
      }
    });
  }
}
