import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterEquip } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import PlaceResult = google.maps.places.PlaceResult;
import { Observable } from 'rxjs';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';

declare var google;

export interface DialogDataCreateEquip {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-equip-create.component',
  templateUrl: 'dialog-equip-create.component.html'
})
export class DialogCreateEquipComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateEquip, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-equip-create',
  templateUrl: './equip-create.component.html',
  styleUrls: ['./equip-create.component.scss']
})
export class EquipCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formequipement: FormGroup;
  public user: Observable<object> | null;
  public formValuesEquip: object | null;

  messageequip: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();
  languagenav: Array<string> = [this.languagecodenavigator];
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.formequipement = this.fb.group({
        nameequip: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressequip: [null, Validators.compose([Validators.required])],
        phoneequip: [null, Validators.compose([Validators.required])],
        emailequip: [null, Validators.compose([Validators.required, emailValidator])],
        typeequip: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitEquipement(values: object): void {
    if (this.formequipement.valid) {
      this.messageequip = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeEquip');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageequip);
      this.formValuesEquip = {
        nameequip: this.formequipement.value.nameequip,
        adressequip: localStorage.getItem('AdressEquip'),
        phoneequip: this.formequipement.value.phoneequip,
        emailequip: this.formequipement.value.emailequip,
        typeequip: this.formequipement.value.typeequip,
        cityid: localStorage.getItem('callbackCityIdEquip'),
        city: localStorage.getItem('callbackCityEquip'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdEquip'),
        cityalt2: localStorage.getItem('callbackCityAlt2Equip'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdEquip'),
        cityalt1: localStorage.getItem('callbackCityAlt1Equip'),
        countryid: localStorage.getItem('callbackCountryIdEquip'),
        country: localStorage.getItem('callbackCountryEquip'),
        countrycode: localStorage.getItem('callbackCountryCodeEquip'),
        continent,
        latEquip: localStorage.getItem('latEquip'),
        lngEquip: localStorage.getItem('lngEquip'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesEquip);

      const cityId = this.formValuesEquip;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);

      if (valuecity[6] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesEquip;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageequip = data.errorMessage;
          // console.log(this.messageequip);
          if (!this.messageequip) {
            // console.log('!this.messageband: ', this.messageequip);
          } else {
            // console.log('this.messageband: ', this.messageequip);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageequip
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterEquip(payload));
      }

      localStorage.removeItem('callbackCityIdEquip');
      localStorage.removeItem('callbackCityEquip');
      localStorage.removeItem('callbackCityAlt2IdEquip');
      localStorage.removeItem('callbackCityAlt2Equip');
      localStorage.removeItem('callbackCityAlt1IdEquip');
      localStorage.removeItem('callbackCityAlt1Equip');
      localStorage.removeItem('callbackCountryIdEquip');
      localStorage.removeItem('callbackCountryEquip');
      localStorage.removeItem('callbackCountryCodeEquip');
      localStorage.removeItem('latEquip');
      localStorage.removeItem('lngEquip');
      localStorage.removeItem('AdressEquip');
    }
  }

  onAutocompleteSelectedEquip(resultEquip: PlaceResult) {
    // console.log('onAddressSelected: ', resultEquip.address_components);
    // console.log('onAddressSelected: ', resultEquip.id);
    const lat = resultEquip.geometry.location.lat();
    const long = resultEquip.geometry.location.lng();
    localStorage.setItem('latEquip', lat.toString());
    localStorage.setItem('lngEquip', long.toString());
    localStorage.setItem('AdressEquip', resultEquip.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Equip']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Equip');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdEquip');
    localStorage.removeItem('callbackCityEquip');
    localStorage.removeItem('callbackCityAlt2IdEquip');
    localStorage.removeItem('callbackCityAlt2Equip');
    localStorage.removeItem('callbackCityAlt1IdEquip');
    localStorage.removeItem('callbackCityAlt1Equip');
    localStorage.removeItem('callbackCountryIdEquip');
    localStorage.removeItem('callbackCountryEquip');
    localStorage.removeItem('callbackCountryCodeEquip');
    localStorage.removeItem('latEquip');
    localStorage.removeItem('lngEquip');
    localStorage.removeItem('AdressEquip');
  }
}
