import { Component, OnInit } from '@angular/core';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EquipService } from '../../../../../services/activity/equip/equip.service';
import { AssetsServices } from '../../../services/assets.services';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();

const APICloud = environment.APICloud;

@Component({
  selector: 'app-protected-home-equip-read',
  templateUrl: './equip-read.component.html',
  styleUrls: ['./equip-read.component.scss']
})
export class EquipReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idEquip: string;
  public nameEquip: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public addressEquip: string;
  public addedDateEquip: string;
  public mailEquip: string;
  public phoneEquip: string;
  public typeEquip: string;
  public contactState: string;
  public myID: string;

  constructor(
    private equipService: EquipService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const equipID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    if (!equipID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
      });
      this.idEquip = this.idDetails;
      this.getEquipById(this.idEquip);
    } else {
      this.idEquip = equipID;
      this.getEquipById(this.idEquip);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getEquipById(equipId): void {
    this.equipService.readEquip(equipId).subscribe(data => {
      if (!data) {
      } else {
        this.nameEquip = data.equipReadname;
        this.addressEquip = data.equipReadaddress;
        this.addedDateEquip = data.equipReadaddedDate;
        this.mailEquip = data.equipReademail;
        this.phoneEquip = data.equipReadphone;
        this.typeEquip = data.equipReadtype;
        this.getReadUserById(data.equipReaduserid);
        this.userID = data.equipReaduserid;
        this.getImgLogoEquip(this.idEquip, this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoEquip(idEquip, idUser): void {
    this.assetsService.getLogoEquip(idEquip).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoequip not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
