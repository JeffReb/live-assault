import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../../../../services/activity/services/services.service';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AssetsServices } from '../../../services/assets.services';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

@Component({
  selector: 'app-protected-home-service-read',
  templateUrl: './service-read.component.html',
  styleUrls: ['./service-read.component.scss']
})
export class ServiceReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idService: string;
  public nameService: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public addressService: string;
  public addedDateService: string;
  public mailService: string;
  public phoneService: string;
  public statusService: string;
  public styleService: string;
  public servicesService: string;
  public contactState: string;
  public myID: string;

  constructor(
    private serviceService: ServicesService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const serviceID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    if (!serviceID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
      });
      this.idService = this.idDetails;
      this.getServiceById(this.idService);
    } else {
      this.idService = serviceID;
      this.getServiceById(this.idService);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getServiceById(serviceId): void {
    this.serviceService.readService(serviceId).subscribe(data => {
      if (!data) {
      } else {
        this.nameService = data.serviceReadname;
        this.addressService = data.serviceReadaddress;
        this.addedDateService = data.serviceReadaddedDate;
        this.mailService = data.serviceReademail;
        this.phoneService = data.serviceReadphone;
        this.statusService = data.serviceReadstatus;
        this.styleService = data.serviceReadstyle;
        this.servicesService = data.serviceReadservice;
        this.getReadUserById(data.serviceReaduserid);
        this.userID = data.serviceReaduserid;
        this.getImgLogoService(this.idService, this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.result[0].user.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoService(idService, idUser): void {
    this.assetsService.getLogoService(idService).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoservice not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
