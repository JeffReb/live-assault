import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../../../../../services/activity/services/services.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { HttpClient } from '@angular/common/http';
import { AssetsServices } from '../../../services/assets.services';
import { MatDialog } from '@angular/material';
import { LogoServiceFilePickerAdapter } from './logo-service-file-picker.adapter';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logoservice-service-complete',
  templateUrl: './dialog-logoservice-service-complete.component.html',
  styleUrls: ['./dialog-logoservice-service-complete.component.scss']
})
export class DialogLogoServiceServiceCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoServiceFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-service-complete',
  templateUrl: './service-complete.component.html',
  styleUrls: ['./service-complete.component.scss']
})
export class ServiceCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameService: string;
  public addressService: string;
  public addedDateService: string;
  public mailService: string;
  public phoneService: string;
  public statusService: string;
  public styleService: string;
  public servicesService: string;

  constructor(
    private serviceService: ServicesService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoService(): void {
    const dialogRef = this.dialog.open(DialogLogoServiceServiceCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoService(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getServiceById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoService(this.idDetails);
  }

  getImgLogoService(idService): void {
    this.assetsService.getLogoService(idService).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoservice not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getServiceById(serviceId): void {
    this.serviceService.readService(serviceId).subscribe(data => {
      if (!data) {
      } else {
        this.nameService = data.serviceReadname;
        this.addressService = data.serviceReadaddress;
        this.addedDateService = data.serviceReadaddedDate;
        this.mailService = data.serviceReademail;
        this.phoneService = data.serviceReadphone;
        this.statusService = data.serviceReadstatus;
        this.styleService = data.serviceReadstyle;
        this.servicesService = data.serviceReadservice;
      }
    });
  }
}
