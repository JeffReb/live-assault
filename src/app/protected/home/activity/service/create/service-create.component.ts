import { AfterViewInit, Component, Inject } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterServices } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { faFileContract, faHandHolding, faMapMarkedAlt, faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';
import PlaceResult = google.maps.places.PlaceResult;

declare var google;

export interface DialogDataCreateService {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-service-create.component',
  templateUrl: 'dialog-service-create.component.html'
})
export class DialogCreateServiceComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateService, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-service-create',
  templateUrl: './service-create.component.html',
  styleUrls: ['./service-create.component.scss']
})
export class ServiceCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formservice: FormGroup;
  public user: Observable<object> | null;
  public formValuesServices: object | null;

  messageservice: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();

  languagenav: Array<string> = [this.languagecodenavigator];
  message: string | null;

  faFileContract = faFileContract;
  faMapMarkedAlt = faMapMarkedAlt;
  faHandHolding = faHandHolding;
  faMicrophone = faMicrophone;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.formservice = this.fb.group({
        nameservice: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressservice: [null, Validators.compose([Validators.required])],
        phoneservice: [null, Validators.compose([Validators.required])],
        emailservice: [null, Validators.compose([Validators.required, emailValidator])],
        typeservice: [null],
        statusservice: [null],
        styleservice: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitService(values: object): void {
    if (this.formservice.valid) {
      this.messageservice = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeService');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageservice);
      this.formValuesServices = {
        nameservices: this.formservice.value.nameservice,
        adressservices: localStorage.getItem('AdressService'),
        phoneservices: this.formservice.value.phoneservice,
        emailservices: this.formservice.value.emailservice,
        servicesservices: this.formservice.value.typeservice,
        statusservices: this.formservice.value.statusservice,
        styleservices: this.formservice.value.styleservice,
        cityid: localStorage.getItem('callbackCityIdService'),
        city: localStorage.getItem('callbackCityService'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdService'),
        cityalt2: localStorage.getItem('callbackCityAlt2Service'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdService'),
        cityalt1: localStorage.getItem('callbackCityAlt1Service'),
        countryid: localStorage.getItem('callbackCountryIdService'),
        country: localStorage.getItem('callbackCountryService'),
        countrycode: localStorage.getItem('callbackCountryCodeService'),
        continent,
        latServices: localStorage.getItem('latService'),
        lngServices: localStorage.getItem('lngService'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesServices);

      const cityId = this.formValuesServices;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[7]);

      if (valuecity[7] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesServices;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageservice = data.errorMessage;
          // console.log(this.messageservice);
          if (!this.messageservice) {
            // console.log('!this.messageband: ', this.messageservice);
          } else {
            // console.log('this.messageband: ', this.messageservice);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageservice
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterServices(payload));
      }

      localStorage.removeItem('callbackCityIdService');
      localStorage.removeItem('callbackCityService');
      localStorage.removeItem('callbackCityAlt2IdService');
      localStorage.removeItem('callbackCityAlt2Service');
      localStorage.removeItem('callbackCityAlt1IdService');
      localStorage.removeItem('callbackCityAlt1Service');
      localStorage.removeItem('callbackCountryIdService');
      localStorage.removeItem('callbackCountryService');
      localStorage.removeItem('callbackCountryCodeService');
      localStorage.removeItem('latService');
      localStorage.removeItem('lngService');
      localStorage.removeItem('AdressService');
    }
  }

  onAutocompleteSelectedService(resultService: PlaceResult) {
    // console.log('onAddressSelected: ', resultService.address_components);
    // console.log('onAddressSelected: ', resultService.id);
    const lat = resultService.geometry.location.lat();
    const long = resultService.geometry.location.lng();
    localStorage.setItem('latService', lat.toString());
    localStorage.setItem('lngService', long.toString());
    localStorage.setItem('AdressService', resultService.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Service']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Service');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdService');
    localStorage.removeItem('callbackCityService');
    localStorage.removeItem('callbackCityAlt2IdService');
    localStorage.removeItem('callbackCityAlt2Service');
    localStorage.removeItem('callbackCityAlt1IdService');
    localStorage.removeItem('callbackCityAlt1Service');
    localStorage.removeItem('callbackCountryIdService');
    localStorage.removeItem('callbackCountryService');
    localStorage.removeItem('callbackCountryCodeService');
    localStorage.removeItem('latService');
    localStorage.removeItem('lngService');
    localStorage.removeItem('AdressService');
  }
}
