import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterManage } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';
import PlaceResult = google.maps.places.PlaceResult;
import { faFileContract, faHandHolding, faMapMarkedAlt, faMicrophone } from '@fortawesome/free-solid-svg-icons';

declare var google;

export interface DialogDataCreateManage {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-manage-create.component',
  templateUrl: 'dialog-manage-create.component.html'
})
export class DialogCreateManageComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateManage, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-manage-create',
  templateUrl: './manage-create.component.html',
  styleUrls: ['./manage-create.component.scss']
})
export class ManageCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formmanager: FormGroup;
  public user: Observable<object> | null;
  public formValuesManager: object | null;

  messagemanager: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();

  languagenav: Array<string> = [this.languagecodenavigator];
  message: string | null;

  faFileContract = faFileContract;
  faMapMarkedAlt = faMapMarkedAlt;
  faHandHolding = faHandHolding;
  faMicrophone = faMicrophone;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.formmanager = this.fb.group({
        namemanager: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressmanager: [null, Validators.compose([Validators.required])],
        phonemanager: [null, Validators.compose([Validators.required])],
        emailmanager: [null, Validators.compose([Validators.required, emailValidator])],
        typemanager: [null],
        statusmanager: [null],
        stylemanager: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitManager(values: object): void {
    if (this.formmanager.valid) {
      this.messagemanager = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeManager');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messagemanager);
      this.formValuesManager = {
        namemanage: this.formmanager.value.namemanager,
        adressmanage: localStorage.getItem('AdressManager'),
        phonemanage: this.formmanager.value.phonemanager,
        emailmanage: this.formmanager.value.emailmanager,
        typemanage: this.formmanager.value.typemanager,
        statusmanage: this.formmanager.value.statusmanager,
        stylemanage: this.formmanager.value.stylemanager,
        cityid: localStorage.getItem('callbackCityIdManager'),
        city: localStorage.getItem('callbackCityManager'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdManager'),
        cityalt2: localStorage.getItem('callbackCityAlt2Manager'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdManager'),
        cityalt1: localStorage.getItem('callbackCityAlt1Manager'),
        countryid: localStorage.getItem('callbackCountryIdManager'),
        country: localStorage.getItem('callbackCountryManager'),
        countrycode: localStorage.getItem('callbackCountryCodeManager'),
        continent,
        latManage: localStorage.getItem('latManager'),
        lngManage: localStorage.getItem('lngManager'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesManager);

      const cityId = this.formValuesManager;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[7]);

      if (valuecity[7] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesManager;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messagemanager = data.errorMessage;
          // console.log(this.messagemanager);
          if (!this.messagemanager) {
            // console.log('!this.messageband: ', this.messagemanager);
          } else {
            // console.log('this.messageband: ', this.messagemanager);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messagemanager
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterManage(payload));
      }

      localStorage.removeItem('callbackCityIdManager');
      localStorage.removeItem('callbackCityManager');
      localStorage.removeItem('callbackCityAlt2IdManager');
      localStorage.removeItem('callbackCityAlt2Manager');
      localStorage.removeItem('callbackCityAlt1IdManager');
      localStorage.removeItem('callbackCityAlt1Manager');
      localStorage.removeItem('callbackCountryIdManager');
      localStorage.removeItem('callbackCountryManager');
      localStorage.removeItem('callbackCountryCodeManager');
      localStorage.removeItem('latManager');
      localStorage.removeItem('lngManager');
      localStorage.removeItem('AdressManager');
    }
  }

  onAutocompleteSelectedManager(resultManager: PlaceResult) {
    // console.log('onAddressSelected: ', resultManager.address_components);
    // console.log('onAddressSelected: ', resultManager.id);
    const lat = resultManager.geometry.location.lat();
    const long = resultManager.geometry.location.lng();
    localStorage.setItem('latManager', lat.toString());
    localStorage.setItem('lngManager', long.toString());
    localStorage.setItem('AdressManager', resultManager.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Manager']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Manager');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdManager');
    localStorage.removeItem('callbackCityManager');
    localStorage.removeItem('callbackCityAlt2IdManager');
    localStorage.removeItem('callbackCityAlt2Manager');
    localStorage.removeItem('callbackCityAlt1IdManager');
    localStorage.removeItem('callbackCityAlt1Manager');
    localStorage.removeItem('callbackCountryIdManager');
    localStorage.removeItem('callbackCountryManager');
    localStorage.removeItem('callbackCountryCodeManager');
    localStorage.removeItem('latManager');
    localStorage.removeItem('lngManager');
    localStorage.removeItem('AdressManager');
  }
}
