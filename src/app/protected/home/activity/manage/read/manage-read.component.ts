import { Component, OnInit } from '@angular/core';
import { ManageService } from '../../../../../services/activity/manage/manage.service';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { AssetsServices } from '../../../services/assets.services';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';
import { JwtHelperService } from '@auth0/angular-jwt';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

@Component({
  selector: 'app-protected-home-manage-read',
  templateUrl: './manage-read.component.html',
  styleUrls: ['./manage-read.component.scss']
})
export class ManageReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idManage: string;
  public nameManage: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public addressManage: string;
  public addedDateManage: string;
  public mailManage: string;
  public phoneManage: string;
  public statusManage: string;
  public styleManage: string;
  public typeManage: string;
  public contactState: string;
  public myID: string;

  constructor(
    private manageService: ManageService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const manageID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    if (!manageID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
      });
      this.idManage = this.idDetails;
      this.getManageById(this.idManage);
    } else {
      this.idManage = manageID;
      this.getManageById(this.idManage);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getManageById(manageId): void {
    this.manageService.readManage(manageId).subscribe(data => {
      if (!data) {
      } else {
        this.nameManage = data.manageReadname;
        this.addressManage = data.manageReadaddress;
        this.addedDateManage = data.manageReadaddedDate;
        this.mailManage = data.manageReademail;
        this.phoneManage = data.manageReadphone;
        this.statusManage = data.manageReadstatus;
        this.styleManage = data.manageReadstyle;
        this.typeManage = data.manageReadtype;
        this.getReadUserById(data.manageReaduserid);
        this.userID = data.manageReaduserid;
        this.getImgLogoManage(this.idManage, this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.result[0].user.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoManage(idManage, idUser): void {
    this.assetsService.getLogoManage(idManage).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logomanage not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
