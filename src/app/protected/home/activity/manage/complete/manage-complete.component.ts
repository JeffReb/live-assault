import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ManageService } from '../../../../../services/activity/manage/manage.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { HttpClient } from '@angular/common/http';
import { AssetsServices } from '../../../services/assets.services';
import { MatDialog } from '@angular/material';
import { LogoManageFilePickerAdapter } from './logo-manage-file-picker.adapter';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logomanage-manage-complete',
  templateUrl: './dialog-logomanage-manage-complete.component.html',
  styleUrls: ['./dialog-logomanage-manage-complete.component.scss']
})
export class DialogLogoManageManageCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoManageFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-manage-complete',
  templateUrl: './manage-complete.component.html',
  styleUrls: ['./manage-complete.component.scss']
})
export class ManageCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameManage: string;
  public addressManage: string;
  public addedDateManage: string;
  public mailManage: string;
  public phoneManage: string;
  public statusManage: string;
  public styleManage: string;
  public typeManage: string;

  constructor(
    private manageService: ManageService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoManage(): void {
    const dialogRef = this.dialog.open(DialogLogoManageManageCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoManage(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getManageById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoManage(this.idDetails);
  }

  getImgLogoManage(idManage): void {
    this.assetsService.getLogoManage(idManage).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logomanage not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getManageById(manageId): void {
    this.manageService.readManage(manageId).subscribe(data => {
      if (!data) {
      } else {
        this.nameManage = data.manageReadname;
        this.addressManage = data.manageReadaddress;
        this.addedDateManage = data.manageReadaddedDate;
        this.mailManage = data.manageReademail;
        this.phoneManage = data.manageReadphone;
        this.statusManage = data.manageReadstatus;
        this.styleManage = data.manageReadstyle;
        this.typeManage = data.manageReadtype;
      }
    });
  }
}
