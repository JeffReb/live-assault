import { AfterViewInit, Component, Inject } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterBand } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Band } from '../../../../../models/band';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { BandService } from '../../../../../services/activity/bands/band.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';
import PlaceResult = google.maps.places.PlaceResult;
import { StreetMapGeoService } from '../../../../../services/forms/geolocation/street-map-geo.service';

declare var google;

export interface DialogDataCreateBand {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-band-create.component',
  templateUrl: 'dialog-band-create.component.html'
})
export class DialogCreateBandComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateBand, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-band-create',
  templateUrl: './band-create.component.html',
  styleUrls: ['./band-create.component.scss']
})
export class BandCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formband: FormGroup;
  public user: Observable<object> | null;
  public formValuesBand: object | null;

  messageband: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();
  languagenav: Array<string> = [this.languagecodenavigator];
  band: Band = new Band();
  message: string | null;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private bandService: BandService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>,
    private streetMapGeoService: StreetMapGeoService
  ) {
    (this.settings = this.appSettings.settings),
      (this.formband = this.fb.group({
        nameband: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        style1band: [null, Validators.compose([Validators.required])],
        style2band: [null],
        style3band: [null],
        cityband: [null, Validators.compose([Validators.required])],
        emailband: [null, Validators.compose([emailValidator])],
        roleband: [null, Validators.compose([Validators.required])]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitBand(values: object): void {
    if (this.formband.valid) {
      this.messageband = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeBand');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageband);
      this.formValuesBand = {
        nameband: this.formband.value.nameband,
        style1: this.formband.value.style1band,
        style2: this.formband.value.style2band,
        style3: this.formband.value.style3band,
        emailband: this.formband.value.emailband,
        roleband: this.formband.value.roleband,
        cityid: localStorage.getItem('callbackCityIdBand'),
        city: localStorage.getItem('callbackCityBand'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdBand'),
        cityalt2: localStorage.getItem('callbackCityAlt2Band'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdBand'),
        cityalt1: localStorage.getItem('callbackCityAlt1Band'),
        countryid: localStorage.getItem('callbackCountryIdBand'),
        country: localStorage.getItem('callbackCountryBand'),
        countrycode: localStorage.getItem('callbackCountryCodeBand'),
        continent,
        latBand: localStorage.getItem('latBand'),
        lngBand: localStorage.getItem('lngBand'),
        token: localStorage.getItem('token')
      };
      // console.warn(this.formValuesBand);

      // console.warn(this.formValuesBand.value.cityid);
      const cityId = this.formValuesBand;
      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[6]);
      if (valuecity[6] === null) {
        // console.log('cityBand: ', this.formband.value.cityband);
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your city address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesBand;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageband = data.errorMessage;
          // console.log(this.messageband);
          if (!this.messageband) {
            // console.log('!this.messageband: ', this.messageband);
          } else {
            // console.log('this.messageband: ', this.messageband);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageband
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterBand(payload));
      }

      localStorage.removeItem('callbackCityIdBand');
      localStorage.removeItem('callbackCityBand');
      localStorage.removeItem('callbackCityAlt2IdBand');
      localStorage.removeItem('callbackCityAlt2Band');
      localStorage.removeItem('callbackCityAlt1IdBand');
      localStorage.removeItem('callbackCityAlt1Band');
      localStorage.removeItem('callbackCountryIdBand');
      localStorage.removeItem('callbackCountryBand');
      localStorage.removeItem('callbackCountryCodeBand');
      localStorage.removeItem('latBand');
      localStorage.removeItem('lngBand');
      localStorage.removeItem('AdressBand');
    }
  }

  onChangeSelectedCityBand(event: KeyboardEvent) {
    const valueCity = (event.target as HTMLInputElement).value;
    // console.log(valueCity);
    const testGMapFail = localStorage.getItem('callbackCityIdBand');
    if (!testGMapFail) {
      this.streetMapGeoService.getLatLongOpenStreetMap(valueCity).subscribe(results => {
        // console.log(results[0].latitude);
        const lat = results[0].latitude;
        const long = results[0].longitude;
        localStorage.setItem('latBand', lat.toString());
        localStorage.setItem('lngBand', long.toString());
        this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Band']);
      });
    }
  }

  onAutocompleteSelectedBand(resultBand: PlaceResult) {
    // ('onAddressSelected: ', resultBand.address_components);
    // console.log('onAddressSelected: ', resultBand.place_id);
    const lat = resultBand.geometry.location.lat();
    const long = resultBand.geometry.location.lng();
    // console.log('lat autocomplete', lat);
    // console.log('long autocomplete', long);
    localStorage.setItem('latBand', lat.toString());
    localStorage.setItem('lngBand', long.toString());
    localStorage.setItem('AdressBand', resultBand.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Band']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Band');
    // console.warn('resultBandonAutocomplete: ', this.newlatlng);
    // console.warn('resultBandonAutocomplete: ', this.newlatlng[2]);
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdBand');
    localStorage.removeItem('callbackCityBand');
    localStorage.removeItem('callbackCityAlt2IdBand');
    localStorage.removeItem('callbackCityAlt2Band');
    localStorage.removeItem('callbackCityAlt1IdBand');
    localStorage.removeItem('callbackCityAlt1Band');
    localStorage.removeItem('callbackCountryIdBand');
    localStorage.removeItem('callbackCountryBand');
    localStorage.removeItem('callbackCountryCodeBand');
    localStorage.removeItem('latBand');
    localStorage.removeItem('lngBand');
    localStorage.removeItem('AdressBand');
  }
}
