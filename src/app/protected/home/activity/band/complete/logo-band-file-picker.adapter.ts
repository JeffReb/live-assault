import { environment } from '../../../../../../environments/environment';
import { FilePickerAdapter, FilePreviewModel } from 'ngx-awesome-uploader';
import { HttpClient, HttpEvent, HttpEventType, HttpRequest } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

const APIEndpoint = environment.APIEndpoint;

export class LogoBandFilePickerAdapter extends FilePickerAdapter {
  constructor(private http: HttpClient) {
    super();
  }

  public uploadFile(fileItem: FilePreviewModel) {
    const form = new FormData();
    // console.log(form);
    form.append('logoband', fileItem.file, fileItem.fileName);

    // console.log(typeof fileItem.file);
    // console.log(fileItem.file);
    const api = APIEndpoint + '/assets/band/uploadlogo';
    const req = new HttpRequest('POST', api, form, { reportProgress: true });

    return this.http.request(req).pipe(
      map((res: HttpEvent<any>) => {
        if (res.type === HttpEventType.Response) {
          return res.body.id.toString();
        } else if (res.type === HttpEventType.UploadProgress) {
          const Uploadprogress = 100 * (res.loaded / res.total);
          return Uploadprogress;
        }
      })
    );
  }

  public removeFile(fileItem): Observable<any> {
    const removeApi = APIEndpoint + '/assets/band/uploadlogo';
    return this.http.post(removeApi, {});
  }
}
