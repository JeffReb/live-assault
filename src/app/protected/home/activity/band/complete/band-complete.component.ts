import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BandService } from '../../../../../services/activity/bands/band.service';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { LogoBandFilePickerAdapter } from './logo-band-file-picker.adapter';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material';
import { AssetsServices } from '../../../services/assets.services';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logoband-band-complete',
  templateUrl: './dialog-logoband-band-complete.component.html',
  styleUrls: ['./dialog-logoband-band-complete.component.scss']
})
export class DialogLogoBandBandCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoBandFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-band-complete',
  templateUrl: './band-complete.component.html',
  styleUrls: ['./band-complete.component.scss']
})
export class BandCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameBand: string;
  public cityBand: string;
  public cityCountryBand: string;
  public addedDateBand: string;
  public mailBand: string;
  public roleBand: string;
  public styleBand: string;

  constructor(
    private bandService: BandService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoBand(): void {
    const dialogRef = this.dialog.open(DialogLogoBandBandCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoBand(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getBandById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoBand(this.idDetails);
  }

  getImgLogoBand(idBand): void {
    this.assetsService.getLogoBand(idBand).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoband not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getBandById(bandId): void {
    // console.log('bandId: ', bandId);
    this.bandService.readBand(bandId).subscribe(data => {
      if (!data) {
      } else {
        this.nameBand = data.bandReadname;
        this.cityBand = data.bandReadcity;
        this.cityCountryBand = data.bandReadcountry;
        this.addedDateBand = data.bandReadaddedDate;
        this.mailBand = data.bandReademail;
        this.roleBand = data.bandReadrole;
        this.styleBand = data.bandReadstyle;
      }
    });
  }
}
