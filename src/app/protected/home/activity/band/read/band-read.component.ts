import { Component, OnInit } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { BandService } from '../../../../../services/activity/bands/band.service';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetsServices } from '../../../services/assets.services';
import { environment } from '../../../../../../environments/environment';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();

const APICloud = environment.APICloud;

@Component({
  selector: 'app-protected-home-band-read',
  templateUrl: './band-read.component.html',
  styleUrls: ['./band-read.component.scss']
})
export class BandReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public settings: Settings;
  public idBand: string;
  public nameBand: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public cityBand: string;
  public cityCountryBand: string;
  public addedDateBand: string;
  public mailBand: string;
  public roleBand: string;
  public styleBand: string;
  public contactState: string;
  public myID: string;

  constructor(
    private bandService: BandService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const bandID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    // console.log('bandIDTest: ', bandID);
    if (!bandID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
        // console.log('idDetails: ', this.idDetails);
      });
      this.idBand = this.idDetails;
      this.getBandById(this.idBand);
    } else {
      this.idBand = bandID;
      this.getBandById(this.idBand);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getBandById(bandId): void {
    // console.log('bandId: ', bandId);
    this.bandService.readBand(bandId).subscribe(data => {
      if (!data) {
      } else {
        this.nameBand = data.bandReadname;
        this.cityBand = data.bandReadcity;
        this.cityCountryBand = data.bandReadcountry;
        this.addedDateBand = data.bandReadaddedDate;
        this.mailBand = data.bandReademail;
        this.roleBand = data.bandReadrole;
        this.styleBand = data.bandReadstyle;

        // console.log('data name: ', data.bandReadname);
        // console.log('data band-userid: ', data.bandReaduserid);
        this.getReadUserById(data.bandReaduserid);
        this.userID = data.bandReaduserid;

        this.getImgLogoBand(this.idBand, this.userID);
        // console.log('this.userID: ', this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        // console.log('userReadAddedDate', this.userReadAddedDate);
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoBand(idBand, idUser): void {
    this.assetsService.getLogoBand(idBand).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoband not found') {
          this.logoImage = '../assets/img/activity/logo.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
