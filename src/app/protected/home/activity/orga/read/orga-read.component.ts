import { Component, OnInit } from '@angular/core';
import { OrgaService } from '../../../../../services/activity/orga/orga.service';
import { TransfertActivityService } from '../../../../../services/activity/transfert.activity.service';
import { UserService } from '../../../../../services/users/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AssetsServices } from '../../../services/assets.services';
import { DatesAndTimes } from '../../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../../services/users/contacts.service';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

@Component({
  selector: 'app-protected-home-orga-read',
  templateUrl: './orga-read.component.html',
  styleUrls: ['./orga-read.component.scss']
})
export class OrgaReadComponent implements OnInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idOrga: string;
  public nameOrga: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userID: string;
  public idDetails: string;
  public MemberSince: string;
  public addressOrga: string;
  public addedDateOrga: string;
  public mailOrga: string;
  public phoneOrga: string;
  public statusOrga: string;
  public styleOrga: string;
  public typeOrga: string;
  public contactState: string;
  public myID: string;

  constructor(
    private orgaService: OrgaService,
    private transfertActivityService: TransfertActivityService,
    private userService: UserService,
    private route: ActivatedRoute,
    private assetsService: AssetsServices,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private router: Router
  ) {}

  ngOnInit() {
    const orgaID = this.transfertActivityService.getData();
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    if (!orgaID) {
      this.route.queryParams.subscribe(params => {
        this.idDetails = params.id;
      });
      this.idOrga = this.idDetails;
      this.getOrgaById(this.idOrga);
    } else {
      this.idOrga = orgaID;
      this.getOrgaById(this.idOrga);
    }
  }

  onUserDetails(event, detailsId) {
    this.transfertActivityService.setData(detailsId);
    const urlUser = '/home/read-user?id=' + detailsId;
    this.router.navigateByUrl(urlUser);
  }

  getOrgaById(orgaId): void {
    this.orgaService.readOrga(orgaId).subscribe(data => {
      if (!data) {
      } else {
        this.nameOrga = data.orgaReadname;
        this.addressOrga = data.orgaReadaddress;
        this.addedDateOrga = data.orgaReadaddedDate;
        this.mailOrga = data.orgaReademail;
        this.phoneOrga = data.orgaReadphone;
        this.statusOrga = data.orgaReadstatus;
        this.styleOrga = data.orgaReadstyle;
        this.typeOrga = data.orgaReadtype;
        this.getReadUserById(data.orgaReaduserid);
        this.userID = data.orgaReaduserid;
        this.getImgLogoOrga(this.idOrga, this.userID);
        this.contactService.contactState(this.userID).subscribe(dataContact => {
          if (!dataContact) {
          } else {
            if (this.myID === this.userID) {
              this.contactState = 'forbidden';
            } else {
              this.contactState = dataContact.contactstate;
            }
            // console.log('state: ', this.contactState);
          }
        });
        this.assetsService.getImgUser(this.userID).subscribe(dataimg => {
          if (!dataimg) {
          } else {
            const Filename = dataimg.filename;
            if (Filename === 'ImgProfile not found') {
              this.userImage = '../assets/img/users/user.png';
            } else {
              this.userImage = APICloud + '/' + this.userID + '/' + Filename;
            }
          }
        });
      }
    });
  }

  getReadUserById(userId): void {
    // console.log('userId: ', userId);
    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.result[0].user.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });
  }

  sendInviteUser(): void {
    this.contactService.inviteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.userID).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  getImgLogoOrga(idOrga, idUser): void {
    this.assetsService.getLogoOrga(idOrga).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoorga not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + idUser + '/' + Filename;
        }
      }
    });
  }
}
