import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrgaService } from '../../../../../services/activity/orga/orga.service';
import { environment } from '../../../../../../environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { FilePickerComponent, FilePreviewModel, ValidationError } from 'ngx-awesome-uploader';
import { HttpClient } from '@angular/common/http';
import { AssetsServices } from '../../../services/assets.services';
import { MatDialog } from '@angular/material';
import { LogoOrgaFilePickerAdapter } from './logo-orga-file-picker.adapter';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

export interface DialogData {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-logoorga-orga-complete',
  templateUrl: './dialog-logoorga-orga-complete.component.html',
  styleUrls: ['./dialog-logoorga-orga-complete.component.scss']
})
export class DialogLogoOrgaOrgaCompleteComponent implements OnInit {
  @ViewChild('uploader', { static: false }) uploader: FilePickerComponent;
  adapter = new LogoOrgaFilePickerAdapter(this.http);

  public isDisabledOkBtn = true;
  public IsWaitProgressBar = false;

  constructor(private http: HttpClient) {}

  onNoClick(): void {}
  ngOnInit() {}

  onValidationError(error: ValidationError) {
    alert(`Validation Error ${error.error} in ${error.file.name}`);
  }

  onFileAdded(file: FilePreviewModel) {
    this.isDisabledOkBtn = true;
    this.IsWaitProgressBar = true;
  }

  onUploadSuccess(e: FilePreviewModel) {
    this.isDisabledOkBtn = false;
    this.IsWaitProgressBar = false;
  }
}

@Component({
  selector: 'app-protected-home-orga-complete',
  templateUrl: './orga-complete.component.html',
  styleUrls: ['./orga-complete.component.scss']
})
export class OrgaCompleteComponent implements OnInit {
  public idDetails: string;
  public myID: string;
  public logoImage = '../assets/img/activity/logo-insert.png';
  public nameOrga: string;
  public addressOrga: string;
  public addedDateOrga: string;
  public mailOrga: string;
  public phoneOrga: string;
  public statusOrga: string;
  public styleOrga: string;
  public typeOrga: string;

  constructor(
    private orgaService: OrgaService,
    private assetsService: AssetsServices,
    private router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  addImgLogoOrga(): void {
    const dialogRef = this.dialog.open(DialogLogoOrgaOrgaCompleteComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('close');
      this.getImgLogoOrga(this.idDetails);
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
      // console.log('idDetails: ', this.idDetails);
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.getOrgaById(this.idDetails);
    localStorage.setItem('currentIdAct', this.idDetails.toString());
    this.getImgLogoOrga(this.idDetails);
  }

  getImgLogoOrga(idOrga): void {
    this.assetsService.getLogoOrga(idOrga).subscribe(dataImg => {
      if (!dataImg) {
      } else {
        const Filename = dataImg.filename;
        if (Filename === 'logoorga not found') {
          this.logoImage = '../assets/img/activity/logo-insert.png';
        } else {
          this.logoImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
  }

  getOrgaById(orgaId): void {
    this.orgaService.readOrga(orgaId).subscribe(data => {
      if (!data) {
      } else {
        this.nameOrga = data.orgaReadname;
        this.addressOrga = data.orgaReadaddress;
        this.addedDateOrga = data.orgaReadaddedDate;
        this.mailOrga = data.orgaReademail;
        this.phoneOrga = data.orgaReadphone;
        this.statusOrga = data.orgaReadstatus;
        this.styleOrga = data.orgaReadstyle;
        this.typeOrga = data.orgaReadtype;
      }
    });
  }
}
