import { AfterViewInit, Component, Inject, OnInit } from '@angular/core';
import { Settings } from '../../../../../app.settings.model';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { select, Store } from '@ngrx/store';
import { AppStates } from '../../../../../store/app.states';
import { CancelErrorMessage, RegisterOrga } from '../../../../../store/actions/auth.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { faFileContract, faHandHolding, faMapMarkedAlt, faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { AppSettings } from '../../../../../app.settings';
import { Router } from '@angular/router';
import { GeocodeInformService } from '../../../../../services/forms/geolocation/geocode-inform.service';
import { GetContinentService } from '../../../../../services/forms/geolocation/get-continent.service';
import { emailValidator } from '../../../../../theme/utils/app-validators';
import { DialogChooseplanComponent } from '../../../../validation/chooseplan/chooseplan.component';
import PlaceResult = google.maps.places.PlaceResult;

declare var google;

export interface DialogDataCreateOrga {
  messagedialog: null;
}

@Component({
  selector: 'app-dialog-orga-create.component',
  templateUrl: 'dialog-orga-create.component.html'
})
export class DialogCreateOrgaComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogDataCreateOrga, private store: Store<AppStates>) {}

  onNoClick(): void {
    // console.log('onclick !!');
    const payload = null;
    this.store.dispatch(new CancelErrorMessage(payload));
    // this.dialogRef.close();
  }
}

@Component({
  selector: 'app-protected-home-orga-create',
  templateUrl: './orga-create.component.html',
  styleUrls: ['./orga-create.component.scss']
})
export class OrgaCreateComponent implements AfterViewInit {
  public settings: Settings;
  public formorga: FormGroup;
  public user: Observable<object> | null;
  public formValuesOrga: object | null;

  messageorga: string | null;
  languagescodenavigator = window.navigator.language;
  languagecodenavigator = this.languagescodenavigator
    .substr(-2, 2)
    .toString()
    .toLocaleLowerCase();

  languagenav: Array<string> = [this.languagecodenavigator];
  message: string | null;

  faFileContract = faFileContract;
  faMapMarkedAlt = faMapMarkedAlt;
  faHandHolding = faHandHolding;
  faMicrophone = faMicrophone;

  constructor(
    public appSettings: AppSettings,
    public fb: FormBuilder,
    public router: Router,
    public dialog: MatDialog,
    private geocodeInformService: GeocodeInformService,
    private getContinentService: GetContinentService,
    private store: Store<AppStates>
  ) {
    (this.settings = this.appSettings.settings),
      (this.formorga = this.fb.group({
        nameorga: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        adressorga: [null, Validators.compose([Validators.required])],
        phoneorga: [null, Validators.compose([Validators.required])],
        emailorga: [null, Validators.compose([Validators.required, emailValidator])],
        typeorga: [null],
        statusorga: [null],
        styleorga: [null]
      })),
      this.geocodeInformService.getGeoCode(['39.82', '-98.57', 'activity']); // BE CAREFULL KEEP THIS VALUES
  }

  public onSubmitOrga(values: object): void {
    if (this.formorga.valid) {
      this.messageorga = 'testerror2';
      const coutryCodeforContinentSearch = localStorage.getItem('callbackCountryCodeOrga');
      // console.warn(coutryCodeforContinentSearch);
      const continent = this.getContinentService.getContinent(coutryCodeforContinentSearch);
      // console.log(continent);

      // console.warn(this.messageorga);
      this.formValuesOrga = {
        nameorga: this.formorga.value.nameorga,
        adressorga: localStorage.getItem('AdressOrga'),
        phoneorga: this.formorga.value.phoneorga,
        emailorga: this.formorga.value.emailorga,
        typeorga: this.formorga.value.typeorga,
        statusorga: this.formorga.value.statusorga,
        styleorga: this.formorga.value.styleorga,
        cityid: localStorage.getItem('callbackCityIdOrga'),
        city: localStorage.getItem('callbackCityOrga'),
        cityalt2id: localStorage.getItem('callbackCityAlt2IdOrga'),
        cityalt2: localStorage.getItem('callbackCityAlt2Orga'),
        cityalt1id: localStorage.getItem('callbackCityAlt1IdOrga'),
        cityalt1: localStorage.getItem('callbackCityAlt1Orga'),
        countryid: localStorage.getItem('callbackCountryIdOrga'),
        country: localStorage.getItem('callbackCountryOrga'),
        countrycode: localStorage.getItem('callbackCountryCodeOrga'),
        continent,
        latOrga: localStorage.getItem('latOrga'),
        lngOrga: localStorage.getItem('lngOrga'),
        token: localStorage.getItem('token')
      };

      // console.warn(this.formValuesOrga);

      const cityId = this.formValuesOrga;

      const valuecity = Object.keys(cityId).map(key => cityId[key]);
      // console.log(valuecity[7]);

      if (valuecity[7] === null) {
        const dialogRef = this.dialog.open(DialogChooseplanComponent, {
          disableClose: true,
          data: {
            messagedialog: 'Your address is not valid'
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          // console.log('result', result);
        });
      } else {
        const payload = this.formValuesOrga;
        this.store.pipe(select('appStates')).subscribe(data => {
          // console.log('Store Subscribe: ', data);
          this.messageorga = data.errorMessage;
          // console.log(this.messageorga);
          if (!this.messageorga) {
            console.log('!this.messageband: ', this.messageorga);
          } else {
            // console.log('this.messageband: ', this.messageorga);
            // this.formband.reset();
            const dialogRef = this.dialog.open(DialogChooseplanComponent, {
              disableClose: true,
              data: {
                messagedialog: this.messageorga
              }
            });
            dialogRef.afterClosed().subscribe(result => {
              // console.log('result', result);
            });
          }
        });
        this.store.dispatch(new RegisterOrga(payload));
      }

      localStorage.removeItem('callbackCityIdOrga');
      localStorage.removeItem('callbackCityOrga');
      localStorage.removeItem('callbackCityAlt2IdOrga');
      localStorage.removeItem('callbackCityAlt2Orga');
      localStorage.removeItem('callbackCityAlt1IdOrga');
      localStorage.removeItem('callbackCityAlt1Orga');
      localStorage.removeItem('callbackCountryIdOrga');
      localStorage.removeItem('callbackCountryOrga');
      localStorage.removeItem('callbackCountryCodeOrga');
      localStorage.removeItem('latOrga');
      localStorage.removeItem('lngOrga');
      localStorage.removeItem('AdressOrga');
    }
  }

  onAutocompleteSelectedOrga(resultOrga: PlaceResult) {
    // console.log('onAddressSelected: ', resultOrga.address_components);
    // console.log('onAddressSelected: ', resultOrga.id);
    const lat = resultOrga.geometry.location.lat();
    const long = resultOrga.geometry.location.lng();
    localStorage.setItem('latOrga', lat.toString());
    localStorage.setItem('lngOrga', long.toString());
    localStorage.setItem('AdressOrga', resultOrga.formatted_address);

    this.geocodeInformService.getGeoCode([lat.toString(), long.toString(), 'Orga']);

    // this.newlatlng = this.getGeoCode(lat, long, 'Orga');
  }

  ngAfterViewInit() {
    this.settings.loadingSpinner = false;

    localStorage.removeItem('callbackCityIdOrga');
    localStorage.removeItem('callbackCityOrga');
    localStorage.removeItem('callbackCityAlt2IdOrga');
    localStorage.removeItem('callbackCityAlt2Orga');
    localStorage.removeItem('callbackCityAlt1IdOrga');
    localStorage.removeItem('callbackCityAlt1Orga');
    localStorage.removeItem('callbackCountryIdOrga');
    localStorage.removeItem('callbackCountryOrga');
    localStorage.removeItem('callbackCountryCodeOrga');
    localStorage.removeItem('latOrga');
    localStorage.removeItem('lngOrga');
    localStorage.removeItem('AdressOrga');
  }
}
