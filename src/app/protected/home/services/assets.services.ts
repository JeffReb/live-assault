import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Imgprofile } from '../../../models/imgprofile';
import { Logoband } from '../../../models/logoband';
import { Logoequip } from '../../../models/logoequip';
import { Logomanage } from '../../../models/logomanage';
import { Logoorga } from '../../../models/logoorga';
import { Logoservice } from '../../../models/logoservice';
import { Logosite } from '../../../models/logosite';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class AssetsServices {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  getImgProfile(): Observable<Imgprofile> {
    const url = `${this.BASE_URL}/assets/imgprofile`;
    return this.https.get<Imgprofile>(url);
  }

  getImgUser(userId): Observable<Imgprofile> {
    const url = `${this.BASE_URL}/assets/imguser`;
    // console.log('request userId: ', userId);
    return this.https.post<Imgprofile>(url, { userId });
  }

  getLogoBand(bandId): Observable<Logoband> {
    const url = `${this.BASE_URL}/assets/logoband`;
    // console.log('request userId: ', userId);
    return this.https.post<Logoband>(url, { bandId });
  }

  getLogoEquip(equipId): Observable<Logoequip> {
    const url = `${this.BASE_URL}/assets/logoequip`;
    // console.log('request userId: ', userId);
    return this.https.post<Logoequip>(url, { equipId });
  }

  getLogoManage(manageId): Observable<Logomanage> {
    const url = `${this.BASE_URL}/assets/logomanage`;
    // console.log('request userId: ', userId);
    return this.https.post<Logomanage>(url, { manageId });
  }

  getLogoOrga(orgaId): Observable<Logoorga> {
    const url = `${this.BASE_URL}/assets/logoorga`;
    // console.log('request userId: ', userId);
    return this.https.post<Logoorga>(url, { orgaId });
  }

  getLogoService(serviceId): Observable<Logoservice> {
    const url = `${this.BASE_URL}/assets/logoservice`;
    // console.log('request userId: ', userId);
    return this.https.post<Logoservice>(url, { serviceId });
  }

  getLogoSite(siteId): Observable<Logosite> {
    const url = `${this.BASE_URL}/assets/logosite`;
    // console.log('request userId: ', userId);
    return this.https.post<Logosite>(url, { siteId });
  }
}
