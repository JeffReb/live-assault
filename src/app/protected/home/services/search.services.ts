import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivitiesSearch } from '../../../models/activities-search';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class SearchServices {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  getSearchActivities(
    locationfilter: string,
    locationdistance: string,
    locationlat: string,
    locationlon: string,
    namefilter: string,
    name: string,
    activityfilter: string,
    activities: string,
    typefilter: string,
    type: string,
    stylefilter: string,
    style: string
  ): Observable<ActivitiesSearch> {
    const url = `${this.BASE_URL}/elastic/test`;
    return this.https.post<ActivitiesSearch>(url, {
      locationfilter,
      locationdistance,
      locationlat,
      locationlon,
      namefilter,
      name,
      activityfilter,
      activities,
      typefilter,
      type,
      stylefilter,
      style
    });
  }
}
