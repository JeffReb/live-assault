import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { DashboardComponent, DialogGeoDashboardComponent } from './dashboard/dashboard.component';
import { VerticalMenuHomeComponent } from './components/menu/vertical-menu/vertical-menu-home.component';
import {
  DialogImgProfileSidenavHomeComponent,
  SidenavHomeComponent
} from './components/sidenav/sidenav-home.component';
import { SharedModule } from '../../shared/shared.module';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { HomeRoutingModule } from './home-routing.module';
import { UserMenuComponent } from './components/user-menu/user-menu.component';
import { FilePickerModule } from 'ngx-awesome-uploader';
import { BandCreateComponent, DialogCreateBandComponent } from './activity/band/create/band-create.component';
import { DialogCreateSiteComponent, SiteCreateComponent } from './activity/site/create/site-create.component';
import { DialogCreateManageComponent, ManageCreateComponent } from './activity/manage/create/manage-create.component';
import { DialogCreateOrgaComponent, OrgaCreateComponent } from './activity/orga/create/orga-create.component';
import {
  DialogCreateServiceComponent,
  ServiceCreateComponent
} from './activity/service/create/service-create.component';
import { DialogCreateEquipComponent, EquipCreateComponent } from './activity/equip/create/equip-create.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BandReadComponent } from './activity/band/read/band-read.component';
import { SiteReadComponent } from './activity/site/read/site-read.component';
import { ManageReadComponent } from './activity/manage/read/manage-read.component';
import { OrgaReadComponent } from './activity/orga/read/orga-read.component';
import { EquipReadComponent } from './activity/equip/read/equip-read.component';
import { ServiceReadComponent } from './activity/service/read/service-read.component';
import { UserReadComponent } from './users/read/user-read.component';
import { ChatComponent } from './chat/chat.component';
import {
  BandCompleteComponent,
  DialogLogoBandBandCompleteComponent
} from './activity/band/complete/band-complete.component';
import {
  DialogLogoEquipEquipCompleteComponent,
  EquipCompleteComponent
} from './activity/equip/complete/equip-complete.component';
import {
  DialogLogoManageManageCompleteComponent,
  ManageCompleteComponent
} from './activity/manage/complete/manage-complete.component';
import {
  DialogLogoOrgaOrgaCompleteComponent,
  OrgaCompleteComponent
} from './activity/orga/complete/orga-complete.component';
import {
  DialogLogoServiceServiceCompleteComponent,
  ServiceCompleteComponent
} from './activity/service/complete/service-complete.component';
import {
  DialogLogoSiteSiteCompleteComponent,
  SiteCompleteComponent
} from './activity/site/complete/site-complete.component';

@NgModule({
  declarations: [
    HomeComponent,
    DashboardComponent,
    DialogGeoDashboardComponent,
    BandCreateComponent,
    DialogCreateBandComponent,
    BandReadComponent,
    BandCompleteComponent,
    DialogLogoBandBandCompleteComponent,
    SiteCreateComponent,
    DialogCreateSiteComponent,
    SiteReadComponent,
    SiteCompleteComponent,
    DialogLogoSiteSiteCompleteComponent,
    ManageCreateComponent,
    DialogCreateManageComponent,
    ManageReadComponent,
    ManageCompleteComponent,
    DialogLogoManageManageCompleteComponent,
    OrgaCreateComponent,
    DialogCreateOrgaComponent,
    OrgaReadComponent,
    OrgaCompleteComponent,
    DialogLogoOrgaOrgaCompleteComponent,
    ServiceCreateComponent,
    DialogCreateServiceComponent,
    ServiceReadComponent,
    ServiceCompleteComponent,
    DialogLogoServiceServiceCompleteComponent,
    EquipCreateComponent,
    DialogCreateEquipComponent,
    EquipReadComponent,
    EquipCompleteComponent,
    DialogLogoEquipEquipCompleteComponent,
    UserReadComponent,
    ChatComponent,
    VerticalMenuHomeComponent,
    SidenavHomeComponent,
    BreadcrumbComponent,
    UserMenuComponent,
    DialogImgProfileSidenavHomeComponent
  ],
  imports: [CommonModule, SharedModule, HomeRoutingModule, FilePickerModule, FontAwesomeModule],
  entryComponents: [
    VerticalMenuHomeComponent,
    DialogImgProfileSidenavHomeComponent,
    DialogLogoBandBandCompleteComponent,
    DialogLogoSiteSiteCompleteComponent,
    DialogLogoManageManageCompleteComponent,
    DialogLogoOrgaOrgaCompleteComponent,
    DialogLogoServiceServiceCompleteComponent,
    DialogLogoEquipEquipCompleteComponent,
    DialogGeoDashboardComponent
  ]
})
export class HomeModule {}
