import { Component, OnInit, ViewChild, HostListener, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { MenuHomeService } from './components/menu/menu-home.service';
import { TransfertActivityService } from '../../services/activity/transfert.activity.service';
import { Socket } from 'ngx-socket-io';
import { SocketIoService } from '../../services/sockets/socket-io.service';

@Component({
  selector: 'app-pages',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MenuHomeService]
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('sidenav', { static: false }) sidenav: any;
  @ViewChild('backToTop', { static: true }) backToTop: any;
  @ViewChildren(PerfectScrollbarDirective) pss: QueryList<PerfectScrollbarDirective>;
  public settings: Settings;
  public menus = ['vertical', 'horizontal'];
  public menuOption: string;
  public menuTypes = ['default', 'compact', 'mini'];
  public menuTypeOption: string;
  public lastScrollTop = 0;
  public showBackToTop = false;
  public toggleSearchBar = false;
  private defaultMenu: string; // declared for return default menu when window resized

  constructor(
    public appSettings: AppSettings,
    public router: Router,
    private menuService: MenuHomeService,
    private transfertActivityService: TransfertActivityService,
    private socket: Socket,
    private socketIoService: SocketIoService
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this.socket.connect();
    this.socketIoService.setConnectedStatus('true').subscribe(data => {
      if (!data) {
      } else {
      }
    });
    if (window.innerWidth <= 1440) {
      this.settings.menu = 'vertical';
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
    }
    this.menuOption = this.settings.menu;
    this.menuTypeOption = this.settings.menuType;
    this.defaultMenu = this.settings.menu;
    const activityJson = localStorage.getItem('activity');
    // console.log('activityJson: ', activityJson);
    if (!activityJson) {
      const activityObj = this.transfertActivityService.getData();
      if (this.settings.menu === 'vertical') {
        this.menuService.expandActiveSubMenu(this.menuService.getVerticalMenuItems(activityObj));
      }
    } else {
      const activityObj = JSON.parse(activityJson);
      if (this.settings.menu === 'vertical') {
        this.menuService.expandActiveSubMenu(this.menuService.getVerticalMenuItems(activityObj));
      }
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.settings.loadingSpinner = false;
    }, 300);
    this.backToTop.nativeElement.style.display = 'none';
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (!this.settings.sidenavIsPinned) {
          this.sidenav.close();
        }
        if (window.innerWidth <= 768) {
          this.sidenav.close();
        }
      }
    });
    const activityJson = localStorage.getItem('activity');
    // console.log('activityJson: ', activityJson);
    if (!activityJson) {
      const activityObj = this.transfertActivityService.getData();
      if (this.settings.menu === 'vertical') {
        this.menuService.expandActiveSubMenu(this.menuService.getVerticalMenuItems(activityObj));
      }
    } else {
      const activityObj = JSON.parse(activityJson);
      if (this.settings.menu === 'vertical') {
        this.menuService.expandActiveSubMenu(this.menuService.getVerticalMenuItems(activityObj));
      }
    }
  }

  public chooseMenu() {
    this.settings.menu = this.menuOption;
    this.defaultMenu = this.menuOption;
    this.router.navigate(['/home/dashboard']);
  }

  public chooseMenuType() {
    this.settings.menuType = this.menuTypeOption;
  }

  public changeTheme(theme) {
    this.settings.theme = theme;
  }

  public toggleSidenav() {
    this.sidenav.toggle();
  }

  public onPsScrollY(event) {
    event.target.scrollTop > 300
      ? (this.backToTop.nativeElement.style.display = 'flex')
      : (this.backToTop.nativeElement.style.display = 'none');
    if (this.settings.menu === 'horizontal') {
      if (this.settings.fixedHeader) {
        const currentScrollTop = event.target.scrollTop > 56 ? event.target.scrollTop : 0;
        if (currentScrollTop > this.lastScrollTop) {
          document.querySelector('#horizontal-menu').classList.add('sticky');
          event.target.classList.add('horizontal-menu-hidden');
        } else {
          document.querySelector('#horizontal-menu').classList.remove('sticky');
          event.target.classList.remove('horizontal-menu-hidden');
        }
        this.lastScrollTop = currentScrollTop;
      } else {
        if (event.target.scrollTop > 56) {
          document.querySelector('#horizontal-menu').classList.add('sticky');
          event.target.classList.add('horizontal-menu-hidden');
        } else {
          document.querySelector('#horizontal-menu').classList.remove('sticky');
          event.target.classList.remove('horizontal-menu-hidden');
        }
      }
    }
  }

  public scrollToTop() {
    this.pss.forEach(ps => {
      if (ps.elementRef.nativeElement.id === 'main' || ps.elementRef.nativeElement.id === 'main-content') {
        ps.scrollToTop(0, 250);
      }
    });
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    if (window.innerWidth <= 768) {
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
      this.settings.menu = 'vertical';
    } else {
      this.defaultMenu === 'horizontal' ? (this.settings.menu = 'horizontal') : (this.settings.menu = 'vertical');
      this.settings.sidenavIsOpened = true;
      this.settings.sidenavIsPinned = true;
    }
  }

  public closeSubMenus() {
    const menu = document.querySelector('.sidenav-menu-outer');
    if (menu) {
      for (let i = 0; i < menu.children[0].children.length; i++) {
        const child = menu.children[0].children[i];
        if (child) {
          if (child.children[0].classList.contains('expanded')) {
            child.children[0].classList.remove('expanded');
            child.children[1].classList.remove('show');
          }
        }
      }
    }
  }
}
