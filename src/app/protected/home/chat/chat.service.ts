import { Injectable } from '@angular/core';
import { Chat } from './chat.model';
import { ContactModel } from './contact.model';

@Injectable()
export class ChatService {
  private date = new Date();
  private day = this.date.getDate();
  private month = this.date.getMonth();
  private year = this.date.getFullYear();
  private hour = this.date.getHours();
  private minute = this.date.getMinutes();

  private contacts = [
    new ContactModel('Ashley', 'assets/img/profile/ashley.jpg', 'Ashley Ahlberg', 'Online'),
    new ContactModel('Bruno', 'assets/img/profile/bruno.jpg', 'Bruno Vespa', 'Offline'),
    new ContactModel('Julia', 'assets/img/profile/julia.jpg', 'Julia Aniston', 'Online'),
    new ContactModel('Adam', 'assets/img/profile/adam.jpg', 'Adam Sandler', 'Online'),
    new ContactModel('Tereza', 'assets/img/profile/tereza.jpg', 'Tereza Stiles', 'Online'),
    new ContactModel('Michael', 'assets/img/profile/michael.jpg', 'Michael Blair', 'Online')
  ];

  private chats = [
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Oh Ashley buy !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'And i thinf is good !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      true
    ),
    new Chat(
      'Bruno',
      'assets/img/profile/bruno.jpg',
      'Bruno Vespa',
      'Do not disturb',
      'Great, then I ll definitely buy this theme. Thanks!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Julia',
      'assets/img/profile/julia.jpg',
      'Julia Aniston',
      'Away',
      'Great, then I ll definitely buy this theme. Thanks!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Adam',
      'assets/img/profile/adam.jpg',
      'Adam Sandler',
      'Online',
      'Great, then I ll definitely buy this theme. Thanks!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Tereza',
      'assets/img/profile/tereza.jpg',
      'Tereza Stiles',
      'Offline',
      'Great, then I ll definitely buy this theme. Thanks!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Michael',
      'assets/img/profile/michael.jpg',
      'Michael Blair',
      'Online',
      'Great, then I ll definitely buy this theme. Thanks!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    )
  ];

  private talks = [
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Salut je suis sur le talk',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute + 3),
      false
    ),
    new Chat(
      'Emilio',
      'assets/img/users/user.png',
      'Emilio Verdines',
      'Online',
      'Moi aussi emilio',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute + 2),
      true
    ),
    new Chat(
      'Michael',
      'assets/img/profile/michael.jpg',
      'Michael Blair',
      'Online',
      'Hi !! I m Michael !!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    )
  ];

  private send = [
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Salut Ashley je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Salut Ashley je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Bruno',
      'assets/img/profile/bruno.jpg',
      'Bruno Vespa',
      'Do not disturb',
      'Salut Bruno je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Julia',
      'assets/img/profile/julia.jpg',
      'Julia Aniston',
      'Away',
      'Salut Julia je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Adam',
      'assets/img/profile/adam.jpg',
      'Adam Sandler',
      'Online',
      'Salut Adam je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Tereza',
      'assets/img/profile/tereza.jpg',
      'Tereza Stiles',
      'Offline',
      'Salut Tereza je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Michael',
      'assets/img/profile/michael.jpg',
      'Michael Blair',
      'Online',
      'Salut Michael je t envoi ce message !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    )
  ];

  private receive = [
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Salut c Ashley !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Ashley',
      'assets/img/profile/ashley.jpg',
      'Ashley Ahlberg',
      'Online',
      'Salut c Ashley  !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Bruno',
      'assets/img/profile/bruno.jpg',
      'Bruno Vespa',
      'Do not disturb',
      'Salut c Bruno !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Julia',
      'assets/img/profile/julia.jpg',
      'Julia Aniston',
      'Away',
      'Salut c Julia  !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Adam',
      'assets/img/profile/adam.jpg',
      'Adam Sandler',
      'Online',
      'Salut c Adam !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Tereza',
      'assets/img/profile/tereza.jpg',
      'Tereza Stiles',
      'Offline',
      'Salut c Tereza !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    ),
    new Chat(
      'Michael',
      'assets/img/profile/michael.jpg',
      'Michael Blair',
      'Online',
      'Salut c Michael !!!!',
      new Date(this.year, this.month, this.day - 2, this.hour, this.minute),
      false
    )
  ];

  public getChats(): Array<Chat> {
    return this.chats;
  }

  public getTalk(): Array<Chat> {
    // console.log(this.talks);
    return this.talks;
  }

  public getContacts(): Array<ContactModel> {
    return this.contacts;
  }

  public getSend(): Array<Chat> {
    return this.send;
  }

  public getReceive(): Array<Chat> {
    return this.receive;
  }
}
