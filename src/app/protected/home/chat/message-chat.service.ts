import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MessageModel } from './message.model';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class MessageChatService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  readTchatMessages(): Observable<MessageModel> {
    const url = `${this.BASE_URL}/messagestchat/read`;
    return this.https.get<MessageModel>(url);
  }

  sendTchatMessage(contactId, message): Observable<MessageModel> {
    const url = `${this.BASE_URL}/messagestchat/send`;
    return this.https.post<MessageModel>(url, { contactId, message });
  }

  setChatWith(contactId): Observable<MessageModel> {
    const url = `${this.BASE_URL}/messagestchat/setchatwith`;
    return this.https.post<MessageModel>(url, { contactId });
  }

  removeChatWith(): Observable<MessageModel> {
    const url = `${this.BASE_URL}/messagestchat/removechatwith`;
    return this.https.get<MessageModel>(url);
  }
}
