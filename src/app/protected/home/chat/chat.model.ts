export class Chat {
  constructor(
    public authorId: string,
    public image: string,
    public author: string,
    public authorStatus: string,
    public text: string,
    public date: Date,
    public my: boolean
  ) {}
}
