export class ContactModel {
  constructor(public authorId: string, public image: string, public author: string, public authorStatus: string) {}
}
