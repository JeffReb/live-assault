export class MessageModel {
  messages?: {
    userId?: string;
    messageId?: string;
    userContactId?: string;
    fluxType?: string;
    message?: string;
    addedDate?: string;
    ok?: string;
  }[];
}
