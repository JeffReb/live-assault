import { environment } from '../../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ContactsModel } from './contacts.model';

const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private BASE_URL = APIEndpoint;

  constructor(private https: HttpClient) {}

  listingContacts(): Observable<ContactsModel> {
    const url = `${this.BASE_URL}/contacts/listingcontact`;
    return this.https.get<ContactsModel>(url);
  }
}
