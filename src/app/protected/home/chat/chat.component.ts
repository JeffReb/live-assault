import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChatService } from './chat.service';
import { Settings } from '../../../app.settings.model';
import { Chat } from './chat.model';
import { AppSettings } from '../../../app.settings';
import { ContactModel } from './contact.model';
import { environment } from '../../../../environments/environment';
import { AssetsServices } from '../services/assets.services';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ContactService } from './contact.service';
import { UserService } from '../../../services/users/user.service';
import { MessageChatService } from './message-chat.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { SocketIoService } from '../../../services/sockets/socket-io.service';

const APICloud = environment.APICloud;

const helper = new JwtHelperService();

const contactChat: {
  authorId: string;
  image: string;
  author: string;
  authorStatus: string;
}[] = [];

const talkChat: {
  authorId: string;
  image: string;
  author: string;
  authorStatus: string;
  text: string;
  date: string;
  my: boolean;
  dateSort: string;
  messageID: string;
}[] = [];

const sortTalkChat: {
  authorId: string;
  image: string;
  author: string;
  authorStatus: string;
  text: string;
  date: string;
  my: boolean;
  dateSort: string;
  messageID: string;
}[] = [];

@Component({
  selector: 'app-protected-home-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
  providers: [ChatService]
})
export class ChatComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', { static: true }) sidenav: any;
  public settings: Settings;
  public userImage = '../../../../assets/img/users/user.png';
  public chats: Array<Chat>;
  public contacts: Array<ContactModel>;
  public talks: Array<Chat>; // a remplacer par talk
  public sidenavOpen = true;
  public currentChat: Chat;
  public newMessage: string;
  public myID: string;
  public myName: string;
  public talk: Array<Chat>; // nouveau talk
  public chatPresenceMess: boolean;
  public chatFromId: string;
  public myImageUser = '../../../../assets/img/users/user.png';
  public currAuthorId: string;
  public currImage: string;
  public currAuthor: string;
  public currAuthStatus: string;
  public sendMessDisabled: boolean;
  public idContactSelect: string;
  public idDetailsMessage: string;

  sortTalkTable = sortTalkChat;

  contactChatTable = contactChat;
  talkChatTable = talkChat;

  constructor(
    public appSettings: AppSettings,
    private chatService: ChatService,
    private assetsService: AssetsServices,
    private contactService: ContactService,
    private userService: UserService,
    private messageChatService: MessageChatService,
    private router: Router,
    private socket: Socket,
    private socketIoService: SocketIoService,
    private route: ActivatedRoute
  ) {
    this.settings = this.appSettings.settings;
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    this.idContactSelect = '';
    this.socketIoService.getMessages().subscribe((message: any) => {
      // message.mess => contenu du message en provenance de socket.io server
      // this.idContactSelect => id du contact selectionner en cours ds la discussion
      // message.idFrom => id du contact qui a envoyer le message via socket.io server (idExpéditeur du message)
      if (this.idContactSelect === message.idFrom) {
        const talkDate = new Date().toDateString();
        const timstampStr = Math.round(new Date().getTime()).toString();
        const talkDateStr = talkDate.toString();
        // console.warn('timstampStr: ', timstampStr);
        // console.log('ça colle !!!!');
        this.assetsService.getImgUser(message.idFrom).subscribe(dataImg => {
          if (!dataImg) {
          } else {
            const Filename = dataImg.filename;
            if (Filename === 'ImgProfile not found') {
              const currentContImg = '../../../../assets/img/users/user.png';
              this.newMessage = message.mess;
              this.sortTalkTable.push({
                authorId: this.idContactSelect,
                image: currentContImg,
                author: 'ppfff',
                authorStatus: 'Online',
                text: this.newMessage,
                date: timstampStr,
                my: false,
                dateSort: talkDateStr,
                messageID: '12458'
              });
              this.newMessage = '';
              const chatContainer = document.querySelector('.chat-content');
              if (chatContainer) {
                setTimeout(() => {
                  const nodes = chatContainer.querySelectorAll('.mat-list-item');
                  const newChatTextHeight = nodes[nodes.length - 1];
                  chatContainer.scrollTop = chatContainer.scrollHeight + newChatTextHeight.clientHeight;
                });
              }
            } else {
              const currentContImg = APICloud + '/' + message.idFrom + '/' + Filename;
              this.newMessage = message.mess;
              this.sortTalkTable.push({
                authorId: this.idContactSelect,
                image: currentContImg,
                author: 'ppfff',
                authorStatus: 'Online',
                text: this.newMessage,
                date: timstampStr,
                my: false,
                dateSort: talkDateStr,
                messageID: '12458'
              });
              this.newMessage = '';
              const chatContainer = document.querySelector('.chat-content');
              if (chatContainer) {
                setTimeout(() => {
                  const nodes = chatContainer.querySelectorAll('.mat-list-item');
                  const newChatTextHeight = nodes[nodes.length - 1];
                  chatContainer.scrollTop = chatContainer.scrollHeight + newChatTextHeight.clientHeight;
                });
              }
            }
          }
        });
      }
    });
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.myName = helper.decodeToken(tokenUser)['userName'];
    this.contactChatTable = [];
    this.talkChatTable = [];
    this.currAuthorId = '';
    this.chatPresenceMess = true;
    this.sendMessDisabled = true;
    this.assetsService.getImgProfile().subscribe(dataMyImage => {
      if (!dataMyImage) {
      } else {
        const Filename = dataMyImage.filename;
        if (Filename === 'ImgProfile not found') {
          this.myImageUser = '../assets/img/users/user.png';
        } else {
          this.myImageUser = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });
    this.contactService.listingContacts().subscribe(datacontact => {
      if (!datacontact) {
      } else {
        if (datacontact.contactsId === null) {
        } else {
          const contactListLength = datacontact.contactsId.length;
          for (let i = 0; i < contactListLength; i++) {
            // console.log('datacontact: ', datacontact.contactsId[i]);

            this.userService.readUser(datacontact.contactsId[i]).subscribe(dataUser => {
              if (!dataUser) {
              } else {
                // console.log('dataUserName: ', dataUser.result[0].user.name, 'userId: ', dataUser.result[0].user.userId);
                const contactName = dataUser.result[0].user.name;
                this.assetsService.getImgUser(dataUser.result[0].user.userId).subscribe(dataImgUser => {
                  if (!dataImgUser) {
                    this.userImage = '../../../../assets/img/users/user.png';
                  } else {
                    const Filename = dataImgUser.filename;
                    if (Filename === 'ImgProfile not found') {
                      this.userImage = '../../../../assets/img/users/user.png';
                    } else {
                      this.userImage = APICloud + '/' + dataUser.result[0].user.userId + '/' + Filename;
                    }
                  }
                  // console.log('userImage: ', this.userImage);
                  const imageContact = this.userImage;
                  const contactId = dataUser.result[0].user.userId;
                  this.contactChatTable.push({
                    authorId: contactId,
                    image: imageContact,
                    author: contactName,
                    authorStatus: dataUser.result[0].user.authorStatus
                  });
                });
              }
            });
          }
          // console.log('datacontact: ', datacontact.contactsId.length);
          // console.log('contactTable: ', this.contactChatTable);
        }
      }
    });

    this.chats = this.chatService.getChats();

    this.contacts = this.chatService.getContacts();
    if (window.innerWidth <= 768) {
      this.sidenavOpen = false;
    }

    this.assetsService.getImgUser(this.myID).subscribe(dataimg => {
      if (!dataimg) {
      } else {
        const Filename = dataimg.filename;
        if (Filename === 'ImgProfile not found') {
          this.userImage = '../../../../assets/img/users/user.png';
        } else {
          this.userImage = APICloud + '/' + this.myID + '/' + Filename;
        }
      }
    });

    this.route.queryParams.subscribe(params => {
      if (!params) {
      } else {
        this.idDetailsMessage = params.id;
        this.getContactTalk(this.idDetailsMessage);
      }
    });
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    window.innerWidth <= 768 ? (this.sidenavOpen = false) : (this.sidenavOpen = true);
  }

  public getContactTalk(idContact) {
    // nouvelle fonction talk
    this.talkChatTable = [];
    this.sortTalkTable = [];
    this.sendMessDisabled = false;
    this.idContactSelect = idContact;
    this.userService.readUser(idContact).subscribe(dataUser => {
      if (!dataUser) {
      } else {
        const currentContactName = dataUser.result[0].user.name;
        this.assetsService.getImgUser(idContact).subscribe(dataImg => {
          if (!dataImg) {
          } else {
            const Filename = dataImg.filename;
            if (Filename === 'ImgProfile not found') {
              const currentContImg = '../../../../assets/img/users/user.png';
              this.currAuthorId = idContact;
              this.currImage = currentContImg;
              this.currAuthor = currentContactName;
              this.currAuthStatus = 'Online';
            } else {
              const currentContImg = APICloud + '/' + idContact + '/' + Filename;
              this.currAuthorId = idContact;
              this.currImage = currentContImg;
              this.currAuthor = currentContactName;
              this.currAuthStatus = 'Online';
            }
          }
        });
      }
    });

    this.messageChatService.readTchatMessages().subscribe(data => {
      if (!data || data.messages.length === 0) {
        this.chatPresenceMess = false;
      } else {
        this.chatPresenceMess = true;
        const messLength = data.messages.length;

        for (let i = 0; i < messLength; i++) {
          this.chatFromId = data.messages[i].userContactId;
          const FromId = data.messages[i].userContactId;
          if (FromId === idContact) {
            const FluxType = data.messages[i].fluxType;
            if (FluxType === 'send') {
              const talkAuthorId = this.myID;
              const talkImage = this.myImageUser;
              const talkName = this.myName;
              const talkText = data.messages[i].message;
              const talkDate = data.messages[i].addedDate;
              const talkDateSort = talkDate.toString();
              const messageId = data.messages[i].messageId;
              // console.log('talkDateSort: ', talkDateSort);
              this.talkChatTable.push({
                authorId: talkAuthorId,
                image: talkImage,
                author: talkName,
                authorStatus: 'Online',
                text: talkText,
                date: talkDate,
                my: true,
                dateSort: talkDateSort,
                messageID: messageId
              });
            } else {
              const talkAuthorId = data.messages[i].userContactId;
              const talkText = data.messages[i].message;
              const talkDate = data.messages[i].addedDate;
              const talkDateSort = talkDate.toString();
              const messageId = data.messages[i].messageId;
              // console.log('talkDateSort: ', talkDateSort);
              this.userService.readUser(talkAuthorId).subscribe(dataUser => {
                if (!dataUser) {
                } else {
                  const talkName = dataUser.result[0].user.name;
                  this.assetsService.getImgUser(talkAuthorId).subscribe(dataImg => {
                    if (!dataImg) {
                    } else {
                      const Filename = dataImg.filename;
                      if (Filename === 'ImgProfile not found') {
                        const talkImage = '../../../../assets/img/users/user.png';
                        this.talkChatTable.push({
                          authorId: talkAuthorId,
                          image: talkImage,
                          author: talkName,
                          authorStatus: 'Online',
                          text: talkText,
                          date: talkDate,
                          my: false,
                          dateSort: talkDateSort,
                          messageID: messageId
                        });
                      } else {
                        const talkImage = APICloud + '/' + talkAuthorId + '/' + Filename;
                        this.talkChatTable.push({
                          authorId: talkAuthorId,
                          image: talkImage,
                          author: talkName,
                          authorStatus: 'Online',
                          text: talkText,
                          date: talkDate,
                          my: false,
                          dateSort: talkDateSort,
                          messageID: messageId
                        });
                      }
                    }
                  });
                }
              });
            }
          }
        }
      }
    });
    // console.log('talkChatTable: ', this.talkChatTable);

    this.messageChatService.setChatWith(idContact).subscribe(data => {
      if (!data) {
      } else {
      }
    });

    setTimeout(() => {
      this.sortTalkTable = this.talkChatTable.sort((a, b) =>
        a.dateSort < b.dateSort ? -1 : a.dateSort > b.dateSort ? 1 : 0
      );
      // console.log('sortedTalkChatTable: ', this.sortTalkTable);
    }, 1000);

    // this.sortTalkTable = this.talkChatTable.sort((b, a) => a.text < b.text ? -1 : a.text > b.text ? 1 : 0);
    // console.log('sortedTalkChatTable: ', this.sortTalkTable);
    const testTri = [
      {
        name: 'Albert',
        phone: '01234'
      },
      {
        name: 'Pierre',
        phone: '01234'
      },
      {
        name: 'Claude',
        phone: '01234'
      }
    ];

    const testTriSort = testTri.sort((b, a) => (a.name < b.name ? -1 : a.name > b.name ? 1 : 0));

    // console.log('testTriSort: ', testTriSort);
  }

  public testSort() {
    // console.log('test');
  }

  public getChat(obj) {
    // fonction a remplacer
    if (this.talks) {
      this.talks.length = 2;
    }
    this.talks = this.chatService.getTalk();
    this.chats = this.chatService.getChats();
    this.chats.forEach(chat => {
      if (chat.authorId === obj.authorId) {
        this.currentChat = chat;
      }
    });
    // this.talks.push(obj);
    // this.currentChat = obj;
    // console.log('this.talks: ', this.talks);
    // console.log('this.currentChat', this.currentChat);
    this.talks.forEach(talk => {
      if (!talk.my) {
        talk.image = obj.image;
      } else {
        talk.image = this.userImage;
      }
    });
    if (window.innerWidth <= 768) {
      this.sidenav.close();
    }
  }

  public sendMessage($event) {
    if (($event.which === 1 || $event.which === 13) && this.newMessage.trim() !== '') {
      if (this.talkChatTable) {
        const talkDate = new Date().toDateString();
        const timstampStr = Math.round(new Date().getTime()).toString();
        const talkDateStr = talkDate.toString();
        // console.warn('timstampStr: ', timstampStr);
        // this.talkChatTable.push({
        //   authorId: this.myID,
        //   image: this.userImage,
        //   author: this.myName,
        //   authorStatus: 'Online',
        //   text: this.newMessage,
        //   date: talkDate,
        //   my: true,
        //   dateSort: talkDateStr,
        //   messageID: '124587'
        // });
        this.sortTalkTable.push({
          authorId: this.myID,
          image: this.userImage,
          author: this.myName,
          authorStatus: 'Online',
          text: this.newMessage,
          date: timstampStr,
          my: true,
          dateSort: talkDateStr,
          messageID: '124587'
        });
        if (!this.newMessage || this.currAuthorId === '') {
        } else {
          this.messageChatService.sendTchatMessage(this.currAuthorId, this.newMessage).subscribe(data => {});
          const messSocket = [
            {
              idUserDest: this.currAuthorId,
              content: {
                idFrom: this.myID,
                mess: this.newMessage
              }
            }
          ];
          this.socket.emit('chat message', messSocket);
        }

        this.newMessage = '';
        const chatContainer = document.querySelector('.chat-content');
        if (chatContainer) {
          setTimeout(() => {
            const nodes = chatContainer.querySelectorAll('.mat-list-item');
            const newChatTextHeight = nodes[nodes.length - 1];
            chatContainer.scrollTop = chatContainer.scrollHeight + newChatTextHeight.clientHeight;
          });
        }
      }
    }
  }

  ngOnDestroy() {
    if (this.talkChatTable) {
      this.talkChatTable = [];
      this.sortTalkTable = [];
      this.sendMessDisabled = true;
    }
    this.messageChatService.removeChatWith().subscribe(data => {
      if (!data) {
      } else {
      }
    });
  }
}
