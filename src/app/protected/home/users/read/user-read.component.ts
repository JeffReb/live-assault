import { AfterViewInit, Component, OnInit } from '@angular/core';
import { TransfertActivityService } from '../../../../services/activity/transfert.activity.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/users/user.service';
import { DatesAndTimes } from '../../../../services/utils/dates-and-times';
import { ContactsService } from '../../../../services/users/contacts.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AssetsServices } from '../../services/assets.services';
import { environment } from '../../../../../environments/environment';
import { BandService } from '../../../../services/activity/bands/band.service';
import { EquipService } from '../../../../services/activity/equip/equip.service';
import { ManageService } from '../../../../services/activity/manage/manage.service';
import { OrgaService } from '../../../../services/activity/orga/orga.service';
import { ServicesService } from '../../../../services/activity/services/services.service';
import { SiteService } from '../../../../services/activity/sites/site.service';

const helper = new JwtHelperService();

const APICloud = environment.APICloud;

const bandRead: {
  bandReadid: string;
  bandReaduserid: string;
  bandReadname: string;
  bandReadaddedDate: string;
  bandReadcity: string;
  bandReadcityalt1: string;
  bandReadcityalt2: string;
  bandReadcountry: string;
  bandReadcountrycode: string;
  bandReadcontinent: string;
  bandReadlatitude: string;
  bandReadlongitude: string;
  bandReademail: string;
  bandReadstyle: string;
  bandReadrole: string;
  bandReadLogo: string;
}[] = [];

const equipRead: {
  equipReadid: string;
  equipReaduserid: string;
  equipReadname: string;
  equipReadaddedDate: string;
  equipReadaddress: string;
  equipReadcity: string;
  equipReadcityalt1: string;
  equipReadcityalt2: string;
  equipReadcountry: string;
  equipReadcountrycode: string;
  equipReadcontinent: string;
  equipReadlatitude: string;
  equipReadlongitude: string;
  equipReademail: string;
  equipReadphone: string;
  equipReadtype: string;
  equipReadLogo: string;
}[] = [];

const manageRead: {
  manageReadid: string;
  manageReaduserid: string;
  manageReadname: string;
  manageReadaddedDate: string;
  manageReadaddress: string;
  manageReadcity: string;
  manageReadcityalt1: string;
  manageReadcityalt2: string;
  manageReadcountry: string;
  manageReadcountrycode: string;
  manageReadcontinent: string;
  manageReadlatitude: string;
  manageReadlongitude: string;
  manageReademail: string;
  manageReadphone: string;
  manageReadtype: string;
  manageReadstyle: string;
  manageReadstatus: string;
  manageReadLogo: string;
}[] = [];

const orgaRead: {
  orgaReadid: string;
  orgaReaduserid: string;
  orgaReadname: string;
  orgaReadaddedDate: string;
  orgaReadaddress: string;
  orgaReadcity: string;
  orgaReadcityalt1: string;
  orgaReadcityalt2: string;
  orgaReadcountry: string;
  orgaReadcountrycode: string;
  orgaReadcontinent: string;
  orgaReadlatitude: string;
  orgaReadlongitude: string;
  orgaReademail: string;
  orgaReadphone: string;
  orgaReadtype: string;
  orgaReadstyle: string;
  orgaReadstatus: string;
  orgaReadLogo: string;
}[] = [];

const serviceRead: {
  serviceReadid: string;
  serviceReaduserid: string;
  serviceReadname: string;
  serviceReadaddedDate: string;
  serviceReadaddress: string;
  serviceReadcity: string;
  serviceReadcityalt1: string;
  serviceReadcityalt2: string;
  serviceReadcountry: string;
  serviceReadcountrycode: string;
  serviceReadcontinent: string;
  serviceReadlatitude: string;
  serviceReadlongitude: string;
  serviceReademail: string;
  serviceReadphone: string;
  serviceReadstyle: string;
  serviceReadstatus: string;
  serviceReadservice: string;
  serviceReadLogo: string;
}[] = [];

const siteRead: {
  siteReadid: string;
  siteReaduserid: string;
  siteReadname: string;
  siteReadaddedDate: string;
  siteReadaddress: string;
  siteReadcity: string;
  siteReadcityalt1: string;
  siteReadcityalt2: string;
  siteReadcountry: string;
  siteReadcountrycode: string;
  siteReadcontinent: string;
  siteReadlatitude: string;
  siteReadlongitude: string;
  siteReademail: string;
  siteReadphone: string;
  siteReadtype: string;
  siteReadstyle: string;
  siteReadcapacity: string;
  siteReadLogo: string;
}[] = [];

@Component({
  selector: 'app-protected-home-user-read',
  templateUrl: './user-read.component.html',
  styleUrls: ['./user-read.component.scss']
})
export class UserReadComponent implements OnInit, AfterViewInit {
  public userImage = '../assets/img/users/user.png';
  public logoImage = '../assets/img/activity/logo.png';
  public idUserRead: string;
  public idDetails: string;
  public userReadName: string;
  public userReadEmail: string;
  public userReadAddedDate: string;
  public userReadActivities: string;
  public MemberSince: string;
  public contactState: string;
  public myID: string;
  public bandReadPres: boolean;
  public equipReadPres: boolean;
  public manageReadPres: boolean;
  public orgaReadPres: boolean;
  public serviceReadPres: boolean;
  public siteReadPres: boolean;

  bandReadTable = bandRead;
  equipReadTable = equipRead;
  manageReadTable = manageRead;
  orgaReadTable = orgaRead;
  serviceReadTable = serviceRead;
  siteReadTable = siteRead;

  constructor(
    private transfertActivityService: TransfertActivityService,
    private route: ActivatedRoute,
    private userService: UserService,
    private dateAndTimes: DatesAndTimes,
    private contactService: ContactsService,
    private assetsService: AssetsServices,
    private router: Router,
    private bandService: BandService,
    private equipService: EquipService,
    private manageService: ManageService,
    private orgaService: OrgaService,
    private serviceService: ServicesService,
    private siteService: SiteService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit() {
    const tokenUser = localStorage.getItem('token');
    this.myID = helper.decodeToken(tokenUser)['userId'];
    this.bandReadTable = [];
    this.equipReadTable = [];
    this.manageReadTable = [];
    this.orgaReadTable = [];
    this.serviceReadTable = [];
    this.siteReadTable = [];
    // this.bandReadPres = false;
    // this.equipReadPres = false;
    // this.manageReadPres = false;
    // this.orgaReadPres = false;
    // this.serviceReadPres = false;
    // this.siteReadPres = false;

    // if (!userID) {
    //   this.route.queryParams.subscribe(params => {
    //     this.idDetails = params.id;
    //   });
    //   this.idUserRead = this.idDetails;
    //   this.getReadUserById(this.idUserRead);
    // } else {
    //   this.idUserRead = userID;
    //   this.getReadUserById(this.idUserRead);
    // }

    this.route.queryParams.subscribe(params => {
      this.idDetails = params.id;
    });
    this.idUserRead = this.idDetails;
    const userId = this.idUserRead;
    // this.getReadUserById(this.idUserRead);

    // this.activeRoute.queryParams.subscribe(queryParams => {
    //     // do something with the query params
    // });	this.activeRoute.params.subscribe(routeParams => {
    //     this.loadUserDetail(routeParams.id);
    // });

    this.userService.readUser(userId).subscribe(data => {
      if (!data) {
      } else {
        // console.log('data name user: ', data.name);
        this.userReadName = data.result[0].user.name;
        this.userReadEmail = data.result[0].user.email;
        this.userReadAddedDate = data.result[0].user.addeddate;
        this.userReadActivities = data.result[0].user.activitiesId;
        if (this.userReadActivities === null) {
          // console.log('null');
        } else {
          const readActivitiesLength = this.userReadActivities.length;
          if (readActivitiesLength === 0) {
          } else {
            for (let i = 0; i < readActivitiesLength; i++) {
              // console.log('data.result[0].user.activitiesId', data.result[0].user.activitiesId[i]);
              const activityId = data.result[0].user.activitiesId[i];
              this.bandService.readBand(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.bandReadid === '') {
                  } else {
                    this.assetsService.getLogoBand(dataActivity.bandReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logoband not found') {
                          const logoImg = '../assets/img/activity/logo.png';
                          this.bandReadTable.push({
                            bandReadid: dataActivity.bandReadid,
                            bandReaduserid: dataActivity.bandReaduserid,
                            bandReadname: dataActivity.bandReadname,
                            bandReadaddedDate: dataActivity.bandReadaddedDate,
                            bandReadcity: dataActivity.bandReadcity,
                            bandReadcityalt1: dataActivity.bandReadcityalt1,
                            bandReadcityalt2: dataActivity.bandReadcityalt2,
                            bandReadcountry: dataActivity.bandReadcountry,
                            bandReadcountrycode: dataActivity.bandReadcountrycode,
                            bandReadcontinent: dataActivity.bandReadcontinent,
                            bandReadlatitude: dataActivity.bandReadlatitude,
                            bandReadlongitude: dataActivity.bandReadlongitude,
                            bandReademail: dataActivity.bandReademail,
                            bandReadstyle: dataActivity.bandReadstyle,
                            bandReadrole: dataActivity.bandReadrole,
                            bandReadLogo: logoImg
                          });
                        } else {
                          const logoImg = APICloud + '/' + userId + '/' + Filename;
                          this.bandReadTable.push({
                            bandReadid: dataActivity.bandReadid,
                            bandReaduserid: dataActivity.bandReaduserid,
                            bandReadname: dataActivity.bandReadname,
                            bandReadaddedDate: dataActivity.bandReadaddedDate,
                            bandReadcity: dataActivity.bandReadcity,
                            bandReadcityalt1: dataActivity.bandReadcityalt1,
                            bandReadcityalt2: dataActivity.bandReadcityalt2,
                            bandReadcountry: dataActivity.bandReadcountry,
                            bandReadcountrycode: dataActivity.bandReadcountrycode,
                            bandReadcontinent: dataActivity.bandReadcontinent,
                            bandReadlatitude: dataActivity.bandReadlatitude,
                            bandReadlongitude: dataActivity.bandReadlongitude,
                            bandReademail: dataActivity.bandReademail,
                            bandReadstyle: dataActivity.bandReadstyle,
                            bandReadrole: dataActivity.bandReadrole,
                            bandReadLogo: logoImg
                          });
                        }
                      }
                    });
                  }
                }
              });
              this.equipService.readEquip(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.equipReadid === '') {
                  } else {
                    this.assetsService.getLogoEquip(dataActivity.equipReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logoequip not found') {
                          const logoImg = '../assets/img/activity/logo.png';
                          this.equipReadTable.push({
                            equipReadid: dataActivity.equipReadid,
                            equipReaduserid: dataActivity.equipReaduserid,
                            equipReadname: dataActivity.equipReadname,
                            equipReadaddedDate: dataActivity.equipReadaddedDate,
                            equipReadaddress: dataActivity.equipReadaddress,
                            equipReadcity: dataActivity.equipReadcity,
                            equipReadcityalt1: dataActivity.equipReadcityalt1,
                            equipReadcityalt2: dataActivity.equipReadcityalt2,
                            equipReadcountry: dataActivity.equipReadcountry,
                            equipReadcountrycode: dataActivity.equipReadcountrycode,
                            equipReadcontinent: dataActivity.equipReadcontinent,
                            equipReadlatitude: dataActivity.equipReadlatitude,
                            equipReadlongitude: dataActivity.equipReadlongitude,
                            equipReademail: dataActivity.equipReademail,
                            equipReadphone: dataActivity.equipReadphone,
                            equipReadtype: dataActivity.equipReadtype,
                            equipReadLogo: logoImg
                          });
                        } else {
                          const logoImg = APICloud + '/' + userId + '/' + Filename;
                          this.equipReadTable.push({
                            equipReadid: dataActivity.equipReadid,
                            equipReaduserid: dataActivity.equipReaduserid,
                            equipReadname: dataActivity.equipReadname,
                            equipReadaddedDate: dataActivity.equipReadaddedDate,
                            equipReadaddress: dataActivity.equipReadaddress,
                            equipReadcity: dataActivity.equipReadcity,
                            equipReadcityalt1: dataActivity.equipReadcityalt1,
                            equipReadcityalt2: dataActivity.equipReadcityalt2,
                            equipReadcountry: dataActivity.equipReadcountry,
                            equipReadcountrycode: dataActivity.equipReadcountrycode,
                            equipReadcontinent: dataActivity.equipReadcontinent,
                            equipReadlatitude: dataActivity.equipReadlatitude,
                            equipReadlongitude: dataActivity.equipReadlongitude,
                            equipReademail: dataActivity.equipReademail,
                            equipReadphone: dataActivity.equipReadphone,
                            equipReadtype: dataActivity.equipReadtype,
                            equipReadLogo: logoImg
                          });
                        }
                      }
                    });
                  }
                }
              });
              this.manageService.readManage(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.manageReadid === '') {
                  } else {
                    this.assetsService.getLogoManage(dataActivity.manageReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logomanage not found') {
                          const logoImgManage = '../assets/img/activity/logo.png';
                          this.manageReadTable.push({
                            manageReadid: dataActivity.manageReadid,
                            manageReaduserid: dataActivity.manageReaduserid,
                            manageReadname: dataActivity.manageReadname,
                            manageReadaddedDate: dataActivity.manageReadaddedDate,
                            manageReadaddress: dataActivity.manageReadaddress,
                            manageReadcity: dataActivity.manageReadcity,
                            manageReadcityalt1: dataActivity.manageReadcityalt1,
                            manageReadcityalt2: dataActivity.manageReadcityalt2,
                            manageReadcountry: dataActivity.manageReadcountry,
                            manageReadcountrycode: dataActivity.manageReadcountrycode,
                            manageReadcontinent: dataActivity.manageReadcontinent,
                            manageReadlatitude: dataActivity.manageReadlatitude,
                            manageReadlongitude: dataActivity.manageReadlongitude,
                            manageReademail: dataActivity.manageReademail,
                            manageReadphone: dataActivity.manageReadphone,
                            manageReadtype: dataActivity.manageReadtype,
                            manageReadstyle: dataActivity.manageReadstyle,
                            manageReadstatus: dataActivity.manageReadstatus,
                            manageReadLogo: logoImgManage
                          });
                        } else {
                          const logoImgManage = APICloud + '/' + userId + '/' + Filename;
                          this.manageReadTable.push({
                            manageReadid: dataActivity.manageReadid,
                            manageReaduserid: dataActivity.manageReaduserid,
                            manageReadname: dataActivity.manageReadname,
                            manageReadaddedDate: dataActivity.manageReadaddedDate,
                            manageReadaddress: dataActivity.manageReadaddress,
                            manageReadcity: dataActivity.manageReadcity,
                            manageReadcityalt1: dataActivity.manageReadcityalt1,
                            manageReadcityalt2: dataActivity.manageReadcityalt2,
                            manageReadcountry: dataActivity.manageReadcountry,
                            manageReadcountrycode: dataActivity.manageReadcountrycode,
                            manageReadcontinent: dataActivity.manageReadcontinent,
                            manageReadlatitude: dataActivity.manageReadlatitude,
                            manageReadlongitude: dataActivity.manageReadlongitude,
                            manageReademail: dataActivity.manageReademail,
                            manageReadphone: dataActivity.manageReadphone,
                            manageReadtype: dataActivity.manageReadtype,
                            manageReadstyle: dataActivity.manageReadstyle,
                            manageReadstatus: dataActivity.manageReadstatus,
                            manageReadLogo: logoImgManage
                          });
                        }
                      }
                    });
                  }
                }
              });
              this.orgaService.readOrga(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.orgaReadid === '') {
                  } else {
                    this.assetsService.getLogoOrga(dataActivity.orgaReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logoorga not found') {
                          const logoImg = '../assets/img/activity/logo.png';
                          this.orgaReadTable.push({
                            orgaReadid: dataActivity.orgaReadid,
                            orgaReaduserid: dataActivity.orgaReaduserid,
                            orgaReadname: dataActivity.orgaReadname,
                            orgaReadaddedDate: dataActivity.orgaReadaddedDate,
                            orgaReadaddress: dataActivity.orgaReadaddress,
                            orgaReadcity: dataActivity.orgaReadcity,
                            orgaReadcityalt1: dataActivity.orgaReadcityalt1,
                            orgaReadcityalt2: dataActivity.orgaReadcityalt2,
                            orgaReadcountry: dataActivity.orgaReadcountry,
                            orgaReadcountrycode: dataActivity.orgaReadcountrycode,
                            orgaReadcontinent: dataActivity.orgaReadcontinent,
                            orgaReadlatitude: dataActivity.orgaReadlatitude,
                            orgaReadlongitude: dataActivity.orgaReadlongitude,
                            orgaReademail: dataActivity.orgaReademail,
                            orgaReadphone: dataActivity.orgaReadphone,
                            orgaReadtype: dataActivity.orgaReadtype,
                            orgaReadstyle: dataActivity.orgaReadstyle,
                            orgaReadstatus: dataActivity.orgaReadstatus,
                            orgaReadLogo: logoImg
                          });
                        } else {
                          const logoImg = APICloud + '/' + userId + '/' + Filename;
                          this.orgaReadTable.push({
                            orgaReadid: dataActivity.orgaReadid,
                            orgaReaduserid: dataActivity.orgaReaduserid,
                            orgaReadname: dataActivity.orgaReadname,
                            orgaReadaddedDate: dataActivity.orgaReadaddedDate,
                            orgaReadaddress: dataActivity.orgaReadaddress,
                            orgaReadcity: dataActivity.orgaReadcity,
                            orgaReadcityalt1: dataActivity.orgaReadcityalt1,
                            orgaReadcityalt2: dataActivity.orgaReadcityalt2,
                            orgaReadcountry: dataActivity.orgaReadcountry,
                            orgaReadcountrycode: dataActivity.orgaReadcountrycode,
                            orgaReadcontinent: dataActivity.orgaReadcontinent,
                            orgaReadlatitude: dataActivity.orgaReadlatitude,
                            orgaReadlongitude: dataActivity.orgaReadlongitude,
                            orgaReademail: dataActivity.orgaReademail,
                            orgaReadphone: dataActivity.orgaReadphone,
                            orgaReadtype: dataActivity.orgaReadtype,
                            orgaReadstyle: dataActivity.orgaReadstyle,
                            orgaReadstatus: dataActivity.orgaReadstatus,
                            orgaReadLogo: logoImg
                          });
                        }
                      }
                    });
                  }
                }
              });
              this.serviceService.readService(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.serviceReadid === '') {
                  } else {
                    this.assetsService.getLogoService(dataActivity.serviceReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logoservice not found') {
                          const logoImg = '../assets/img/activity/logo.png';
                          this.serviceReadTable.push({
                            serviceReadid: dataActivity.serviceReadid,
                            serviceReaduserid: dataActivity.serviceReaduserid,
                            serviceReadname: dataActivity.serviceReadname,
                            serviceReadaddedDate: dataActivity.serviceReadaddedDate,
                            serviceReadaddress: dataActivity.serviceReadaddress,
                            serviceReadcity: dataActivity.serviceReadcity,
                            serviceReadcityalt1: dataActivity.serviceReadcityalt1,
                            serviceReadcityalt2: dataActivity.serviceReadcityalt2,
                            serviceReadcountry: dataActivity.serviceReadcountry,
                            serviceReadcountrycode: dataActivity.serviceReadcountrycode,
                            serviceReadcontinent: dataActivity.serviceReadcontinent,
                            serviceReadlatitude: dataActivity.serviceReadlatitude,
                            serviceReadlongitude: dataActivity.serviceReadlongitude,
                            serviceReademail: dataActivity.serviceReademail,
                            serviceReadphone: dataActivity.serviceReadphone,
                            serviceReadstyle: dataActivity.serviceReadstyle,
                            serviceReadstatus: dataActivity.serviceReadstatus,
                            serviceReadservice: dataActivity.serviceReadservice,
                            serviceReadLogo: logoImg
                          });
                        } else {
                          const logoImg = APICloud + '/' + userId + '/' + Filename;
                          this.serviceReadTable.push({
                            serviceReadid: dataActivity.serviceReadid,
                            serviceReaduserid: dataActivity.serviceReaduserid,
                            serviceReadname: dataActivity.serviceReadname,
                            serviceReadaddedDate: dataActivity.serviceReadaddedDate,
                            serviceReadaddress: dataActivity.serviceReadaddress,
                            serviceReadcity: dataActivity.serviceReadcity,
                            serviceReadcityalt1: dataActivity.serviceReadcityalt1,
                            serviceReadcityalt2: dataActivity.serviceReadcityalt2,
                            serviceReadcountry: dataActivity.serviceReadcountry,
                            serviceReadcountrycode: dataActivity.serviceReadcountrycode,
                            serviceReadcontinent: dataActivity.serviceReadcontinent,
                            serviceReadlatitude: dataActivity.serviceReadlatitude,
                            serviceReadlongitude: dataActivity.serviceReadlongitude,
                            serviceReademail: dataActivity.serviceReademail,
                            serviceReadphone: dataActivity.serviceReadphone,
                            serviceReadstyle: dataActivity.serviceReadstyle,
                            serviceReadstatus: dataActivity.serviceReadstatus,
                            serviceReadservice: dataActivity.serviceReadservice,
                            serviceReadLogo: logoImg
                          });
                        }
                      }
                    });
                  }
                }
              });
              this.siteService.readSite(activityId).subscribe(dataActivity => {
                if (!dataActivity) {
                } else {
                  if (dataActivity.siteReadid === '') {
                  } else {
                    this.assetsService.getLogoSite(dataActivity.siteReadid).subscribe(dataImg => {
                      if (!dataImg) {
                      } else {
                        const Filename = dataImg.filename;
                        if (Filename === 'logosite not found') {
                          const logoImg = '../assets/img/activity/logo.png';
                          this.siteReadTable.push({
                            siteReadid: dataActivity.siteReadid,
                            siteReaduserid: dataActivity.siteReaduserid,
                            siteReadname: dataActivity.siteReadname,
                            siteReadaddedDate: dataActivity.siteReadaddedDate,
                            siteReadaddress: dataActivity.siteReadaddress,
                            siteReadcity: dataActivity.siteReadcity,
                            siteReadcityalt1: dataActivity.siteReadcityalt1,
                            siteReadcityalt2: dataActivity.siteReadcityalt2,
                            siteReadcountry: dataActivity.siteReadcountry,
                            siteReadcountrycode: dataActivity.siteReadcountrycode,
                            siteReadcontinent: dataActivity.siteReadcontinent,
                            siteReadlatitude: dataActivity.siteReadlatitude,
                            siteReadlongitude: dataActivity.siteReadlongitude,
                            siteReademail: dataActivity.siteReademail,
                            siteReadphone: dataActivity.siteReadphone,
                            siteReadtype: dataActivity.siteReadtype,
                            siteReadstyle: dataActivity.siteReadstyle,
                            siteReadcapacity: dataActivity.siteReadcapacity,
                            siteReadLogo: logoImg
                          });
                        } else {
                          const logoImg = APICloud + '/' + userId + '/' + Filename;
                          this.siteReadTable.push({
                            siteReadid: dataActivity.siteReadid,
                            siteReaduserid: dataActivity.siteReaduserid,
                            siteReadname: dataActivity.siteReadname,
                            siteReadaddedDate: dataActivity.siteReadaddedDate,
                            siteReadaddress: dataActivity.siteReadaddress,
                            siteReadcity: dataActivity.siteReadcity,
                            siteReadcityalt1: dataActivity.siteReadcityalt1,
                            siteReadcityalt2: dataActivity.siteReadcityalt2,
                            siteReadcountry: dataActivity.siteReadcountry,
                            siteReadcountrycode: dataActivity.siteReadcountrycode,
                            siteReadcontinent: dataActivity.siteReadcontinent,
                            siteReadlatitude: dataActivity.siteReadlatitude,
                            siteReadlongitude: dataActivity.siteReadlongitude,
                            siteReademail: dataActivity.siteReademail,
                            siteReadphone: dataActivity.siteReadphone,
                            siteReadtype: dataActivity.siteReadtype,
                            siteReadstyle: dataActivity.siteReadstyle,
                            siteReadcapacity: dataActivity.siteReadcapacity,
                            siteReadLogo: logoImg
                          });
                        }
                      }
                    });
                  }
                }
              });
            }
          }
        }

        // console.log('bandReadTable: ', this.bandReadTable);
        // console.log('equipReadTable: ', this.equipReadTable);
        // console.log('manageReadTable: ', this.manageReadTable);
        // console.log('orgaReadTable: ', this.orgaReadTable);
        // console.log('serviceReadTable: ', this.serviceReadTable);
        // console.log('siteReadTable: ', this.siteReadTable);
        // console.log('this.userReadActivities', this.userReadActivities.length);
        if (this.bandReadTable.length === 0) {
          this.bandReadPres = false;
        } else {
          this.bandReadPres = true;
        }
        if (this.equipReadTable.length === 0) {
          this.equipReadPres = false;
        } else {
          this.equipReadPres = true;
        }
        if (this.manageReadTable.length === 0) {
          this.manageReadPres = false;
        } else {
          this.manageReadPres = true;
        }
        if (this.orgaReadTable.length === 0) {
          this.orgaReadPres = false;
        } else {
          this.orgaReadPres = true;
        }
        if (this.serviceReadTable.length === 0) {
          this.serviceReadPres = false;
        } else {
          this.serviceReadPres = true;
        }
        if (this.siteReadTable.length === 0) {
          this.siteReadPres = false;
        } else {
          this.siteReadPres = true;
        }
        // console.log('userId', userId);
        this.MemberSince = this.dateAndTimes.getMemberSince(this.userReadAddedDate);
      }
    });

    this.contactService.contactState(this.idUserRead).subscribe(dataContact => {
      if (!dataContact) {
      } else {
        if (this.myID === this.idUserRead) {
          this.contactState = 'forbidden';
        } else {
          this.contactState = dataContact.contactstate;
        }
        // console.log('state: ', this.contactState);
      }
    });
    this.assetsService.getImgUser(this.idUserRead).subscribe(dataimg => {
      if (!dataimg) {
      } else {
        const Filename = dataimg.filename;
        if (Filename === 'ImgProfile not found') {
          this.userImage = '../assets/img/users/user.png';
        } else {
          this.userImage = APICloud + '/' + this.idUserRead + '/' + Filename;
        }
      }
    });
  }

  ngAfterViewInit() {}

  sendInviteUser(): void {
    this.contactService.inviteContact(this.idUserRead).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'pending';
      }
    });
  }

  acceptInvitationUser(): void {
    this.contactService.acceptContact(this.idUserRead).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'confirmed';
      }
    });
  }

  refuseInvitationUser(): void {
    this.contactService.refuseContact(this.idUserRead).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }

  deleteContactUser(): void {
    this.contactService.deleteContact(this.idUserRead).subscribe(data => {
      if (!data) {
      } else {
        this.contactState = 'available';
      }
    });
  }
}
