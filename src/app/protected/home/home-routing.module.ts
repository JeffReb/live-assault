import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { AuthGuardService, ValidateGuardService } from '../../services/auth-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BandCreateComponent } from './activity/band/create/band-create.component';
import { SiteCreateComponent } from './activity/site/create/site-create.component';
import { ManageCreateComponent } from './activity/manage/create/manage-create.component';
import { OrgaCreateComponent } from './activity/orga/create/orga-create.component';
import { ServiceCreateComponent } from './activity/service/create/service-create.component';
import { EquipCreateComponent } from './activity/equip/create/equip-create.component';
import { BandReadComponent } from './activity/band/read/band-read.component';
import { SiteReadComponent } from './activity/site/read/site-read.component';
import { ManageReadComponent } from './activity/manage/read/manage-read.component';
import { OrgaReadComponent } from './activity/orga/read/orga-read.component';
import { EquipReadComponent } from './activity/equip/read/equip-read.component';
import { ServiceReadComponent } from './activity/service/read/service-read.component';
import { UserReadComponent } from './users/read/user-read.component';
import { ChatComponent } from './chat/chat.component';
import { BandCompleteComponent } from './activity/band/complete/band-complete.component';
import { EquipCompleteComponent } from './activity/equip/complete/equip-complete.component';
import { ManageCompleteComponent } from './activity/manage/complete/manage-complete.component';
import { OrgaCompleteComponent } from './activity/orga/complete/orga-complete.component';
import { ServiceCompleteComponent } from './activity/service/complete/service-complete.component';
import { SiteCompleteComponent } from './activity/site/complete/site-complete.component';

export const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', component: DashboardComponent, data: { breadcrumb: 'Searching...' } },
      { path: 'create-band', component: BandCreateComponent, data: { breadcrumb: 'Bands' } },
      { path: 'read-band', component: BandReadComponent, data: { breadcrumb: 'Band Detail' } },
      { path: 'complete-band', component: BandCompleteComponent, data: { breadcrumb: 'Band Complete' } },
      { path: 'create-site', component: SiteCreateComponent, data: { breadcrumb: 'Site - Bar - Concert hall' } },
      { path: 'read-site', component: SiteReadComponent, data: { breadcrumb: 'Site Detail' } },
      { path: 'complete-site', component: SiteCompleteComponent, data: { breadcrumb: 'Site Complete' } },
      {
        path: 'create-manage',
        component: ManageCreateComponent,
        data: { breadcrumb: 'Label - Production - Management' }
      },
      { path: 'read-manage', component: ManageReadComponent, data: { breadcrumb: 'Manage Detail' } },
      { path: 'complete-manage', component: ManageCompleteComponent, data: { breadcrumb: 'Manage Complete' } },
      {
        path: 'create-orga',
        component: OrgaCreateComponent,
        data: { breadcrumb: 'Association - Organization - Festival Organizer...' }
      },
      { path: 'read-orga', component: OrgaReadComponent, data: { breadcrumb: 'Orga Detail' } },
      { path: 'complete-orga', component: OrgaCompleteComponent, data: { breadcrumb: 'Orga Complete' } },
      {
        path: 'create-service',
        component: ServiceCreateComponent,
        data: { breadcrumb: 'Services - Music School - Designers - Photographers - Live Reporters...' }
      },
      { path: 'read-service', component: ServiceReadComponent, data: { breadcrumb: 'Service Detail' } },
      { path: 'complete-service', component: ServiceCompleteComponent, data: { breadcrumb: 'Service Complete' } },
      {
        path: 'create-equip',
        component: EquipCreateComponent,
        data: { breadcrumb: 'Equipment Store - Instruments...' }
      },
      { path: 'read-equip', component: EquipReadComponent, data: { breadcrumb: 'Equip Detail' } },
      { path: 'complete-equip', component: EquipCompleteComponent, data: { breadcrumb: 'Equip complete' } },
      { path: 'read-user', component: UserReadComponent, data: { breadcrumb: 'User Detail' } },
      { path: 'chat', component: ChatComponent, data: { breadcrumb: 'User Detail' } }
    ],
    canActivate: [AuthGuardService, ValidateGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}
