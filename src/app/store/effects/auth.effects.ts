import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import {
  AuthActionTypes,
  LogIn,
  LogInSuccess,
  LogInFailure,
  SignUp,
  SignUpSuccess,
  SignUpFailure,
  GetStatus,
  GetStatusSuccess,
  GetStatusFailure,
  LogOut,
  RegisterBand,
  RegisterBandFailure,
  RegisterBandSuccess,
  RegisterSite,
  RegisterSiteFailure,
  RegisterSiteSuccess,
  SendEmailConfirm,
  SendEmailConfirmSuccess,
  SendEmailConfirmFailure,
  ReceipEmailConfirm,
  ReceipEmailConfirmSuccess,
  ReceipEmailConfirmFailure,
  RegisterManage,
  RegisterManageSuccess,
  RegisterManageFailure,
  RegisterOrga,
  RegisterOrgaSuccess,
  RegisterOrgaFailure,
  RegisterServices,
  RegisterServicesSuccess,
  RegisterServicesFailure,
  RegisterEquip,
  RegisterEquipFailure,
  RegisterEquipSuccess,
  RegisterActivity,
  RegisterActivitySuccess,
  RegisterActivityFailure
} from '../actions/auth.actions';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BandService } from '../../services/activity/bands/band.service';
import { SiteService } from '../../services/activity/sites/site.service';
import { ManageService } from '../../services/activity/manage/manage.service';
import { OrgaService } from '../../services/activity/orga/orga.service';
import { ServicesService } from '../../services/activity/services/services.service';
import { EquipService } from '../../services/activity/equip/equip.service';
import { ActivityService } from '../../services/activity/activity.service';
import { TransfertActivityService } from '../../services/activity/transfert.activity.service';
import { TransfertProfileService } from '../../services/activity/transfert.profile.service';

const helper = new JwtHelperService();

@Injectable()
export class AuthEffects {
  constructor(
    private actions: Actions,
    private authService: AuthService,
    private bandService: BandService,
    private siteService: SiteService,
    private manageService: ManageService,
    private orgaService: OrgaService,
    private servicesService: ServicesService,
    private equipService: EquipService,
    private activityService: ActivityService,
    private router: Router,
    private transfertActivitiService: TransfertActivityService,
    private transfertProfileService: TransfertProfileService
  ) {}

  // effects go here
  @Effect()
  LogIn: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN),
    map((action: LogIn) => action.payload),
    switchMap(payload => {
      return this.authService.logIn(payload.email, payload.password).pipe(
        map(user => {
          // console.log(AuthActionTypes.LOGIN);
          // console.log(user.token);
          // console.log(user.activity);
          return new LogInSuccess({
            token: user.token,
            hasband: user.hasband,
            hassite: user.hassite,
            hasmanage: user.hasmanage,
            hasorga: user.hasorga,
            hasservice: user.hasservice,
            hasequip: user.hasequip,
            activity: user.activity,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          // // console.log(AuthActionTypes.LOGIN);
          // // .log(error.error);
          const errorMess = error.error;
          return of(new LogInFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    tap(user => {
      // console.log(user.payload.token);
      // console.log(user.payload.activity);
      const tokenObj = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', tokenObj);
      const validate = helper.decodeToken(user.payload.token)['isValidate'];
      const activity = helper.decodeToken(user.payload.token)['isActivity'];
      // console.log(validate);
      // console.log(activity);
      if (!validate) {
        this.router.navigateByUrl('/emailvalidation');
      } else {
        if (!activity) {
          this.transfertActivitiService.setData(user.payload.activity);
          this.transfertProfileService.setData(user.payload.token);
          this.router.navigateByUrl('/home');
        } else {
          this.transfertActivitiService.setData(user.payload.activity);
          this.transfertProfileService.setData(user.payload.token);
          // console.log('effect loginsuccessactivity: ', user.payload.activity)
          this.router.navigateByUrl('/home');
        }
        // if(!chooseplan) => redirect to: /chooseplan else redirect to: /home
      }
    })
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.LOGIN_FAILURE));

  @Effect()
  SignUp: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SIGNUP),
    map((action: SignUp) => action.payload),
    switchMap(payload => {
      return this.authService.signUp(payload.name, payload.email, payload.password).pipe(
        map(user => {
          return new SignUpSuccess({
            token: user.token,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          const errorMess = error.error;
          return of(new SignUpFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  SignUpSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SIGNUP_SUCCESS),
    tap(user => {
      localStorage.setItem('token', user.payload.token);
      this.router.navigateByUrl('/emailvalidation');
    })
  );

  @Effect({ dispatch: false })
  SignUpFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.SIGNUP_FAILURE));

  @Effect()
  GetStatus: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.GET_STATUS),
    map((action: GetStatus) => action),
    switchMap(payload => {
      return this.authService.getStatus().pipe(
        map(user => {
          // console.log(AuthActionTypes.GET_STATUS);
          // console.log(user.token);
          // console.log(payload);
          return new GetStatusSuccess({
            token: user.token,
            hasband: user.hasband,
            hassite: user.hassite,
            hasmanage: user.hasmanage,
            hasorga: user.hasorga,
            hasservice: user.hasservice,
            hasequip: user.hasequip,
            activity: user.activity,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          // console.log(AuthActionTypes.GET_STATUS);
          // console.log(error.error);
          const errorMess = error.error;
          return of(new GetStatusFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  GetStatusSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.GET_STATUS_SUCCESS),
    tap(user => {
      // console.log(AuthActionTypes.GET_STATUS_SUCCESS);
      // console.log('verify role');
      // console.log('user get status success', user['payload']['profile']['userId']);
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('activity', activity);
    })
  );

  @Effect({ dispatch: false })
  GetStatusFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.GET_STATUS_FAILURE),
    tap(user => {
      // console.log(AuthActionTypes.GET_STATUS_FAILURE);
      // console.log('user get status failure', user);
      // console.log('effect failure', user);
      localStorage.clear();
      this.router.navigateByUrl('');
    })
  );

  @Effect()
  RegisterBand: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_BAND),
    map((action: RegisterBand) => action.payload),
    switchMap(payload => {
      return this.bandService
        .registerBand(
          payload.nameband,
          payload.style1,
          payload.style2,
          payload.style3,
          payload.emailband,
          payload.roleband,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latBand,
          payload.lngBand
        )
        .pipe(
          map(user => {
            return new RegisterBandSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterBandFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterBandSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_BAND_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
      // this.router.navigateByUrl('/home');
      // location.reload();
    })
  );

  @Effect({ dispatch: false })
  RegisterBandFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_BAND_FAILURE));

  @Effect()
  RegisterSite: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_SITE),
    map((action: RegisterSite) => action.payload),
    switchMap(payload => {
      return this.siteService
        .registerSite(
          payload.namehall,
          payload.stylehall,
          payload.adresshall,
          payload.phonehall,
          payload.emailhall,
          payload.typehall,
          payload.capacityhall,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latHall,
          payload.lngHall
        )
        .pipe(
          map(user => {
            return new RegisterSiteSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterSiteFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterSiteSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_SITE_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
    })
  );

  @Effect({ dispatch: false })
  RegisterSiteFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_SITE_FAILURE));

  @Effect()
  RegisterManage: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_MANAGE),
    map((action: RegisterManage) => action.payload),
    switchMap(payload => {
      return this.manageService
        .registerManage(
          payload.namemanage,
          payload.stylemanage,
          payload.adressmanage,
          payload.phonemanage,
          payload.emailmanage,
          payload.typemanage,
          payload.statusmanage,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latManage,
          payload.lngManage
        )
        .pipe(
          map(user => {
            return new RegisterManageSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterManageFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterManageSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_MANAGE_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
    })
  );

  @Effect({ dispatch: false })
  RegisterManageFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_MANAGE_FAILURE));

  @Effect()
  RegisterOrga: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_ORGA),
    map((action: RegisterOrga) => action.payload),
    switchMap(payload => {
      return this.orgaService
        .registerOrga(
          payload.nameorga,
          payload.styleorga,
          payload.adressorga,
          payload.phoneorga,
          payload.emailorga,
          payload.typeorga,
          payload.statusorga,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latOrga,
          payload.lngOrga
        )
        .pipe(
          map(user => {
            return new RegisterOrgaSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterOrgaFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterOrgaSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_ORGA_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
    })
  );

  @Effect({ dispatch: false })
  RegisterOrgaFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_ORGA_FAILURE));

  @Effect()
  RegisterServices: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_SERVICES),
    map((action: RegisterServices) => action.payload),
    switchMap(payload => {
      return this.servicesService
        .registerServices(
          payload.nameservices,
          payload.styleservices,
          payload.adressservices,
          payload.phoneservices,
          payload.emailservices,
          payload.servicesservices,
          payload.statusservices,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latServices,
          payload.lngServices
        )
        .pipe(
          map(user => {
            return new RegisterServicesSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterServicesFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterServicesSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_SERVICES_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
    })
  );

  @Effect({ dispatch: false })
  RegisterServicesFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_SERVICES_FAILURE));

  @Effect()
  RegisterEquip: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_EQUIP),
    map((action: RegisterEquip) => action.payload),
    switchMap(payload => {
      return this.equipService
        .registerEquip(
          payload.nameequip,
          payload.adressequip,
          payload.phoneequip,
          payload.emailequip,
          payload.typeequip,
          payload.cityid,
          payload.city,
          payload.cityalt2id,
          payload.cityalt2,
          payload.cityalt1id,
          payload.cityalt1,
          payload.countryid,
          payload.country,
          payload.countrycode,
          payload.continent,
          payload.latEquip,
          payload.lngEquip
        )
        .pipe(
          map(user => {
            return new RegisterEquipSuccess({
              token: user.token,
              hasband: user.hasband,
              hassite: user.hassite,
              hasmanage: user.hasmanage,
              hasorga: user.hasorga,
              hasservice: user.hasservice,
              hasequip: user.hasequip,
              activity: user.activity,
              profile: helper.decodeToken(user.token)
            });
          }),
          catchError(error => {
            const errorMess = error.error;
            return of(new RegisterEquipFailure({ errorMess }));
          })
        );
    })
  );

  @Effect({ dispatch: false })
  RegisterEquipSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_EQUIP_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      location.href = '/';
    })
  );

  @Effect({ dispatch: false })
  RegisterEquipFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_EQUIP_FAILURE));

  @Effect()
  RegisterActivity: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_ACTIVITY),
    map((action: RegisterActivity) => action.payload),
    switchMap(payload => {
      return this.activityService.registerActivity(payload.firstname, payload.lastname, payload.phone).pipe(
        map(user => {
          return new RegisterActivitySuccess({
            token: user.token,
            hasband: user.hasband,
            hassite: user.hassite,
            hasmanage: user.hasmanage,
            hasorga: user.hasorga,
            hasservice: user.hasservice,
            hasequip: user.hasequip,
            activity: user.activity,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          const errorMess = error.error;
          return of(new RegisterActivityFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  RegisterActivitySuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.REGISTER_ACTIVITY_SUCCESS),
    tap(user => {
      const activity = JSON.stringify(user.payload.activity);
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('activity', activity);
      this.transfertProfileService.setData(user.payload.token);
      this.router.navigateByUrl('/home');
    })
  );

  @Effect({ dispatch: false })
  RegisterActivityFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.REGISTER_ACTIVITY_FAILURE));

  @Effect()
  SendEmailConfirm: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SEND_EMAIL_CONFIRM),
    map((action: SendEmailConfirm) => action.payload),
    switchMap(payload => {
      return this.authService.sendEmailConf(payload.id).pipe(
        map(user => {
          // console.log('effect', AuthActionTypes.SEND_EMAIL_CONFIRM);
          // console.log(user.token);
          // console.log(payload);
          return new SendEmailConfirmSuccess({
            token: user.token,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          // console.log(AuthActionTypes.SEND_EMAIL_CONFIRM);
          // console.log('error effect: ', error.error);
          const errorMess = error.error;
          // return of(false);
          return of(new SendEmailConfirmFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  SendEmailConfirmSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SEND_EMAIL_CONFIRM_SUCCESS),
    tap(user => {
      if (!user) {
        // console.log('not user');
      } else {
        // console.log('user email confirm success:', user);
        // console.log('AuthActionTypes.SEND_EMAIL_CONFIRM_SUCCESS', AuthActionTypes.SEND_EMAIL_CONFIRM_SUCCESS);
        // console.log('verify role');
        // console.log('verify role');
        // console.log(user['payload']['profile']['userId']);
      }
    })
  );
  //
  @Effect({ dispatch: false })
  SendEmailConfirmFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SEND_EMAIL_CONFIRM_FAILURE),
    tap(user => {
      // console.log('AuthActionTypes.SEND_EMAIL_CONFIRM_FAILURE: ', AuthActionTypes.SEND_EMAIL_CONFIRM_FAILURE);
      // console.log('user email confirm failure', user);
    })
  );

  @Effect()
  ReceipEmailConfirm: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.RECEIP_EMAIL_CONFIRM),
    map((action: LogIn) => action.payload),
    switchMap(payload => {
      return this.authService.receipEmailConf(payload.email, payload.password, payload.tokenemail).pipe(
        map(user => {
          // console.log(AuthActionTypes.RECEIP_EMAIL_CONFIRM);
          // console.log(user.token);
          return new ReceipEmailConfirmSuccess({
            token: user.token,
            profile: helper.decodeToken(user.token)
          });
        }),
        catchError(error => {
          // console.log(AuthActionTypes.RECEIP_EMAIL_CONFIRM);
          // console.log(error.error);
          const errorMess = error.error;
          return of(new ReceipEmailConfirmFailure({ errorMess }));
        })
      );
    })
  );

  @Effect({ dispatch: false })
  ReceipEmailConfirmSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.RECEIP_EMAIL_CONFIRM_SUCCESS),
    tap(user => {
      localStorage.setItem('token', user.payload.token);
      // console.log(user.payload.token);
      const validate = helper.decodeToken(user.payload.token)['isValidate'];
      const activity = helper.decodeToken(user.payload.token)['isActivity'];
      // console.log(validate);
      // console.log(activity);
      if (!validate) {
        this.router.navigateByUrl('/emailvalidation');
      } else {
        if (!activity) {
          this.router.navigateByUrl('/home');
        } else {
          this.router.navigateByUrl('/home');
        }
        // if(!chooseplan) => redirect to: /chooseplan else redirect to: /home
      }
    })
  );

  @Effect({ dispatch: false })
  ReceipEmailConfirmFailure: Observable<any> = this.actions.pipe(ofType(AuthActionTypes.RECEIP_EMAIL_CONFIRM_FAILURE));

  @Effect({ dispatch: false })
  public LogOut: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGOUT),
    tap(user => {
      // console.log(user);
      localStorage.clear();
    })
  );
}
