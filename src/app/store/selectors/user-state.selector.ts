import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from '../app.states';

export const getElementsState = createFeatureSelector<State>('elements');

export const getAppErrors = createSelector(
  getElementsState,
  (state: State) => state.errorMessage
);

export const getAppUserAuth = createSelector(
  getElementsState,
  (state: State) => state.isAuthenticated
);

export const getAppUser = createSelector(
  getElementsState,
  (state: State) => state.user
);
