import { Action } from '@ngrx/store';

export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAILURE = '[Auth] Login Failure',
  SIGNUP = '[Auth] Signup',
  SIGNUP_SUCCESS = '[Auth] Signup Success',
  SIGNUP_FAILURE = '[Auth] Signup Failure',
  GET_STATUS = '[Auth] GetStatus',
  GET_STATUS_SUCCESS = '[Auth] GetStatus Success',
  GET_STATUS_FAILURE = '[Auth] GetStatus Failure',
  LOGOUT = '[Auth Logout]',
  REGISTER_BAND = '[Band] Register',
  REGISTER_BAND_SUCCESS = '[Band] Register Success',
  REGISTER_BAND_FAILURE = '[Band] Register Failure',
  REGISTER_SITE = '[Site] Register',
  REGISTER_SITE_SUCCESS = '[Site] Register Success',
  REGISTER_SITE_FAILURE = '[Site] Register Failure',
  REGISTER_MANAGE = '[Manage] Register',
  REGISTER_MANAGE_SUCCESS = '[Manage] Register Success',
  REGISTER_MANAGE_FAILURE = '[Manage] Register Failure',
  REGISTER_ORGA = '[Orga] Register',
  REGISTER_ORGA_SUCCESS = '[Orga] Register Success',
  REGISTER_ORGA_FAILURE = '[Orga] Register Failure',
  REGISTER_SERVICES = '[Services] Register',
  REGISTER_SERVICES_SUCCESS = '[Services] Register Success',
  REGISTER_SERVICES_FAILURE = '[Services] Register Failure',
  REGISTER_EQUIP = '[Equip] Register',
  REGISTER_EQUIP_SUCCESS = '[Equip] Register Success',
  REGISTER_EQUIP_FAILURE = '[Equip] Register Failure',
  REGISTER_ACTIVITY = '[Activity] Register',
  REGISTER_ACTIVITY_SUCCESS = '[Activity] Register Success',
  REGISTER_ACTIVITY_FAILURE = '[Activity] Register Failure',
  CANCEL_ERROR_MESSAGE = '[Error] Cancel error Message',
  SEND_EMAIL_CONFIRM = '[Email Confirm] Send email confirm',
  SEND_EMAIL_CONFIRM_SUCCESS = '[Email Confirm] Send email confirm Success',
  SEND_EMAIL_CONFIRM_FAILURE = '[Email Confirm] Send email confirm Success',
  RECEIP_EMAIL_CONFIRM = '[Receip Email Confirm] Receip email confirm',
  RECEIP_EMAIL_CONFIRM_SUCCESS = '[Receip Email Confirm] Receip email confirm Success',
  RECEIP_EMAIL_CONFIRM_FAILURE = '[Receip Email Confirm] Receip email confirm Failure'
}

export class LogIn implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: any) {}
}

export class LogInSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: any) {}
}

export class LogInFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: any) {}
}

export class SignUp implements Action {
  readonly type = AuthActionTypes.SIGNUP;
  constructor(public payload: any) {}
}

export class SignUpSuccess implements Action {
  readonly type = AuthActionTypes.SIGNUP_SUCCESS;
  constructor(public payload: any) {}
}

export class SignUpFailure implements Action {
  readonly type = AuthActionTypes.SIGNUP_FAILURE;
  constructor(public payload: any) {}
}

export class GetStatus implements Action {
  readonly type = AuthActionTypes.GET_STATUS;
}

export class GetStatusSuccess implements Action {
  readonly type = AuthActionTypes.GET_STATUS_SUCCESS;
  constructor(public payload: any) {}
}

export class GetStatusFailure implements Action {
  readonly type = AuthActionTypes.GET_STATUS_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterBand implements Action {
  readonly type = AuthActionTypes.REGISTER_BAND;
  constructor(public payload: any) {}
}

export class RegisterBandSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_BAND_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterBandFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_BAND_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterSite implements Action {
  readonly type = AuthActionTypes.REGISTER_SITE;
  constructor(public payload: any) {}
}

export class RegisterSiteSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_SITE_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterSiteFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_SITE_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterManage implements Action {
  readonly type = AuthActionTypes.REGISTER_MANAGE;
  constructor(public payload: any) {}
}

export class RegisterManageSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_MANAGE_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterManageFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_MANAGE_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterOrga implements Action {
  readonly type = AuthActionTypes.REGISTER_ORGA;
  constructor(public payload: any) {}
}

export class RegisterOrgaSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_ORGA_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterOrgaFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_ORGA_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterServices implements Action {
  readonly type = AuthActionTypes.REGISTER_SERVICES;
  constructor(public payload: any) {}
}

export class RegisterServicesSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_SERVICES_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterServicesFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_SERVICES_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterEquip implements Action {
  readonly type = AuthActionTypes.REGISTER_EQUIP;
  constructor(public payload: any) {}
}

export class RegisterEquipSuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_EQUIP_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterEquipFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_EQUIP_FAILURE;
  constructor(public payload: any) {}
}

export class RegisterActivity implements Action {
  readonly type = AuthActionTypes.REGISTER_ACTIVITY;
  constructor(public payload: any) {}
}

export class RegisterActivitySuccess implements Action {
  readonly type = AuthActionTypes.REGISTER_ACTIVITY_SUCCESS;
  constructor(public payload: any) {}
}

export class RegisterActivityFailure implements Action {
  readonly type = AuthActionTypes.REGISTER_ACTIVITY_FAILURE;
  constructor(public payload: any) {}
}

export class CancelErrorMessage implements Action {
  readonly type = AuthActionTypes.CANCEL_ERROR_MESSAGE;
  constructor(public payload: any) {}
}

export class SendEmailConfirm implements Action {
  readonly type = AuthActionTypes.SEND_EMAIL_CONFIRM;
  constructor(public payload: any) {}
}

export class SendEmailConfirmSuccess implements Action {
  readonly type = AuthActionTypes.SEND_EMAIL_CONFIRM_SUCCESS;
  constructor(public payload: any) {}
}

export class SendEmailConfirmFailure implements Action {
  readonly type = AuthActionTypes.SEND_EMAIL_CONFIRM_FAILURE;
  constructor(public payload: any) {}
}

export class ReceipEmailConfirm implements Action {
  readonly type = AuthActionTypes.RECEIP_EMAIL_CONFIRM;
  constructor(public payload: any) {}
}

export class ReceipEmailConfirmSuccess implements Action {
  readonly type = AuthActionTypes.RECEIP_EMAIL_CONFIRM_SUCCESS;
  constructor(public payload: any) {}
}

export class ReceipEmailConfirmFailure implements Action {
  readonly type = AuthActionTypes.RECEIP_EMAIL_CONFIRM_FAILURE;
  constructor(public payload: any) {}
}

export class LogOut implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export type All =
  | LogIn
  | LogInSuccess
  | LogInFailure
  | SignUp
  | SignUpSuccess
  | SignUpFailure
  | GetStatus
  | GetStatusSuccess
  | GetStatusFailure
  | RegisterBand
  | RegisterBandSuccess
  | RegisterBandFailure
  | RegisterSite
  | RegisterSiteSuccess
  | RegisterSiteFailure
  | RegisterManage
  | RegisterManageSuccess
  | RegisterManageFailure
  | RegisterOrga
  | RegisterOrgaSuccess
  | RegisterOrgaFailure
  | RegisterServices
  | RegisterServicesSuccess
  | RegisterServicesFailure
  | RegisterEquip
  | RegisterEquipSuccess
  | RegisterEquipFailure
  | RegisterActivity
  | RegisterActivitySuccess
  | RegisterActivityFailure
  | CancelErrorMessage
  | SendEmailConfirm
  | SendEmailConfirmSuccess
  | SendEmailConfirmFailure
  | ReceipEmailConfirm
  | ReceipEmailConfirmSuccess
  | ReceipEmailConfirmFailure
  | LogOut;
