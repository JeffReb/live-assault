import { All, AuthActionTypes } from '../actions/auth.actions';
import { initialState, State } from '../app.states';
import { JwtHelperService } from '@auth0/angular-jwt';
import * as _ from 'lodash';

const helper = new JwtHelperService();

export function reducer(state = initialState, action: All): State {
  switch (action.type) {
    case AuthActionTypes.LOGIN_SUCCESS: {
      let bandObj = {};
      let siteObj = {};
      let manageObj = {};
      let orgaObj = {};
      let serviceObj = {};
      let equipObj = {};
      if (action.payload.activity === null) {
        bandObj = null;
        siteObj = null;
        manageObj = null;
        orgaObj = null;
        serviceObj = null;
        equipObj = null;
      } else {
        const acti = action.payload.activity;
        const lenact = Object.keys(acti).length;

        for (let i = 0, len = lenact; i < len; ++i) {
          const keyact = Object.keys(acti)[i];
          const keyactband = _.startsWith(keyact, 'band');
          const keyactsite = _.startsWith(keyact, 'site');
          const keyactmanage = _.startsWith(keyact, 'manage');
          const keyactorga = _.startsWith(keyact, 'orga');
          const keyactservice = _.startsWith(keyact, 'service');
          const keyactequip = _.startsWith(keyact, 'equip');
          if (keyactband === true) {
            const nameband = _.replace(keyact, 'band_', '');
            bandObj[nameband] = Object.values(acti)[i];
          }
          if (keyactsite === true) {
            const namesite = _.replace(keyact, 'site_', '');
            siteObj[namesite] = Object.values(acti)[i];
          }
          if (keyactmanage === true) {
            const namemanage = _.replace(keyact, 'manage_', '');
            manageObj[namemanage] = Object.values(acti)[i];
          }
          if (keyactorga === true) {
            const nameorga = _.replace(keyact, 'orga_', '');
            orgaObj[nameorga] = Object.values(acti)[i];
          }
          if (keyactservice === true) {
            const nameservice = _.replace(keyact, 'service_', '');
            serviceObj[nameservice] = Object.values(acti)[i];
          }
          if (keyactequip === true) {
            const nameequip = _.replace(keyact, 'equip_', '');
            equipObj[nameequip] = Object.values(acti)[i];
          }
        }
        if (Object.keys(bandObj).length === 0) {
          bandObj = null;
        }
        if (Object.keys(siteObj).length === 0) {
          siteObj = null;
        }
        if (Object.keys(manageObj).length === 0) {
          manageObj = null;
        }
        if (Object.keys(orgaObj).length === 0) {
          orgaObj = null;
        }
        if (Object.keys(serviceObj).length === 0) {
          serviceObj = null;
        }
        if (Object.keys(equipObj).length === 0) {
          equipObj = null;
        }
      }
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        band: bandObj,
        site: siteObj,
        manage: manageObj,
        orga: orgaObj,
        services: serviceObj,
        equip: equipObj,
        errorMessage: null
      };
    }
    case AuthActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.SIGNUP_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.SIGNUP_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.GET_STATUS_SUCCESS: {
      let bandObj = {};
      let siteObj = {};
      let manageObj = {};
      let orgaObj = {};
      let serviceObj = {};
      let equipObj = {};
      if (action.payload.activity === null) {
        bandObj = null;
        siteObj = null;
        manageObj = null;
        orgaObj = null;
        serviceObj = null;
        equipObj = null;
      } else {
        const acti = action.payload.activity;
        const lenact = Object.keys(acti).length;

        for (let i = 0, len = lenact; i < len; ++i) {
          const keyact = Object.keys(acti)[i];
          const keyactband = _.startsWith(keyact, 'band');
          const keyactsite = _.startsWith(keyact, 'site');
          const keyactmanage = _.startsWith(keyact, 'manage');
          const keyactorga = _.startsWith(keyact, 'orga');
          const keyactservice = _.startsWith(keyact, 'service');
          const keyactequip = _.startsWith(keyact, 'equip');
          if (keyactband === true) {
            const nameband = _.replace(keyact, 'band_', '');
            bandObj[nameband] = Object.values(acti)[i];
          }
          if (keyactsite === true) {
            const namesite = _.replace(keyact, 'site_', '');
            siteObj[namesite] = Object.values(acti)[i];
          }
          if (keyactmanage === true) {
            const namemanage = _.replace(keyact, 'manage_', '');
            manageObj[namemanage] = Object.values(acti)[i];
          }
          if (keyactorga === true) {
            const nameorga = _.replace(keyact, 'orga_', '');
            orgaObj[nameorga] = Object.values(acti)[i];
          }
          if (keyactservice === true) {
            const nameservice = _.replace(keyact, 'service_', '');
            serviceObj[nameservice] = Object.values(acti)[i];
          }
          if (keyactequip === true) {
            const nameequip = _.replace(keyact, 'equip_', '');
            equipObj[nameequip] = Object.values(acti)[i];
          }
        }
        if (Object.keys(bandObj).length === 0) {
          bandObj = null;
        }
        if (Object.keys(siteObj).length === 0) {
          siteObj = null;
        }
        if (Object.keys(manageObj).length === 0) {
          manageObj = null;
        }
        if (Object.keys(orgaObj).length === 0) {
          orgaObj = null;
        }
        if (Object.keys(serviceObj).length === 0) {
          serviceObj = null;
        }
        if (Object.keys(equipObj).length === 0) {
          equipObj = null;
        }
      }
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        band: bandObj,
        site: siteObj,
        manage: manageObj,
        orga: orgaObj,
        services: serviceObj,
        equip: equipObj,
        errorMessage: null
      };
    }
    case AuthActionTypes.GET_STATUS_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_BAND_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_BAND_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_SITE_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_SITE_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_MANAGE_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_MANAGE_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_ORGA_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_ORGA_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_SERVICES_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_SERVICES_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_EQUIP_SUCCESS: {
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_EQUIP_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.REGISTER_ACTIVITY_SUCCESS: {
      const acti = action.payload.activity;
      const lenact = Object.keys(acti).length;
      let bandObj = {};
      let siteObj = {};
      let manageObj = {};
      let orgaObj = {};
      let serviceObj = {};
      let equipObj = {};
      for (let i = 0, len = lenact; i < len; ++i) {
        const keyact = Object.keys(acti)[i];
        const keyactband = _.startsWith(keyact, 'band');
        const keyactsite = _.startsWith(keyact, 'site');
        const keyactmanage = _.startsWith(keyact, 'manage');
        const keyactorga = _.startsWith(keyact, 'orga');
        const keyactservice = _.startsWith(keyact, 'service');
        const keyactequip = _.startsWith(keyact, 'equip');
        if (keyactband === true) {
          const nameband = _.replace(keyact, 'band_', '');
          bandObj[nameband] = Object.values(acti)[i];
        }
        if (keyactsite === true) {
          const namesite = _.replace(keyact, 'site_', '');
          siteObj[namesite] = Object.values(acti)[i];
        }
        if (keyactmanage === true) {
          const namemanage = _.replace(keyact, 'manage_', '');
          manageObj[namemanage] = Object.values(acti)[i];
        }
        if (keyactorga === true) {
          const nameorga = _.replace(keyact, 'orga_', '');
          orgaObj[nameorga] = Object.values(acti)[i];
        }
        if (keyactservice === true) {
          const nameservice = _.replace(keyact, 'service_', '');
          serviceObj[nameservice] = Object.values(acti)[i];
        }
        if (keyactequip === true) {
          const nameequip = _.replace(keyact, 'equip_', '');
          equipObj[nameequip] = Object.values(acti)[i];
        }
      }
      if (Object.keys(bandObj).length === 0) {
        bandObj = null;
      }
      if (Object.keys(siteObj).length === 0) {
        siteObj = null;
      }
      if (Object.keys(manageObj).length === 0) {
        manageObj = null;
      }
      if (Object.keys(orgaObj).length === 0) {
        orgaObj = null;
      }
      if (Object.keys(serviceObj).length === 0) {
        serviceObj = null;
      }
      if (Object.keys(equipObj).length === 0) {
        equipObj = null;
      }
      return {
        ...state,
        user: {
          token: action.payload.token,
          hasband: action.payload.hasband,
          hassite: action.payload.hassite,
          hasmanage: action.payload.hasmanage,
          hasorga: action.payload.hasorga,
          hasservice: action.payload.hasservice,
          hasequip: action.payload.hasequip,
          activity: action.payload.activity,
          profile: helper.decodeToken(action.payload.token)
        },
        band: bandObj,
        site: siteObj,
        manage: manageObj,
        orga: orgaObj,
        services: serviceObj,
        equip: equipObj,
        errorMessage: null
      };
    }
    case AuthActionTypes.REGISTER_ACTIVITY_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.CANCEL_ERROR_MESSAGE: {
      return {
        ...state,
        errorMessage: null
      };
    }
    case AuthActionTypes.SEND_EMAIL_CONFIRM_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.RECEIP_EMAIL_CONFIRM_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        user: {
          token: action.payload.token,
          profile: helper.decodeToken(action.payload.token)
        },
        errorMessage: null
      };
    }
    case AuthActionTypes.RECEIP_EMAIL_CONFIRM_FAILURE: {
      return {
        ...state,
        errorMessage: action.payload.errorMess.error
      };
    }
    case AuthActionTypes.LOGOUT: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
