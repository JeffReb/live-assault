import { User } from '../models/user';
import { Band } from '../models/band';
import { Site } from '../models/site';
import { Manage } from '../models/manage';
import { Orga } from '../models/orga';
import { Services } from '../models/services';
import { Equip } from '../models/equip';

export interface State {
  // is a user authenticated ?
  isAuthenticated: boolean;

  // if authenticated, there should be a user object
  user: User | null;

  band: Band | null;

  site: Site | null;

  manage: Manage | null;

  orga: Orga | null;

  services: Services | null;

  equip: Equip | null;

  // error message
  errorMessage: string | null;
}

export const initialState: State = {
  isAuthenticated: false,
  user: null,
  band: null,
  site: null,
  manage: null,
  orga: null,
  services: null,
  equip: null,
  errorMessage: null
};

export interface AppStates {
  appStates: State;
}
