import { AfterViewInit, Component } from '@angular/core';
import { Settings } from './app.settings.model';
import { AppSettings } from './app.settings';

import { GetStatus } from './store/actions/auth.actions';
import { Store } from '@ngrx/store';
import { AppStates } from './store/app.states';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  public settings: Settings;

  constructor(public appSettings: AppSettings, private auth: AuthService, private store: Store<AppStates>) {
    this.settings = this.appSettings.settings;
  }

  ngAfterViewInit() {
    if (!this.auth.getToken()) {
      return false;
    } else {
      this.store.dispatch(new GetStatus());
    }
  }
}
