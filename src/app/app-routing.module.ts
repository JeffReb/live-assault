import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './public/errors/error/error.component';
import { LoginComponent } from './public/login/login.component';
import { RegisterComponent } from './public/register/register.component';
import { HomepublicComponent } from './public/homepublic/homepublic.component';
import { NotFoundComponent } from './public/errors/not-found/not-found.component';
import { EmailvalComponent } from './protected/validation/emailval/emailval.component';
import { ChooseplanComponent } from './protected/validation/chooseplan/chooseplan.component';
import { ActivityGuardService, AuthGuardService, ValidateGuardService } from './services/auth-guard.service';
import { RedirectGuardService } from './services/redirect-guard.service';
import { EmailloginconfComponent } from './public/emailval/emailloginconf.component';

export const AppRoutes: Routes = [
  { path: '', component: HomepublicComponent, pathMatch: 'full', canActivate: [RedirectGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'emailloginconf/:token', component: EmailloginconfComponent },
  { path: 'emailvalidation', component: EmailvalComponent, canActivate: [AuthGuardService] },
  { path: 'chooseplan', component: ChooseplanComponent, canActivate: [AuthGuardService, ValidateGuardService] },
  {
    path: 'home',
    loadChildren: './protected/home/home.module#HomeModule'
  },
  { path: 'error', component: ErrorComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(AppRoutes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
