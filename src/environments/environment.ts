// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APIEndpoint: 'http://127.0.0.1:5000/api',
  APICloud: 'https://lva-cloud.live-assault.com',
  MAPGoogleKey: 'AIzaSyAC-l7XOnaC0Br7LHkozP-Ayoj--8oFDN4',
  SocketEndPoint: 'http://localhost:5000'
};

// *************** For another member of dev team ****************
// production: false,
// APIEndpoint: 'https://feature.meta.live-assault.com/api',
// APICloud: 'https://lva-cloud.live-assault.com',
// MAPGoogleKey: 'AIzaSyAC-l7XOnaC0Br7LHkozP-Ayoj--8oFDN4',   <= for this you must send to me your IP to insert authorization on google API
// SocketEndPoint: 'https://feature.meta.live-assault.com'

// ********** for the dev master *****************
// production: false,
// APIEndpoint: 'http://127.0.0.1:5000/api',
// APICloud: 'https://lva-cloud.live-assault.com',
// MAPGoogleKey: 'AIzaSyAC-l7XOnaC0Br7LHkozP-Ayoj--8oFDN4',
// SocketEndPoint: 'http://localhost:5000'

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
