import { $$, browser, by, element, Key } from 'protractor';

export class AppPage {
  navigateTo(route) {
    return browser.get(route) as Promise<any>;
  }

  getSendPseudoRegister(data) {
    return element(by.id('mat-input-0')).sendKeys(data);
  }

  getSendEmailRegister(data) {
    return element(by.id('mat-input-1')).sendKeys(data);
  }

  getSendPassRegister(data) {
    return element(by.id('mat-input-2')).sendKeys(data);
  }

  getSendConfirmPassRegister(data) {
    return element(by.id('mat-input-3')).sendKeys(data);
  }

  getEnterRegister() {
    return element(by.css('.box-button > .mat-button-wrapper')).click();
  }

  getMatInputSend0(data) {
    return element(by.id('mat-input-0')).sendKeys(data);
  }

  getMatInputSend1(data) {
    return element(by.id('mat-input-1')).sendKeys(data);
  }

  getMatInputSend2(data) {
    return element(by.id('mat-input-2')).sendKeys(data);
  }

  getMatInputSend3(data) {
    return element(by.id('mat-input-3')).sendKeys(data);
  }

  getMatInputClick4() {
    return element(by.id('mat-input-4')).click();
  }

  getMatInputSend4(data) {
    return element(by.id('mat-input-4')).sendKeys(data);
  }

  getMatInputSend5(data) {
    return element(by.id('mat-input-5')).sendKeys(data);
  }

  getMatSelectRoleClick() {
    return element(by.css('.mat-select-placeholder')).click();
  }

  getMatSelectRole0Click() {
    return element(by.css('#mat-option-0 > .mat-option-pseudo-checkbox')).click();
  }

  getOverlayClick() {
    return element(by.css('.cdk-overlay-backdrop')).click();
  }

  getEnterKeyMap() {
    return element(by.id('mat-input-4')).sendKeys(Key.ENTER);
  }

  getKeyDownMap() {
    return element(by.id('mat-input-4')).sendKeys(Key.DOWN);
  }

  getEnterBand() {
    return element(by.css('.mat-elevation-z12 > .mat-button-wrapper')).click();
  }

  getTitleText() {
    return element(by.css('.box-content-header')).getText() as Promise<string>;
  }

  getCreateBandText() {
    return element(by.css('.user-name')).getText() as Promise<string>;
  }

  getInputMailClick() {
    return element(by.id('mat-input-0')).click();
  }

  getInputMailSend(mail) {
    return element(by.id('mat-input-0')).sendKeys(mail);
  }

  getInputPassClick() {
    return element(by.id('mat-input-1')).click();
  }

  getInputPassSend() {
    return element(by.id('mat-input-1')).sendKeys('slayer1');
  }

  getInputEnter() {
    return element(by.css('.box-button')).click();
  }

  getHomeText() {
    return element(by.css('.mat-card-subtitle > h4')).getText() as Promise<string>;
  }

  getNumberofInput() {
    console.log('input: ', $$('input').count());
    return $$('input').count();
  }
}
