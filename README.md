Develop branch:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/develop/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/develop)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/develop/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/develop)
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

[https://staging.live-assault.com](https://staging.live-assault.com)

Beta branch:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/beta/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/beta)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/beta/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/beta)
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

[https://beta.live-assault.com](https://beta.live-assault.com)

Master:

[![pipeline status](https://gitlab.com/JeffReb/live-assault/badges/master/pipeline.svg)](https://gitlab.com/JeffReb/live-assault/commits/master)
[![coverage report](https://gitlab.com/JeffReb/live-assault/badges/master/coverage.svg)](https://gitlab.com/JeffReb/live-assault/commits/master)  
[![Known Vulnerabilities](https://snyk.io//test/github/jeffAerials/live-assault/badge.svg?targetFile=package.json)](https://snyk.io//test/github/jeffAerials/live-assault?targetFile=package.json)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/JeffReb/live-front)

[https://live-assault.com](https://live-assault.com)

# LiveAssault

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# Git Flow

![](doc-contrib/img/gitflow.png)

## Commits rules

a. First, 1 commit by feature

b. commit title and description rules

```sh
    <type>:<subject>
    <description>
    example:
    feat:Add Sass instead of css
    Add npm Sass dependencies and replace in
    angular-cli.json the stylesheet type in by sass

```

for this we use precommit git hook

### Git Hook in precommit

this project is set to use TSLint before commit and test commit rules

if your code don't pass TSLint and commit rules your commit action are not executed

we use husky npm package installed in this project to setup this fonction

we use commitlint npm package to setup lint commit message in pre-commit

you can see rules below:
see: https://commitlint.js.org/#/reference-rules

```nashorn js
- `feat`: (new feature for the user, not a new feature for build script)
- `fix`: (bug fix for the user, not a fix to a build script)
- `docs`: (changes to the documentation)
- `style`: (formatting, missing semi colons, etc; no production code change)
- `refactor`: (refactoring production code, eg. renaming a variable)
- `test`: (adding missing tests, refactoring tests; no production code change)
- `chore`: (updating grunt tasks etc; no production code change)
- `build`: (Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm))
- `ci`: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
```
